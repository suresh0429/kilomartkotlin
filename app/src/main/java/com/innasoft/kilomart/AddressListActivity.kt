package com.innasoft.kilomart

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.os.Build
import android.os.Bundle

import android.text.Html
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.AddressAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Model.AddressModelItem
import com.innasoft.kilomart.Response.AddressResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.ArrayList
import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission_group.CAMERA
import kotlinx.android.synthetic.main.activity_address_list.*

class AddressListActivity : AppCompatActivity(), View.OnClickListener {


    internal var locationManager: LocationManager? = null



    private var pref: PrefManager? = null
    private var cartAdapter: AddressAdapter? = null

    internal var userId: String? = null
    internal var itemslength: String? = null
    internal var module: String? = null
    internal var customerId: String? = null
    internal var totalPrice: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var checked: Int = 0
    internal var checkoutStatus: Boolean = false
    internal var addressModelItems = ArrayList<AddressModelItem>()
    private val provider = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_list)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Address"


        val app = application as AppController
        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        if (intent.extras != null) {
            checkoutStatus = intent.getBooleanExtra("Checkout", false)

        }


        if (app.isConnection) {

            prepareAddressData()


        } else {

            setContentView(R.layout.internet)


        }

        btnAddAddress.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        val intent = Intent(this@AddressListActivity, AddAddressActivity::class.java)
        intent.putExtra("Checkout", checkoutStatus)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }


    private fun prepareAddressData() {
        progress!!.visibility = View.VISIBLE
        val call = RetrofitClient.instance!!.api.getAddress(tokenValue, userId)
        call.enqueue(object : Callback<AddressResponse> {
            override fun onResponse(
                call: Call<AddressResponse>,
                response: Response<AddressResponse>
            ) {
                progress!!.visibility = View.GONE

                val addressResponse = response.body()

                Log.d("ADDRESS", "onResponse: " + addressResponse!!.message)

                if (response.isSuccessful) {

                    val docsBeanList = if (response.body() != null) response.body()!!.data else null

                    if (addressResponse.status.equals("10100", ignoreCase = true)) {

                        for (address in docsBeanList!!) {

                            addressModelItems.add(
                                AddressModelItem(
                                    address.id,
                                    address.name,
                                    address.address_line1,
                                    address.address_line2,
                                    address.area,
                                    address.city,
                                    address.state,
                                    address.pincode,
                                    address.country,
                                    address.contact_no,
                                    address.alternate_contact_no,
                                    address.latitude,
                                    address.longitude,
                                    address.is_default,
                                    address.isDelivery_status,
                                    address.delivery_charges
                                )
                            )
                        }

                        cartAdapter = AddressAdapter(
                            this@AddressListActivity,
                            addressModelItems,
                            tokenValue.toString(),
                            userId.toString(),
                            checkoutStatus
                        )
                        addressList!!.adapter = cartAdapter
                        cartAdapter!!.setSelectedIndex(checked)
                        addressList!!.onItemClickListener =
                            AdapterView.OnItemClickListener { parent, view, position, id ->
                                cartAdapter!!.setSelectedIndex(position)
                                cartAdapter!!.notifyDataSetChanged()
                            }

                    } else if (addressResponse.status == "10200") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (addressResponse.status == "10300") {

                        txtAlert!!.visibility = View.VISIBLE
                        txtAlert!!.text = addressResponse.message


                        //   Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (addressResponse.status == "10400") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
            }

            override fun onFailure(call: Call<AddressResponse>, t: Throwable) {
                progress!!.visibility = View.GONE
                Snackbar.make(
                    addressList!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })


    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home ->

                if (checkoutStatus) {
                    val intent = Intent(this@AddressListActivity, CheckoutActivity::class.java)
                    intent.putExtra("Checkout", checkoutStatus)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                } else {
                    val intent = Intent(this@AddressListActivity, MyAccountActivity::class.java)
                    intent.putExtra("Checkout", checkoutStatus)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        // super.onBackPressed();

        if (checkoutStatus) {
            val intent = Intent(this@AddressListActivity, CheckoutActivity::class.java)
            intent.putExtra("Checkout", checkoutStatus)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            val intent = Intent(this@AddressListActivity, MyAccountActivity::class.java)
            intent.putExtra("Checkout", checkoutStatus)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }


}
