package com.innasoft.kilomart

import android.app.Activity
import android.graphics.Color
import android.os.Bundle

import android.text.Html
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.TextView


import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.SearchAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Response.ProductResponse
import com.innasoft.kilomart.Storage.PrefManager

import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_search.*
import retrofit2.Call
import retrofit2.Callback

class SearchActivity : Activity() {

    private var adapter: SearchAdapter? = null

    private var pref: PrefManager? = null
    internal var userid: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)


        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userid = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        btnBack!!.setOnClickListener { onBackPressed() }

        search!!.isActivated = true
        search!!.queryHint = "Type your keyword here"
        search!!.onActionViewExpanded()
        search!!.isIconified = false


        search!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {


                Log.e("QUERY :", "" + query)

                prepareProductData(query, "")

                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                if (search!!.query.length == 0) {

                    prepareProductData(query, "")

                }
                return false
            }
        })


        search!!.setOnCloseListener {
            search!!.setQuery("", false)
            adapter!!.notifyDataSetChanged()
            prepareProductData("", "HideRecycler")
            false
        }


    }


    private fun prepareProductData(query: String, hideRecycler: String) {

        var searchQuery = ""

        try {
            searchQuery = URLEncoder.encode(query, "utf-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        val finalSearchQuery = searchQuery

        progressBar!!.visibility = View.VISIBLE
        val call =
            RetrofitClient.instance!!.api.getProductSearch("SINGLE", deviceId, finalSearchQuery)
        call.enqueue(object : Callback<ProductResponse> {
            override fun onResponse(
                call: Call<ProductResponse>,
                response: retrofit2.Response<ProductResponse>
            ) {
                progressBar!!.visibility = View.GONE

                val addressResponse = response.body()

                if (response.isSuccessful) {

                    val docsBeanList = if (response.body() != null) response.body()!!.data else null

                    if (addressResponse!!.status.equals("10100", ignoreCase = true)) {

                        txtSearch!!.visibility = View.VISIBLE
                        txtNodata!!.visibility = View.GONE
                        rcProduct!!.visibility = View.VISIBLE
                        txtSearch!!.text = "Search results of $query"


                        if (hideRecycler.equals("HideRecycler", ignoreCase = true)) {
                            rcProduct!!.visibility = View.GONE
                        } else {

                            // homeKitchen Adapter
                            val mLayoutManager = GridLayoutManager(applicationContext, 2)
                            rcProduct!!.layoutManager = mLayoutManager
                            rcProduct!!.itemAnimator = DefaultItemAnimator()


                            adapter = SearchAdapter(applicationContext, docsBeanList!!.toList())
                            rcProduct!!.adapter = adapter
                            adapter!!.notifyDataSetChanged()
                        }

                    } else if (addressResponse.status.equals("10300", ignoreCase = true)) {

                        txtNodata!!.visibility = View.VISIBLE
                        txtSearch!!.visibility = View.GONE
                        rcProduct!!.visibility = View.GONE
                        if (finalSearchQuery.length == 0 || finalSearchQuery.isEmpty() || finalSearchQuery.equals(
                                "",
                                ignoreCase = true
                            )
                        ) {
                            txtNodata!!.visibility = View.GONE
                            rcProduct!!.visibility = View.GONE
                        } else {
                            txtNodata!!.text = "Your search $query did not match any products !"
                        }

                    } else {


                    }

                } else {
                    // error case
                    when (response.code()) {
                        404 -> Snackbar.make(
                            rcProduct!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.not_found) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        500 -> Snackbar.make(
                            rcProduct!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.server_broken) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        else -> Snackbar.make(
                            rcProduct!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.unknown_error) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }

            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Snackbar.make(
                    rcProduct!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })


    }


    // validate name
    private fun isValidQuery(name: String): Boolean {

        if (name.length < 3) {

            return false
        } else {
            prepareProductData(name, "HideRecycler")
        }
        return true
    }


    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }
}
