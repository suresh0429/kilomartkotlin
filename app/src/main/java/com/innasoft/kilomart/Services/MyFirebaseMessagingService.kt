package com.innasoft.kilomart.Services


import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build

import android.util.Log
import android.widget.RemoteViews

import androidx.core.app.NotificationCompat

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.innasoft.kilomart.NotificationActivity
import com.innasoft.kilomart.R

import org.json.JSONException
import org.json.JSONObject

import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

import com.bumptech.glide.gifdecoder.GifHeaderParser.TAG

class MyFirebaseMessagingService : FirebaseMessagingService() {

    internal var bitmap: Bitmap? = null
    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s!!)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.e("remoteMessage", "" + remoteMessage.data)


        /*  Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);
        String taskId= "";
        try {
            Log.d("JSON_OBJECT", object.getString("task_id"));
            taskId = object.getString("task_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        if (remoteMessage.data.size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            val title: String?
            val message: String?
            val imageUrl: String?

            title = remoteMessage.data["title"]
            message = remoteMessage.data["message"]
            imageUrl = remoteMessage.data["imageUrl"]

            bitmap = getBitmapfromUrl(imageUrl)

            val intent = Intent(this, NotificationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            builder.setContentTitle(title)
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(
                    NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap)
                )/*Notification with Image*/
                .setSound(uri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(0 /* ID of notification */, builder.build())

        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body!!)
        }


    }

    /*
     *To get a Bitmap image from the URL received
     * */
    fun getBitmapfromUrl(imageUrl: String?): Bitmap? {
        try {
            val url = URL(imageUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            return BitmapFactory.decodeStream(input)

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
            return null

        }

    }

    companion object {
        //there can be multiple notifications so it can be used as notification identity
        private val CHANNEL_ID = "KILOMART_01"
        val NOTIFICATION_ID = 1
    }

}
