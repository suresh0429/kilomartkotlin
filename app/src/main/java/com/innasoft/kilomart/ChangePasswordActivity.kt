package com.innasoft.kilomart

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_changepassword.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity : AppCompatActivity(),
    ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener {

    private var pref: PrefManager? = null
    internal var userId: String? = null
    internal var mobile: String? = null
    internal var username: String? = null
    internal var imagePic: String? = null
    internal var email: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var gender: String? = null
    internal lateinit var appController: AppController

    internal lateinit var session: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_changepassword)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Edit Account"


        session = PrefManager(this)
        appController = application as AppController

        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        username = profile["name"]
        mobile = profile["mobile"]
        email = profile["email"]
        imagePic = profile["profilepic"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]
        gender = profile["gender"]


        editSave.setOnClickListener(this)
    }


    override fun onClick(view: View?) {
        when (view!!.id) {


            R.id.editSave -> {
                // Method to manually check connection status
                val isConnected = appController.isConnection

                if (isConnected) {
                    uploadFile(view)
                } else {

                    val message = "Sorry! Not connected to internet"
                    val color = Color.RED

                    snackBar(message, color)
                    //showSnack(isConnected);
                }
            }
        }    }



    private fun uploadFile(parentView: View) {


        val oldPassword = editoldpwd!!.text!!.toString().trim { it <= ' ' }
        val newPassword = editnewPwd!!.text!!.toString().trim { it <= ' ' }

        if (oldPassword.isEmpty()) {
            Toast.makeText(applicationContext, "Please Enter Old Password", Toast.LENGTH_SHORT)
                .show()
            return
        }
        if (newPassword.isEmpty()) {
            Toast.makeText(applicationContext, "Please Enter New Password", Toast.LENGTH_SHORT)
                .show()
            return
        }


        /* if (!validatePassword()) {
            return;
        }*/

        /* if (!NewPassword.equalsIgnoreCase(ConfirmPassword)) {
            Snackbar.make(parentView, "Password Doesn't Match !", Snackbar.LENGTH_SHORT).show();
            return;
        }*/


        val progressDoalog = ProgressDialog(this@ChangePasswordActivity)
        progressDoalog.setMessage("Loading....")
        progressDoalog.show()


        val call = RetrofitClient.instance!!.api.userChangePassword(
            tokenValue,
            userId,
            oldPassword,
            newPassword
        )

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressDoalog.dismiss()
                val baseResponse = response.body()


                if (response.isSuccessful) {

                    if (baseResponse!!.status.equals("10100", ignoreCase = true)) {

                        Handler().postDelayed({
                            val meService =
                                Intent(this@ChangePasswordActivity, MyAccountActivity::class.java)
                            meService.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(meService)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }, 2000)

                        // update session
                        session.createLogin(
                            userId,
                            username,
                            email,
                            mobile,
                            null,
                            deviceId,
                            tokenValue,
                            gender
                        )

                        Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                            .show()

                    } else if (baseResponse.status == "10200") {

                        Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    } else if (baseResponse.status == "10300") {

                        Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    } else if (baseResponse.status == "10400") {

                        Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    }

                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressDoalog.dismiss()
            }
        })
    }

    override fun onResume() {
        super.onResume()

        // register connection status listener

        AppController.instance!!.setConnectivityListener(this)
    }

    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String
        val color: Int
        if (isConnected) {
            message = "Good! Connected to Internet"
            color = Color.WHITE

        } else {
            message = "Sorry! Not connected to internet"
            color = Color.RED
        }

        snackBar(message, color)


    }

    // snackBar
    private fun snackBar(message: String, color: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}