package com.innasoft.kilomart

import android.content.Intent
import android.os.Bundle

import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.innasoft.kilomart.Adapters.OrdersListAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Response.OrderListResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_orders_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrdersListActivity : AppCompatActivity() {
    internal lateinit var appController: AppController


    private var pref: PrefManager? = null
    internal lateinit var ordersListAdapter: OrdersListAdapter

    internal var userId: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var orderId: String? = null
    internal var descriptionData =
        arrayOf("OrderPlace", "InProgress", "Shipped", "delivered", "Completed")
    internal var cartStatus: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders_list)
        ButterKnife.bind(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Orders Items"
        appController = application as AppController


        // Displaying user information from shared preferences
        pref = PrefManager(applicationContext)

        val profile = pref!!.userDetails
        userId = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        if (intent != null) {
            orderId = intent.getStringExtra("Order_ID")
            cartStatus = intent.getBooleanExtra("Checkout", false)
        }

        if (appController.isConnection) {

            prepareOrderListData()

        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }
    }


    private fun prepareOrderListData() {

        progressBar!!.visibility = View.VISIBLE

        val call = RetrofitClient.instance!!.api.getOrderList(tokenValue, userId, orderId)
        call.enqueue(object : Callback<OrderListResponse> {
            override fun onResponse(
                call: Call<OrderListResponse>,
                response: Response<OrderListResponse>
            ) {
                if (response.isSuccessful);
                val orderListResponse = response.body()
                if (orderListResponse!!.status == "10100") {
                    progressBar!!.visibility = View.GONE
                    //                    Toast.makeText(OrdersListActivity.this, orderListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    val orderDetailsBean = orderListResponse.data!!.orderDetails
                    txtSubtotal!!.text =
                        resources.getString(R.string.Rs) + " " + orderDetailsBean!!.totalMrpPrice
                    txtDeliverCharges!!.text =
                        resources.getString(R.string.Rs) + " " + orderDetailsBean.shippingCharges
                    txtPayMode!!.text = "Payment Mode : " + orderDetailsBean.getawayName
                    order_status!!.text = "Order status : " + orderDetailsBean.orderStatus
                    txtExpectedDate!!.text =
                        "Expected Delivery Date : " + orderDetailsBean.expectedDelivery
                    txtamntPaid!!.text =
                        resources.getString(R.string.Rs) + " " + orderDetailsBean.finalPrice

                    val productsBeanList = orderListResponse.data!!.products

                    val layoutManager =
                        LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
                    recycler_view!!.layoutManager = layoutManager
                    recycler_view!!.itemAnimator = DefaultItemAnimator()
                    ordersListAdapter = OrdersListAdapter(this@OrdersListActivity, productsBeanList!!.toList())
                    recycler_view!!.adapter = ordersListAdapter
                } else if (orderListResponse.status == "10200") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@OrdersListActivity,
                        orderListResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (orderListResponse.status == "10300") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@OrdersListActivity,
                        orderListResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (orderListResponse.status == "10400") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@OrdersListActivity,
                        orderListResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }

            override fun onFailure(call: Call<OrderListResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Toast.makeText(this@OrdersListActivity, t.message, Toast.LENGTH_SHORT).show()

            }
        })


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home ->

                if (cartStatus) {
                    val intent = Intent(this@OrdersListActivity, HomeActivity::class.java)
                    intent.putExtra("Checkout", cartStatus)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                } else {
                    val intent = Intent(this@OrdersListActivity, OrderIdActvity::class.java)
                    intent.putExtra("Checkout", cartStatus)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        // super.onBackPressed();

        if (cartStatus) {
            val intent = Intent(this@OrdersListActivity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("Checkout", cartStatus)
            startActivity(intent)
        } else {
            val intent = Intent(this@OrdersListActivity, OrderIdActvity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("Checkout", cartStatus)
            startActivity(intent)
        }
    }


}
