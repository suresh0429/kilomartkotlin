package com.innasoft.kilomart

import android.content.Intent
import android.graphics.Color
import android.os.Bundle

import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast


import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.innasoft.kilomart.Adapters.OrderIdAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Response.MyOrdersResponse
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Model.OrderIdItem
import com.innasoft.kilomart.Singleton.AppController

import java.util.ArrayList
import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_order_id_actvity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderIdActvity : AppCompatActivity() {
    internal var color = Color.RED
    internal lateinit var appController: AppController

    private var pref: PrefManager? = null
    internal lateinit var myOrderAdapter: OrderIdAdapter
    internal var orderIdItemList: List<OrderIdItem> = ArrayList()
    internal var UserID: String? = null
    internal var orderId: String? = null
    internal var userId: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var checkoutStatus: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_id_actvity)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "My Orders"
        appController = application as AppController

        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        if (intent.extras != null) {
            checkoutStatus = intent.getBooleanExtra("Checkout", false)

        }


        if (appController.isConnection) {

            prepareOrderIdData()

        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }


        }
    }


    private fun prepareOrderIdData() {

        progressBar!!.visibility = View.VISIBLE

        val call = RetrofitClient.instance!!.api.MyOrders(tokenValue, userId)
        call.enqueue(object : Callback<MyOrdersResponse> {
            override fun onResponse(
                call: Call<MyOrdersResponse>,
                response: Response<MyOrdersResponse>
            ) {
                if (response.isSuccessful);
                val myOrdersResponse = response.body()

                if (myOrdersResponse!!.status == "10100") {

                    progressBar!!.visibility = View.GONE

                    //                    Toast.makeText(OrderIdActvity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    val modulesBeanList = response.body()!!.data!!.recordData
                    val layoutManager =
                        LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
                    orderidRecycler!!.layoutManager = layoutManager
                    orderidRecycler!!.itemAnimator = DefaultItemAnimator()

                    myOrderAdapter = OrderIdAdapter(applicationContext, modulesBeanList!!.toList())
                    orderidRecycler!!.adapter = myOrderAdapter
                    myOrderAdapter.notifyDataSetChanged()

                } else if (myOrdersResponse.status == "10200") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@OrderIdActvity,
                        myOrdersResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (myOrdersResponse.status == "10300") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@OrderIdActvity,
                        myOrdersResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                    txtError!!.visibility = View.VISIBLE
                    txtError!!.text = "No Orders Found !"

                } else if (myOrdersResponse.status == "10400") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@OrderIdActvity,
                        myOrdersResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    txtError!!.visibility = View.VISIBLE
                    txtError!!.text = "No Orders Found !"
                }
            }

            override fun onFailure(call: Call<MyOrdersResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Toast.makeText(this@OrderIdActvity, t.message, Toast.LENGTH_SHORT).show()
            }
        })


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}
