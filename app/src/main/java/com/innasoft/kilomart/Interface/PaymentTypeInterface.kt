package com.innasoft.kilomart.Interface

import com.innasoft.kilomart.Response.CheckoutResponse

interface PaymentTypeInterface {

    fun onItemClick(
        paymentGatewayBean: List<CheckoutResponse.DataBean.PaymentGatewayBean>?,
        position: Int
    )
}
