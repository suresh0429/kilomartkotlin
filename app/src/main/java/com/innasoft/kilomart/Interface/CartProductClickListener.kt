package com.innasoft.kilomart.Interface

import com.innasoft.kilomart.Model.Product

interface CartProductClickListener {

    fun onMinusClick(product: Product)

    fun onPlusClick(product: Product)

    fun onSaveClick(product: Product)

    fun onRemoveDialog(product: Product)

    fun onWishListDialog(product: Product)

}
