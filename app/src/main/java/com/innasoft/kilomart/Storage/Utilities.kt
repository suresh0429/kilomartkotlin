package com.innasoft.kilomart.Storage

import java.text.DecimalFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

object Utilities {

    val PREFS_LOCATION = "LOCATION_PREF"

    val KEY_LOCATIONFIRSTTIME = "LocationFirstTime"
    val KEY_PINCODE = "pincode"
    val KEY_AREA = "area"
    val KEY_SHIPPINGCHARGES = "shipping_charges"


    fun capitalize(capString: String?): String {
        val capBuffer = StringBuffer()
        val capMatcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1)!!.toUpperCase() + capMatcher.group(2)!!.toLowerCase()
            )
        }

        return capMatcher.appendTail(capBuffer).toString()

    }

    public val df = DecimalFormat("0.00")

}
