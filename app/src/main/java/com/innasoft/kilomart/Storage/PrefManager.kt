package com.innasoft.kilomart.Storage

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor

import java.util.HashMap

/**
 * Created by Ravi on 08/07/15.
 */
class PrefManager(// Context
    internal var _context: Context
) {
    // Shared Preferences
    internal var pref: SharedPreferences

    // Editor for Shared preferences
    internal var editor: Editor

    // Shared pref mode
    internal var PRIVATE_MODE = 0

    var isWaitingForSms: Boolean
        get() = pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false)
        set(isWaiting) {
            editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting)
            editor.commit()
        }

    var mobileNumber: String?
        get() = pref.getString(KEY_MOBILE_NUMBER, null)
        set(mobileNumber) {
            editor.putString(KEY_MOBILE_NUMBER, mobileNumber)
            editor.commit()
        }

    val isLoggedIn: Boolean
        get() = pref.getBoolean(KEY_IS_LOGGED_IN, false)

    val userDetails: HashMap<String, String?>
        get() {
            val profile = HashMap<String, String?>()
            profile["id"] = pref.getString(KEY_ID, null)
            profile["name"] = pref.getString(KEY_NAME, null)
            profile["email"] = pref.getString(KEY_EMAIL, null)
            profile["mobile"] = pref.getString(KEY_MOBILE, null)
            profile["profilepic"] = pref.getString(KEY_PROFILE_PIC, null)
            profile["deviceId"] = pref.getString(KEY_DEVICEID, null)
            profile["AccessToken"] = pref.getString(KEY_TOKEN, null)
            profile["gender"] = pref.getString(KEY_GENDER, null)

            return profile
        }


    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLogin(
        id: String?,
        name: String?,
        email: String?,
        mobile: String?,
        profilepic: String?,
        device_id: String?,
        token: String?,
        gender: String?
    ) {
        editor.putString(KEY_ID, id)
        editor.putString(KEY_NAME, name)
        editor.putString(KEY_EMAIL, email)
        editor.putString(KEY_MOBILE, mobile)
        editor.putString(KEY_PROFILE_PIC, profilepic)
        editor.putString(KEY_DEVICEID, device_id)
        editor.putString(KEY_TOKEN, token)
        editor.putString(KEY_GENDER, gender)
        editor.putBoolean(KEY_IS_LOGGED_IN, true)

        editor.commit()
    }

    fun clearSession() {
        editor.clear()
        editor.commit()
    }

    companion object {

        // Shared preferences file name
        private val PREF_NAME = "pickany"

        // All Shared Preferences Keys
        private val KEY_IS_WAITING_FOR_SMS = "IsWaitingForSms"
        private val KEY_MOBILE_NUMBER = "mobile_number"
        private val KEY_IS_LOGGED_IN = "isLoggedIn"
        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_EMAIL = "email"
        private val KEY_MOBILE = "mobile"
        private val KEY_PROFILE_PIC = "profilepic"
        private val KEY_DEVICEID = "deviceId"
        private val KEY_TOKEN = "AccessToken"
        private val KEY_GENDER = "gender"
    }
}
