package com.innasoft.kilomart

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle

import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView


import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.ProductAadpter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Helper.Converter

import com.innasoft.kilomart.Response.ProductResponse
import com.innasoft.kilomart.Storage.PrefManager


import com.innasoft.kilomart.Singleton.AppController

import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_product_list.*
import retrofit2.Call
import retrofit2.Callback


class ProductListActivity : AppCompatActivity() {
    internal lateinit var swipeRefreshLayout: SwipeRefreshLayout
    internal lateinit var appController: AppController

    private var adapter: ProductAadpter? = null
    private var rcProduct: RecyclerView? = null
    internal var sortLinear: LinearLayout? = null
    internal var filterLinear: LinearLayout? = null
    private var pref: PrefManager? = null
    internal var userid: String? = null
    internal var title: String? = null
    internal var module: String? = null
    internal var catId: String? = null
    internal var subcatId: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null


    internal var cartindex: Int = 0
    internal var positionValue: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userid = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        if (intent.extras != null) {
            catId = intent.getStringExtra("catId")
            subcatId = intent.getStringExtra("subcatId")
            title = intent.getStringExtra("title")

        }

        supportActionBar!!.title = title
        appController = applicationContext as AppController


        // init SwipeRefreshLayout and ListView
        swipeRefreshLayout = findViewById<View>(R.id.simpleSwipeRefreshLayout) as SwipeRefreshLayout
        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorAccent,
            R.color.colorBlue,
            R.color.colorPrimary
        )

        rcProduct = findViewById<View>(R.id.rcProduct) as RecyclerView


        // cart count
        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindex", "" + cartindex)
        invalidateOptionsMenu()


        if (appController.isConnection) {

            prepareProductData(subcatId)

            swipeRefreshLayout.setOnRefreshListener {
                prepareProductData(subcatId)

                swipeRefreshLayout.isRefreshing = false
            }


        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            // app.internetDilogue(KitchenitemListActivity.this);

        }

    }

    private fun prepareProductData(subcatId: String?) {

        swipeRefreshLayout.isRefreshing = true

        val call = RetrofitClient.instance!!.api.getProductList(
            catId,
            subcatId,
            "0",
            "SINGLE",
            "1",
            userid,
            deviceId,
            "",
            "",
            "LOW_PRICE"
        )

        call.enqueue(object : Callback<ProductResponse> {
            override fun onResponse(
                call: Call<ProductResponse>,
                response: retrofit2.Response<ProductResponse>
            ) {
                swipeRefreshLayout.isRefreshing = false
                val productResponse = response.body()

                if (response.isSuccessful) {


                    val filteredProductsBeanList =
                        if (response.body() != null) response.body()!!.data else null

                    if (filteredProductsBeanList?.size ?: 0 != 0) {

                        val mLayoutManager = GridLayoutManager(applicationContext, 2)
                        rcProduct!!.layoutManager = mLayoutManager
                        rcProduct!!.itemAnimator = DefaultItemAnimator()

                        adapter = ProductAadpter(applicationContext, filteredProductsBeanList!!.toList())
                        rcProduct!!.adapter = adapter
                        adapter!!.notifyDataSetChanged()
                    } else {
                        txtError!!.visibility = View.VISIBLE
                        txtError!!.text = productResponse!!.message
                    }


                } else {
                    // error case
                    when (response.code()) {
                        404 -> Snackbar.make(
                            rcProduct!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.not_found) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        500 -> Snackbar.make(
                            rcProduct!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.server_broken) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        else -> Snackbar.make(
                            rcProduct!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.unknown_error) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }

            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                swipeRefreshLayout.isRefreshing = false
                Snackbar.make(
                    rcProduct!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })


    }

    override fun onRestart() {

        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindexonstart", "" + cartindex)
        invalidateOptionsMenu()
        super.onRestart()
    }

    override fun onStart() {
        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindexonstart", "" + cartindex)
        invalidateOptionsMenu()
        super.onStart()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.actionbar_menu, menu)
        val menuItem = menu.findItem(R.id.action_cart)
        menuItem.icon = Converter.convertLayoutToImage(
            this@ProductListActivity,
            cartindex,
            R.drawable.ic_actionbar_bag
        )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()

            R.id.action_cart -> {
                val intent = Intent(applicationContext, CartActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            R.id.action_search -> {
                val intent1 = Intent(this@ProductListActivity, SearchActivity::class.java)
                intent1.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent1)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
