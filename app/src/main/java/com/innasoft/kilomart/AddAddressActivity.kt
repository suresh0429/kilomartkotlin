package com.innasoft.kilomart

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Storage.Utilities

import java.io.IOException
import java.util.Arrays
import java.util.HashMap
import java.util.LinkedHashSet
import java.util.Locale
import java.util.regex.Matcher
import java.util.regex.Pattern

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import kotlinx.android.synthetic.main.activity_add_address.*


class AddAddressActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener,
    OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {


    internal lateinit var geocoder: Geocoder
    internal lateinit var latitude1: String
    internal lateinit var longitude1: String
    internal lateinit var addresses: List<Address>
    internal var latitute_tt: Double = 0.toDouble()
    internal var longitiu_II: Double = 0.toDouble()
    internal lateinit var mapFragment: SupportMapFragment
    internal var PLACE_AUTOCOMPLETE_REQUEST_CODE = 1

    private var mMap: GoogleMap? = null
    private var onCameraIdleListener: GoogleMap.OnCameraIdleListener? = null
    internal lateinit var mLocationRequest: LocationRequest
    internal var mGoogleApiClient: GoogleApiClient? = null
    internal lateinit var mLastLocation: Location
    internal var mCurrLocationMarker: Marker? = null


    internal var parentLayout: RelativeLayout? = null
    internal lateinit var appController: AppController


    private var pref: PrefManager? = null
    internal var userId: String? = null
    internal lateinit var checkId: String
    internal var loc_area: String? = null
    internal var loc_pincode: String? = null
    internal var shippingcaharge: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var username: String? = null
    internal var mobile: String? = null
    internal var checkoutStatus: Boolean = false
    internal var locationPrefBoolean: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_address)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Add Address")


        geocoder = Geocoder(this, Locale.getDefault())

        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        username = profile["name"]
        mobile = profile["mobile"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        // location Prefences
        val locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0)
        locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false)
        loc_area = locationPref.getString(Utilities.KEY_AREA, "")
        loc_pincode = locationPref.getString(Utilities.KEY_PINCODE, "")
        shippingcaharge = locationPref.getString(Utilities.KEY_SHIPPINGCHARGES, "")


        if (intent.extras != null) {
            checkoutStatus = intent.getBooleanExtra("Checkout", false)

        }

        appController = application as AppController


        etName!!.setText(username)
        etPhoneNo!!.setText(mobile)
        etArea!!.setText(loc_area)
        etPincode!!.setText(loc_pincode)
        etCity!!.setText("Hyderabad")
        etState!!.setText("Telangana")
        etCountry!!.setText("India")

        etName!!.addTextChangedListener(MyTextWatcher(etName!!))
        etAddressline1!!.addTextChangedListener(MyTextWatcher(etAddressline1!!))
        etAddressline2!!.addTextChangedListener(MyTextWatcher(etAddressline2!!))
        etArea!!.addTextChangedListener(MyTextWatcher(etArea!!))
        etCity!!.addTextChangedListener(MyTextWatcher(etCity!!))
        etState!!.addTextChangedListener(MyTextWatcher(etState!!))
        etCountry!!.addTextChangedListener(MyTextWatcher(etCountry!!))
        etPincode!!.addTextChangedListener(MyTextWatcher(etPincode!!))
        etPhoneNo!!.addTextChangedListener(MyTextWatcher(etPhoneNo!!))
        etAlternateno!!.addTextChangedListener(MyTextWatcher(etAlternateno!!))


        checkId = "Yes"
        checkboxDefault!!.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {

                checkId = "Yes"
            } else {

                checkId = "No"
            }
        }

        textSearch!!.setOnClickListener {
            try {
                val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setBoundsBias(BOUNDS_INDIA)
                    .build(this@AddAddressActivity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            }
        }

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        configureCameraIdle()

        btnApply.setOnClickListener(this)
    }


    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btnApply -> {

                val isConnected = appController.isConnection
                if (isConnected) {
                    validateForm()
                } else {

                    val message = "Sorry! Not connected to internet"
                    val color = Color.RED
                    snackBar(message, color)
                }
            }
        }
    }


    //validate inputs...
    private fun validateForm() {

        val name = etName!!.text.toString()
        val addressline1 = etAddressline1!!.text.toString()
        val addressline2 = etAddressline2!!.text.toString()
        val area = etArea!!.text.toString()
        val city = etCity!!.text.toString()
        val state = etState!!.text.toString()
        val country = etCountry!!.text.toString()
        val pincode = etPincode!!.text.toString()
        val phone = etPhoneNo!!.text.toString()
        val alternateno = etAlternateno!!.text.toString()


        if (!isValidName(name)) {
            return
        }
        if (!isValidPinCode(pincode)) {
            return
        }
        if (!isValidPhoneNumber(phone)) {
            return
        }
        /*if ((!isValidPhoneNumber1(alternateno))) {
            return;
        }
*/


        progressBar!!.visibility = View.VISIBLE
        btnApply!!.isEnabled = false
        btnApply!!.visibility = View.GONE

        //  Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userAddress(tokenValue, userId, name, addressline1, addressline2, area, city, state, pincode, phone, alternateno, String.valueOf(latitute_tt), String.valueOf(longitiu_II), checkId);
        val call = RetrofitClient.instance!!.api.userAddress(
            tokenValue,
            userId,
            name,
            addressline1,
            addressline2,
            area,
            city,
            state,
            pincode,
            phone,
            alternateno,
            "",
            "",
            checkId
        )

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressBar!!.visibility = View.GONE
                btnApply!!.isEnabled = true
                btnApply!!.visibility = View.VISIBLE

                val registerResponse = response.body()

                if (response.isSuccessful) {

                    if (registerResponse!!.status.equals("10100", ignoreCase = true)) {


                        Handler().postDelayed({
                            val intent =
                                Intent(this@AddAddressActivity, AddressListActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            intent.putExtra("Checkout", checkoutStatus)
                            startActivity(intent)
                        }, 1000)


                    }

                } else {
                    val color = Color.RED
                    snackBar(registerResponse!!.message!!, color)
                }


            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                btnApply!!.isEnabled = true
                btnApply!!.visibility = View.VISIBLE

            }
        })


    }


    override fun onResume() {
        super.onResume()

        // register connection status listener

        AppController.instance!!.setConnectivityListener(this)
    }


    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String
        val color: Int
        if (isConnected) {
            message = "Good! Connected to Internet"
            color = Color.WHITE

        } else {
            message = "Sorry! Not connected to internet"
            color = Color.RED
        }

        snackBar(message, color)


    }


    // snackBar
    private fun snackBar(message: String, color: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    // validate name
    private fun isValidName(name: String): Boolean {
        val pattern = Pattern.compile("[a-zA-Z ]+")
        val matcher = pattern.matcher(name)

        if (name.isEmpty()) {
            name_til!!.error = "name is required"
            requestFocus(etName!!)
            return false
        } else if (!matcher.matches()) {
            name_til!!.error = "Enter Alphabets Only"
            requestFocus(etName!!)
            return false
        } else if (name.length < 5 || name.length > 20) {
            name_til!!.error = "Name Should be 5 to 20 characters"
            requestFocus(etName!!)
            return false
        } else {
            name_til!!.isErrorEnabled = false
        }
        return matcher.matches()
    }


    // validate phone
    private fun isValidPhoneNumber(mobile: String): Boolean {
        val pattern = Pattern.compile("^[9876]\\d{9}$")
        val matcher = pattern.matcher(mobile)

        if (mobile.isEmpty()) {
            mobile_til!!.error = "Phone no is required"
            requestFocus(etPhoneNo!!)
            return false
        } else if (!matcher.matches()) {
            mobile_til!!.error = "Enter a valid mobile"
            requestFocus(etPhoneNo!!)
            return false
        } else {
            mobile_til!!.isErrorEnabled = false
        }

        return matcher.matches()
    }

    private fun isValidPhoneNumber1(mobile: String): Boolean {
        val pattern = Pattern.compile("^[9876]\\d{9}$")
        val matcher = pattern.matcher(mobile)

        if (mobile.isEmpty()) {
            ti_etAlternateno!!.error = "Phone no is required"
            requestFocus(etAlternateno!!)
            return false
        } else if (!matcher.matches()) {
            ti_etAlternateno!!.error = "Enter a valid mobile"
            requestFocus(etAlternateno!!)
            return false
        } else {
            ti_etAlternateno!!.isErrorEnabled = false
        }

        return matcher.matches()
    }

    // valid OTP
    private fun isValidPinCode(pincode: String): Boolean {


        if (pincode.isEmpty()) {
            zip_til!!.error = "Pincode is required"
            requestFocus(etPincode!!)
            etCity!!.setText("")
            etState!!.setText("")
            return false
        } else if (pincode.length < 6) {
            zip_til!!.error = "Enter a valid Pincode"

            return false
        } else {
            zip_til!!.isErrorEnabled = false
        }

        return true
    }


    // request focus
    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    // text input layout class
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.etName -> isValidName(etName!!.text.toString().trim { it <= ' ' })
                R.id.etPhoneNo -> isValidPhoneNumber(etPhoneNo!!.text.toString().trim { it <= ' ' })
                R.id.etPincode -> isValidPinCode(etPincode!!.text.toString().trim { it <= ' ' })
                R.id.etAlternateno -> isValidPhoneNumber1(etAlternateno!!.text.toString().trim { it <= ' ' })
            }
        }
    }


    private fun configureCameraIdle() {
        onCameraIdleListener = GoogleMap.OnCameraIdleListener {
            val latLng = mMap!!.cameraPosition.target
            val geocoder = Geocoder(this@AddAddressActivity)

            try {
                val addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                if (addressList != null && addressList.size > 0) {
                    val locality =
                        addressList[0].getAddressLine(0)  //whole address coming in lovality
                    val addr = locality.split(",".toRegex(), 2).toTypedArray()
                    Log.d("LOClity", locality)
                    Log.d("ARYA", addr[0] + "////" + addr[1])

                    val country = addressList[0].countryName
                    val state = addressList[0].adminArea
                    val city = addressList[0].locality
                    val area = addressList[0].subLocality
                    val pincode = addressList[0].postalCode
                    latitute_tt = addressList[0].latitude
                    longitiu_II = addressList[0].longitude
                    if (!locality.isEmpty() && !country.isEmpty())
                        etName!!.setText(username)
                    etAddressline1!!.setText(addr[0])
                    etAddressline2!!.setText(addr[1])
                    etArea!!.setText(area)
                    etCity!!.setText(city)
                    etState!!.setText(state)
                    etCountry!!.setText(country)
                    etPincode!!.setText(pincode)
                    etPhoneNo!!.setText(mobile)

                }

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }


    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            )
        }


    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }

        //Place current location marker
        val latLng = LatLng(location.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        /*  markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
*/
        //  CircleOptions addCircle = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        //   mCircle = mMap.addCircle(addCircle);

        //move map camera
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(19f))

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }

    public override fun onPause() {
        super.onPause()

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                //Location Permission already granted
                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true
            } else {
                //Request Location Permission
                checkLocationPermission()
            }
        } else {
            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
        }
        mMap!!.setOnCameraIdleListener(onCameraIdleListener)
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this@AddAddressActivity)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient!!.connect()
    }


    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(this, data!!)
                Log.d("EDDIPPPP", "" + place.address + "---" + place.latLng)
                var stringlat = place.latLng.toString()
                var name: CharSequence? = place.name
                val address = place.address
                var attributions = place.attributions as String?
                if (attributions == null) {
                    attributions = ""
                }
                if (name!!.toString().contains("°")) {
                    name = ""
                }
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1)
                stringlat = stringlat.substring(0, stringlat.indexOf(")"))
                val latValue =
                    stringlat.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
                latitude1 = latValue
                val lngValue =
                    stringlat.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                longitude1 = lngValue

                val lat = java.lang.Double.valueOf(latitude1)
                val lng = java.lang.Double.valueOf(longitude1)
                val fulladd = address!!.toString()
                val addr = fulladd.split(",".toRegex(), 2).toTypedArray()
                if (name != null && name.length > 0)
                    etAddressline1!!.setText(name)
                else
                    etAddressline2!!.setText(addr[0])


                //area_edt.setText(addr[5]+","+addr[6]+","+addr[7]);
                ////#2-56/2/19, 3rd Floor, Vijaya Towers, near Meridian School,, Ayyappa Society 100ft Road, Madhapur, Ayyappa Society, Chanda Naik Nagar, Madhapur, Hyderabad, Telangana 500081, India
                try {
                    geocoder = Geocoder(this@AddAddressActivity)
                    addresses = geocoder.getFromLocation(lat, lng, 1)
                    //    save_btn.setEnabled(true);
                    val addLine = addresses[0].getAddressLine(0)
                    val city = addresses[0].locality
                    etCity!!.setText(city)
                    val area = addresses[0].subLocality
                    etArea!!.setText(area)
                    val state = addresses[0].adminArea
                    etState!!.setText(state)
                    val country = addresses[0].countryName
                    etCountry!!.setText(country)
                    val postalCode = addresses[0].postalCode
                    etPincode!!.setText(postalCode)
                    val addLineTwo = addresses[0].thoroughfare
                    // address_two_edt.setText(addLineTwo);
                    val addLineOne = addresses[0].featureName
                    // address_one_edt.setText(addLineOne);
                    if (addr[1].contains(postalCode)) {
                        addr[1] = addr[1].replace(postalCode, " ")
                    }
                    if (addr[1].contains(state)) {
                        addr[1] = addr[1].replace(state, " ")
                    }
                    if (addr[1].contains(country)) {
                        addr[1] = addr[1].replace(country, " ")
                    }
                    if (addr[1].contains(city)) {
                        addr[1] = addr[1].replace(city, " ")
                    }
                    if (addr[1] != null && area != null && addr[1].contains(area)) {
                        addr[1] = addr[1].replace(area, " ")
                    }
                    addr[1] =
                        LinkedHashSet(Arrays.asList(*addr[1].split("\\s".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())).toString()
                            .replace("[\\[\\],]".toRegex(), "")
                    etAddressline2!!.setText(addr[1])
                    etName!!.setText(username)
                    if (!mobile!!.equals("null", ignoreCase = true))
                        etPhoneNo!!.setText(mobile)

                    Log.d(
                        "ADDRESSES",
                        addLine + "\n" + city + "\n" + state + "\n" + country + "\n" + postalCode + "\n" + etAddressline1
                    )
                    Log.d(
                        "REMAININGADDRESS",
                        addresses[0].subAdminArea + "\n" + addresses[0].subLocality + "\n" + addresses[0].premises + "\n" + addresses[0].subThoroughfare
                                + "\n" + addresses[0].thoroughfare
                    )
                    Log.v("FFFFFFF", "//$address")
                    //address_txt.setText(address);
                    // finaladdress = address.toString();
                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker!!.remove()
                    }
                    val latLng = LatLng(
                        java.lang.Double.parseDouble(latitude1),
                        java.lang.Double.parseDouble(longitude1)
                    )
                    val markerOptions = MarkerOptions()
                    markerOptions.position(latLng)
                    /*   markerOptions.title("Current Position");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    mCurrLocationMarker = mMap.addMarker(markerOptions);*/

                    //  CircleOptions addCircle = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
                    //  mCircle = mMap.addCircle(addCircle);

                    //move map camera
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(19f))

                    //stop location updates
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(
                            mGoogleApiClient,
                            this
                        )
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                val status = PlaceAutocomplete.getStatus(this, data!!)
                // TODO: Handle the error.
                Log.i("XDIDK", status.statusMessage!!)

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder(this)
                    .setTitle("Location Permission Needed")
                    .setMessage("This app needs the Location permission, please accept to use location functionality")
                    .setPositiveButton("OK") { dialogInterface, i ->
                        //Prompt the user once explanation has been shown
                        ActivityCompat.requestPermissions(
                            this@AddAddressActivity,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            MY_PERMISSIONS_REQUEST_LOCATION
                        )
                    }
                    .create()
                    .show()


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient()
                        }
                        mMap!!.isMyLocationEnabled = true
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }/*Intent intent =new Intent(CartActivity.this, SmsActivity.class);
                intent.putExtra("link",carBikeItem.getLink());
                intent.putExtra("id",carBikeItem.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);*/
        return super.onOptionsItemSelected(item)
    }

    companion object {

        val MY_PERMISSIONS_REQUEST_LOCATION = 99

        private val TAG = "AddAddressActivity"

        private val BOUNDS_INDIA =
            LatLngBounds(LatLng(23.63936, 68.14712), LatLng(28.20453, 97.34466))
    }


}

