package com.innasoft.kilomart.Model

class AddressModelItem(
    var id: String?,
    var name: String?,
    var address_line1: String?,
    var address_line2: String?,
    var area: String?,
    var city: String?,
    var state: String?,
    var pincode: String?,
    var country: String?,
    var contact_no: String?,
    var alternate_contact_no: String?,
    var latitude: String?,
    var longitude: String?,
    var is_default: String?,
    var isDelivery_status: Boolean,
    var delivery_charges: String?
)
