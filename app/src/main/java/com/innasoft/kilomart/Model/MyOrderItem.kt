package com.innasoft.kilomart.Model

class MyOrderItem(
    var isChecked: Boolean,
    var product_name: String,
    var id: String,
    var user_id: String,
    var order_id: String,
    var category: String,
    var cart_type: String,
    var item_id: String,
    var quantity: String,
    var unit_price_incl_tax: String,
    var unit_price_exld_tax: String,
    var tax_rate: String,
    var tax_amt: String,
    var discount: String,
    var total_price: String,
    var color: String,
    var delivery_time: String,
    var order_status: String,
    var reason_for_cancel_or_return: String,
    var photo_cake_image: String,
    var delivery_charges: String,
    var date_of_order: String,
    var image_path: String
)
