package com.innasoft.kilomart.Model

class OrderIdItem(
    var orderId: String?,
    var orderDate: String?,
    var paymentId: String?,
    var orderstatus: String?,
    var paymentType: String?,
    var paymentStatus: String?,
    var transactionId: String?
)
