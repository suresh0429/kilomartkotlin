package com.innasoft.kilomart.Model

class Product(
    var id: String?,
    var product_id: String?,
    var product_name: String?,
    var selling_price: Double?,
    var mrp_price: Double?,
    var purchase_quantity: Int,
    var unit_id: String?,
    var unit_name: String?,
    var unit_value: String?,
    var brand_id: String?,
    var brand_name: String?,
    var net_amount: Double?,
    var gross_amount: Double?,
    var images: String?
)
