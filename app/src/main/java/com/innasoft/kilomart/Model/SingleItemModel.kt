package com.innasoft.kilomart.Model

/**
 * Created by pratap.kesaboyina on 01-12-2015.
 */
class SingleItemModel(
    var id: String?,
    var url_name: String?,
    var product_name: String?,
    var type: String?,
    var main_category_id: String?,
    var main_category_name: String?,
    var sub_category_id: String?,
    var sub_category_name: String?,
    var child_category_id: String?,
    var child_category_name: Any?,
    var unit_id: String?,
    var unit_name: String?,
    var unit_value: String?,
    var brand_id: String?,
    var brand_name: String?,
    var qty: String?,
    var mrp_price: Double?,
    var offer_price: Double?,
    var selling_price: Double?,
    var about: String?,
    var moreinfo: String?,
    var availability: String?,
    var user_rating: String?,
    var features: String?,
    var position: String?,
    var seo_title: String?,
    var seo_description: String?,
    var seo_keywords: String?,
    var images: List<String>?
)
