package com.innasoft.kilomart.Model

import java.util.ArrayList

/**
 * Created by pratap.kesaboyina on 30-11-2015.
 */
class HeaderSectionDataModel {


    var headerTitle: String? = null
    var categoryId: String? = null
    var subcategoryId: String? = null
    var allItemsInSection: ArrayList<SingleItemModel>? = null


    constructor() {

    }

    constructor(
        headerTitle: String,
        categoryId: String,
        subcategoryId: String,
        allItemsInSection: ArrayList<SingleItemModel>
    ) {
        this.headerTitle = headerTitle
        this.categoryId = categoryId
        this.subcategoryId = subcategoryId
        this.allItemsInSection = allItemsInSection
    }
}
