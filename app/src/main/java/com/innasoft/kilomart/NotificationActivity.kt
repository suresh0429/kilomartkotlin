package com.innasoft.kilomart

import android.os.Bundle

import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.innasoft.kilomart.Adapters.NotificationsAdapter
import com.innasoft.kilomart.Adapters.PaymentTpyeRecyclerAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Response.NotificationsResponse
import com.innasoft.kilomart.Storage.PrefManager

import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_notifications.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : AppCompatActivity() {

    internal var userId: String? = null
    internal var email: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    private var pref: PrefManager? = null
    internal lateinit var notificationsAdapter: NotificationsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Notifications"



        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        email = profile["email"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        val call = RetrofitClient.instance!!.api.Notifications(tokenValue, userId)
        call.enqueue(object : Callback<NotificationsResponse> {
            override fun onResponse(
                call: Call<NotificationsResponse>,
                response: Response<NotificationsResponse>
            ) {
                if (response.isSuccessful);
                val notificationsResponse = response.body()
                if (notificationsResponse!!.status == "10100") {
                    progressBar!!.visibility = View.GONE
                    //                    Toast.makeText(NotificationActivity.this, notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    val dataBeans = notificationsResponse.data

                    val layoutManager =
                        LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
                    notifction_recycleview!!.layoutManager = layoutManager
                    notifction_recycleview!!.itemAnimator = DefaultItemAnimator()
                    notificationsAdapter =
                        NotificationsAdapter(this@NotificationActivity, dataBeans!!)
                    notifction_recycleview!!.adapter = notificationsAdapter
                } else if (notificationsResponse.status == "10200") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@NotificationActivity,
                        notificationsResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (notificationsResponse.status == "10300") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@NotificationActivity,
                        notificationsResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (notificationsResponse.status == "10400") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@NotificationActivity,
                        notificationsResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }

            override fun onFailure(call: Call<NotificationsResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Toast.makeText(this@NotificationActivity, t.message, Toast.LENGTH_SHORT).show()

            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


}
