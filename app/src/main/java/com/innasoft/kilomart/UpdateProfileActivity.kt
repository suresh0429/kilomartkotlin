package com.innasoft.kilomart

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler

import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.HashMap
import java.util.regex.Matcher
import java.util.regex.Pattern

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_update_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateProfileActivity : AppCompatActivity(),
    ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener {



    private var pref: PrefManager? = null

    internal lateinit var radioButton: RadioButton

    internal var userId: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var gender: String? = null
    internal lateinit var app: AppController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)
        ButterKnife.bind(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Update Profile")

        app = application as AppController
        pref = PrefManager(applicationContext)


        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        val username = profile["name"]
        val mobile = profile["mobile"]
        val email = profile["email"]
        val imagePic = profile["profilepic"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]
        gender = profile["gender"]

        if (gender!!.equals("Male", ignoreCase = true)) {
            male!!.isChecked = true
            gender = "Male"
        } else if (gender!!.equals("Female", ignoreCase = true)) {
            female!!.isChecked = true
            gender = "Female"
        } else {
            male!!.isChecked = false
            female!!.isChecked = false
        }


        etUsername!!.setText(username)
        etPhone!!.setText(mobile)
        etEmail!!.setText(email)

        etUsername!!.addTextChangedListener(MyTextWatcher(etUsername!!))
        etPhone!!.addTextChangedListener(MyTextWatcher(etPhone!!))
        etEmail!!.addTextChangedListener(MyTextWatcher(etEmail!!))

        btnUpdate.setOnClickListener(this)
    }

    override fun onClick(view:  View?) {
        val isConnected = app.isConnection
        if (isConnected) {
            updateProfile()
        } else {

            val message = "Sorry! Not connected to internet"
            val color = Color.RED
            snackBar(message, color)
        }    }


    private fun updateProfile() {

        val name = etUsername!!.text!!.toString().trim { it <= ' ' }
        val mobile = etPhone!!.text!!.toString().trim { it <= ' ' }
        val email = etEmail!!.text!!.toString().trim { it <= ' ' }


        // get selected radio button from radioGroup
        val selectedId = radioGroup!!.checkedRadioButtonId

        // find the radiobutton by returned id
        radioButton = findViewById<View>(selectedId) as RadioButton
        val gender = radioButton.text.toString()

        if (!isValidName(name)) {
            return
        }
        if (!isValidPhoneNumber(mobile)) {
            return
        }
        if (!isValidEmail(email)) {
            return
        }


        progressBar!!.visibility = View.VISIBLE
        btnUpdate!!.isEnabled = false

        val call = RetrofitClient.instance!!.api.updateProfile(
            tokenValue,
            userId,
            name,
            email,
            mobile,
            gender
        )

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressBar!!.visibility = View.GONE
                btnUpdate!!.isEnabled = true

                val registerResponse = response.body()


                if (registerResponse!!.status.equals("10100", ignoreCase = true)) {

                    pref!!.createLogin(
                        userId, etUsername!!.text!!.toString(),
                        etEmail!!.text!!.toString(), etPhone!!.text!!.toString(),
                        null, deviceId,
                        tokenValue, gender
                    )



                    Handler().postDelayed({
                        val intent = Intent(applicationContext, MyAccountActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }, 1000)


                } else if (registerResponse.status == "10200") {
                    Toast.makeText(applicationContext, registerResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (registerResponse.status == "10300") {
                    Toast.makeText(applicationContext, registerResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (registerResponse.status == "10400") {
                    Toast.makeText(applicationContext, registerResponse.message, Toast.LENGTH_SHORT)
                        .show()
                }
                /* else{
                    int color = Color.RED;
                    snackBar(registerResponse.getMessage(), color);

                }*/


            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                btnUpdate!!.isEnabled = true
            }
        })
    }

    override fun onResume() {
        super.onResume()

        // register connection status listener

        AppController.instance!!.setConnectivityListener(this)
    }


    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String
        val color: Int
        if (isConnected) {
            message = "Good! Connected to Internet"
            color = Color.WHITE

        } else {
            message = "Sorry! Not connected to internet"
            color = Color.RED
        }

        snackBar(message, color)


    }


    // snackBar
    private fun snackBar(message: String, color: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    // validate name
    private fun isValidName(name: String): Boolean {
        val pattern = Pattern.compile("[a-zA-Z ]+")
        val matcher = pattern.matcher(name)

        if (name.isEmpty()) {
            user_til!!.error = "name is required"
            requestFocus(etUsername!!)
            return false
        } else if (!matcher.matches()) {
            user_til!!.error = "Enter Alphabets Only"
            requestFocus(etUsername!!)
            return false
        } else if (name.length < 5 || name.length > 20) {
            user_til!!.error = "Name Should be 5 to 20 characters"
            requestFocus(etUsername!!)
            return false
        } else {
            user_til!!.isErrorEnabled = false
        }
        return matcher.matches()
    }


    // validate phone
    private fun isValidPhoneNumber(mobile: String): Boolean {
        val pattern = Pattern.compile("^[9876]\\d{9}$")
        val matcher = pattern.matcher(mobile)

        if (mobile.isEmpty()) {
            mobile_til1!!.error = "Phone no is required"
            requestFocus(etPhone!!)

            return false
        } else if (!matcher.matches()) {
            mobile_til1!!.error = "Enter a valid mobile"
            requestFocus(etPhone!!)
            return false
        } else {
            mobile_til1!!.isErrorEnabled = false
        }

        return matcher.matches()
    }


    // validate your email address
    private fun isValidEmail(email: String): Boolean {
        val EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email)

        if (email.isEmpty()) {
            email_til1!!.error = "Email is required"
            requestFocus(etEmail!!)
            return false
        } else if (!matcher.matches()) {
            email_til1!!.error = "Enter a valid email"
            requestFocus(etEmail!!)
            return false
        } else {
            email_til1!!.isErrorEnabled = false
        }


        return matcher.matches()
    }


    // request focus
    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    // text input layout class
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.etUsername -> isValidName(etUsername!!.text!!.toString().trim { it <= ' ' })
                R.id.etPhone -> isValidPhoneNumber(etPhone!!.text!!.toString().trim { it <= ' ' })

                R.id.etEmail -> isValidEmail(etEmail!!.text!!.toString().trim { it <= ' ' })
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
