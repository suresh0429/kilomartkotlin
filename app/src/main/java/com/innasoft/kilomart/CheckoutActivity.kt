package com.innasoft.kilomart

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle

import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.PaymentTpyeRecyclerAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Interface.PaymentTypeInterface
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Storage.Utilities

import java.text.DecimalFormat
import java.text.ParseException

import butterknife.ButterKnife
import com.innasoft.kilomart.Response.*
import com.innasoft.kilomart.Storage.Utilities.df
import kotlinx.android.synthetic.main.activity_checkout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckoutActivity : AppCompatActivity(), PaymentTypeInterface {
    internal lateinit var paymentTypeInterface: PaymentTypeInterface


    private val price: Double = 0.00
    internal lateinit var appController: AppController


    internal lateinit var paymentTpyeRecyclerAdapter: PaymentTpyeRecyclerAdapter


    private var pDialog: ProgressDialog? = null
    private var pref: PrefManager? = null
    internal var userId: String? = null
    internal var addressid: String? = null
    internal var payment_id: String? = null
    internal var email: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var loc_area: String? = null
    internal var loc_pincode: String? = null
    internal var shippingCharges: String? = null
    internal var cartStatus: Boolean = false

    internal lateinit var txnId: String
    private var activity: AppCompatActivity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Checkout"

        appController = application as AppController
        paymentTypeInterface = this
        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        email = profile["email"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        // location Prefences
        val locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0)
        val locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false)
        loc_area = locationPref.getString(Utilities.KEY_AREA, "")
        loc_pincode = locationPref.getString(Utilities.KEY_PINCODE, "")
        shippingCharges = locationPref.getString(Utilities.KEY_SHIPPINGCHARGES, "")


        // transaction id
        txnId = System.currentTimeMillis().toString() + ""

        if (intent.extras != null) {
            addressid = intent.getStringExtra("addressId")
            cartStatus = intent.getBooleanExtra("Checkout", false)

        }


        if (appController.isConnection) {

            // Showing progress dialog before making http request
            pDialog = ProgressDialog(this)

            pDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            pDialog!!.isIndeterminate = true
            pDialog!!.setCancelable(false)
            pDialog!!.show()
            pDialog!!.setContentView(R.layout.my_progress)

            // showLocations();
            init()
            checkoutData()


        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            // app.internetDilogue(KitchenitemListActivity.this);

        }

    }

    private fun init() {
        activity = this@CheckoutActivity

        btnOrder!!.setOnClickListener { PaymentData() }
    }

    // checkout response
    private fun checkoutData() {

        val call = RetrofitClient.instance!!.api.checkOutStatus(tokenValue, userId, addressid)
        call.enqueue(object : Callback<CheckoutResponse> {
            override fun onResponse(
                call: Call<CheckoutResponse>,
                response: Response<CheckoutResponse>
            ) {

                hidePDialog()
                val stringResponse = response.body().toString()
                val cartResponse = response.body()
                println("$cartResponse")
                Log.d("TAG", "${cartResponse!!.data!!.mobile}")

                val addressBeans = cartResponse!!.data?.address
                val paymentGatewayBeans = cartResponse.data?.payment_gateway

                if (response.isSuccessful) {
                    if (cartResponse.status.equals("10100", ignoreCase = true)) {



                        txtSubTotal!!.text = "\u20B9" + cartResponse.data?.final_gross_amount
                        txtDiscount!!.text = "\u20B9" + cartResponse.data?.final_discount
                        txtGrandTotal!!.text = "\u20B9" + cartResponse.data?.payable_amount



                        if (cartResponse.data!!.address!!.size != 0) {


                            addressid = cartResponse.data!!.address!![0].id

                            // username and address
                            txtUserName!!.text = cartResponse.data!!.address!![0].name
                            txtAddress!!.text =
                                cartResponse.data!!.address!![0].address_line1 + "," + cartResponse.data!!.address!![0].address_line2 + "," + cartResponse.data!!.address!![0].area + "," + cartResponse.data!!.address!![0].city + "," +
                                        cartResponse.data!!.address!![0].state + "," + cartResponse.data!!.address!![0].pincode


                            showLocations(addressBeans!![0].pincode!!,
                                cartResponse.data!!.payable_amount?.toString()!!
                            )


                        } else {
                            txtAddress!!.text = "Please Add Address !! "
                            txtAddress!!.setTextColor(Color.RED)
                            txtAddress!!.textSize = 20f


                        }

                        // payment gateway array

                        val layoutManager = LinearLayoutManager(
                            applicationContext,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        recycler_paymentoptions!!.layoutManager = layoutManager
                        recycler_paymentoptions!!.itemAnimator = DefaultItemAnimator()

                        paymentTpyeRecyclerAdapter = PaymentTpyeRecyclerAdapter(
                            this@CheckoutActivity,
                            paymentGatewayBeans,
                            paymentTypeInterface
                        )
                        recycler_paymentoptions!!.adapter = paymentTpyeRecyclerAdapter


                        // Change Address
                        txtChangeAddress!!.setOnClickListener {
                            val intent =
                                Intent(this@CheckoutActivity, AddressListActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            intent.putExtra("Checkout", cartStatus)
                            activity!!.startActivity(intent)
                            activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }


                    } else if (cartResponse.status == "10200") {

                        Toast.makeText(applicationContext, cartResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    } else if (cartResponse.status == "10300") {

                        Toast.makeText(applicationContext, cartResponse.message, Toast.LENGTH_SHORT)
                            .show()


                    } else if (cartResponse.status == "10400") {

                        Toast.makeText(applicationContext, cartResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                }




            }

            override fun onFailure(call: Call<CheckoutResponse>, t: Throwable) {
                hidePDialog()
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })


    }

    override fun onItemClick(
        paymentGatewayBean: List<CheckoutResponse.DataBean.PaymentGatewayBean>?,
        position: Int
    ) {
        payment_id = paymentGatewayBean!![position].id.toString()
        // Toast.makeText(getApplicationContext(),""+paymentGatewayBean.get(position).getId(),Toast.LENGTH_SHORT).show();
    }

    private fun showLocations(pincode: String, payable_amount: String) {

        val call = RetrofitClient.instance!!.api.locations
        call.enqueue(object : Callback<LocationsResponse> {
            override fun onResponse(
                call: Call<LocationsResponse>,
                response: retrofit2.Response<LocationsResponse>
            ) {

                val addressResponse = response.body()

                if (response.isSuccessful) {


                    if (addressResponse!!.status.equals("10100", ignoreCase = true)) {


                        val locationItemsArray =
                            if (response.body() != null) response.body()!!.data else null

                        for (dataBean in locationItemsArray!!) {

                            if (pincode.equals(dataBean.pincode, ignoreCase = true)) {

                                Log.d(TAG, "onResponse: " + pincode + "-- " + dataBean.pincode)
                                Log.d(
                                    TAG,
                                    "onResponse2: " + pincode + "-- " + dataBean.shipping_charges
                                )


                                try {
                                    val payAmount = DecimalFormat.getNumberInstance().parse(payable_amount)!!.toDouble()

                                    if (payAmount > 1500) {

                                        val paybleAmount = payAmount + 0.00
                                        // txtDeliver.setText("\u20B9" + "0.00");
                                        txtDeliver!!.text = "Free Delivery"
                                        txtAmountTobepaid!!.text =
                                            "\u20B9" + df.format(paybleAmount)
                                        println(paybleAmount) //111111.23
                                    } else {
                                        val paybleAmount = payAmount + dataBean.shipping_charges!!
                                        txtDeliver!!.text =
                                            "\u20B9" + df.format(dataBean.shipping_charges!!)
                                        // txtAmountTobepaid!!.text = "\u20B9" + String.format("%.2f", paybleAmount).toDouble()
                                        txtAmountTobepaid!!.text =
                                            "\u20B9" + df.format(paybleAmount)
                                        println(paybleAmount) //111111.23
                                    }


                                } catch (e: ParseException) {
                                    e.printStackTrace()
                                }

                            }
                        }


                    }


                }

            }

            override fun onFailure(call: Call<LocationsResponse>, t: Throwable) {

            }
        })

    }

    private fun PaymentData() {

        if (addressid == null) {
            Snackbar.make(
                parentLayout!!,
                Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Choose Address" + "</font>"),
                Snackbar.LENGTH_SHORT
            ).show()

            //Toast.makeText(CheckoutActivity.this, "Please Choose Address ", Toast.LENGTH_SHORT).show();
            return
        }

        if (payment_id == null) {
            Snackbar.make(
                parentLayout!!,
                Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Select Payment method" + "</font>"),
                Snackbar.LENGTH_SHORT
            ).show()

            //Toast.makeText(CheckoutActivity.this, "Please Select Payment method ", Toast.LENGTH_SHORT).show();
            return
        }


        postOrder(addressid!!, payment_id!!)

    }


    // order post
    private fun postOrder(addressid: String, selectedId: String) {
        // Showing progress dialog before making http request
        pDialog = ProgressDialog(this)
        pDialog!!.setMessage("Loading..")
        pDialog!!.show()

        val call = RetrofitClient.instance!!.api.checkoutPost(
            tokenValue,
            userId,
            addressid,
            "",
            selectedId,
            "ANDROID"
        )
        call.enqueue(object : Callback<CheckoutPostResponse> {
            override fun onResponse(
                call: Call<CheckoutPostResponse>,
                response: Response<CheckoutPostResponse>
            ) {

                hidePDialog()

                val invoicePostResponse = response.body()

                if (response.isSuccessful) {

                    if (invoicePostResponse!!.status.equals("10100", ignoreCase = true)) {

                        showCustomDialog(
                            invoicePostResponse.order_id,
                            invoicePostResponse.message!!
                        )

                        // cart update
                        appController.cartCount(userId, deviceId)


                    } else if (invoicePostResponse.status == "10200") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.status == "10300") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        //Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (invoicePostResponse.status == "10400") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.status == "10500") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.status == "10600") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (invoicePostResponse.status == "10700") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.status == "10800") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (invoicePostResponse.status == "10900") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        //Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            override fun onFailure(call: Call<CheckoutPostResponse>, t: Throwable) {
                hidePDialog()
            }
        })
    }

    private fun showCustomDialog(order_id: Int, message: String) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false)


        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        val txtmessage = dialogView.findViewById<View>(R.id.txtMessage) as TextView
        txtmessage.text = message


        val buttonOk = dialogView.findViewById<View>(R.id.buttonOk) as Button
        buttonOk.setOnClickListener {
            val order_detail = Intent(this@CheckoutActivity, OrdersListActivity::class.java)
            order_detail.putExtra("Order_ID", order_id.toString())
            order_detail.putExtra("Checkout", cartStatus)
            order_detail.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(order_detail)
            finish()
        }

        //finally creating the alert dialog and displaying it
        val alertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    public override fun onDestroy() {
        super.onDestroy()
        hidePDialog()
    }

    private fun hidePDialog() {
        if (pDialog != null) {
            pDialog!!.dismiss()
            pDialog = null
        }
    }

    private fun ParseDouble(strNumber: String?): Double {
        return if (strNumber != null && strNumber.length > 0) {
            try {
                java.lang.Double.parseDouble(strNumber)
            } catch (e: Exception) {
                -1.00   // or some value to mark this field is wrong. or make a function validates field first ...
            }

        } else
            0.00
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        val TAG = "CheckoutActivity : "
    }
}
