package com.innasoft.kilomart

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Process
import android.preference.PreferenceManager
import android.provider.Settings

import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView

import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Response.LoginResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Storage.Utilities
import java.util.regex.Matcher
import java.util.regex.Pattern

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Header

class LoginActivity : AppCompatActivity(), View.OnClickListener,
    ConnectivityReceiver.ConnectivityReceiverListener {
    internal lateinit var session: PrefManager
    internal var doubleBackToExitPressedOnce = false
    internal var snackbar: Snackbar? = null


    internal lateinit var deviceID: String
    internal lateinit var fcmToken: String

    internal lateinit var app: AppController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        app = application as AppController

        session = PrefManager(this)
        if (session.isLoggedIn) {
            startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
            finish()
        }

        deviceID = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )

        val next = "Not a member ? <font color='#f89113'>SignUp</font> now"
        txtSignup!!.text = Html.fromHtml(next)

        etEmail!!.addTextChangedListener(MyTextWatcher(etEmail!!))
        etPassword!!.addTextChangedListener(MyTextWatcher(etPassword!!))


        // fcm Token
        FirebaseInstanceId.getInstance()
            .instanceId.addOnSuccessListener(this@LoginActivity) { instanceIdResult ->
            fcmToken = instanceIdResult.token
            Log.e("TOKEN", "" + fcmToken)
        }

        btnLogin.setOnClickListener(this)
        txtForgotPassword.setOnClickListener(this)
        txtSignup.setOnClickListener(this)

    }


    /* Validating form
     */
    private fun submitForm() {

        if (!isValidEmail(etEmail!!.text!!.toString().trim { it <= ' ' })) {
            return
        }

        if (!validatePassword()) {
            return
        }

        userLogin()

    }

    // retrofit login
    private fun userLogin() {


        val email = etEmail!!.text!!.toString().trim { it <= ' ' }
        val password = etPassword!!.text!!.toString().trim { it <= ' ' }


        progressBar!!.visibility = View.VISIBLE
        btnLogin!!.isEnabled = false

        val call = RetrofitClient.instance!!.api.userLogin(email, password, deviceID)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {


                progressBar!!.visibility = View.GONE

                btnLogin!!.isEnabled = true

                val loginResponse = response.body()


                if (loginResponse!!.status == "10100") {

                    if (loginResponse.data!!.mobile_verify_status.equals("0", ignoreCase = true)) {

                        val intent = Intent(this@LoginActivity, VerifyOtpActivity::class.java)
                        intent.putExtra("MobileNumber", loginResponse.data!!.mobile)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)

                        sendOtp(loginResponse.data!!.mobile!!)
                    } else {

                        Toast.makeText(
                            this@LoginActivity,
                            loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()

                        session.createLogin(
                            loginResponse.data!!.user_id,
                            loginResponse.data!!.user_name,
                            loginResponse.data!!.email,
                            loginResponse.data!!.mobile,
                            null, deviceID,
                            loginResponse.data!!.jwt,
                            loginResponse.data!!.gender
                        )

                        // location preferences
                        setDefaults()

                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)

                        updateFcmToken(loginResponse.data!!.jwt!!, loginResponse.data!!.user_id!!)

                    }


                } else if (loginResponse.status == "10200") {
                    Toast.makeText(this@LoginActivity, loginResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (loginResponse.status == "10300") {
                    Toast.makeText(this@LoginActivity, loginResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (loginResponse.status == "10400") {
                    Toast.makeText(this@LoginActivity, loginResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val color = Color.RED
                    snackBar(loginResponse.message!!, color)

                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                btnLogin!!.isEnabled = true
            }
        })
    }


    private fun sendOtp(mobile: String) {
        val call1 = RetrofitClient.instance!!.api.ResendOtpRequest(mobile)
        call1.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                if (response.isSuccessful);
                val resendOtpResponse = response.body()

                if (resendOtpResponse!!.status == "10100") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@LoginActivity,
                        resendOtpResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (resendOtpResponse.status == "10200") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@LoginActivity,
                        resendOtpResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (resendOtpResponse.status == "10300") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@LoginActivity,
                        resendOtpResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (resendOtpResponse.status == "10400") {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(
                        this@LoginActivity,
                        resendOtpResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Toast.makeText(this@LoginActivity, t.message, Toast.LENGTH_SHORT).show()


            }
        })
    }

    // update Fcm Token
    private fun updateFcmToken(jwt: String, user_id: String) {

        val call = RetrofitClient.instance!!.api.updateFcmTocken(jwt, user_id, fcmToken)
        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {


                progressBar!!.visibility = View.GONE


                val loginResponse = response.body()


                if (loginResponse!!.status == "10100") {

                    Log.d(TAG, "onResponse: " + loginResponse.message)


                } else if (loginResponse.status == "10200") {
                    Log.d(TAG, "onResponse: " + loginResponse.message)
                } else if (loginResponse.status == "10300") {
                    Log.d(TAG, "onResponse: " + loginResponse.message)
                } else if (loginResponse.status == "10400") {
                    Log.d(TAG, "onResponse: " + loginResponse.message)
                } else {
                    val color = Color.RED
                    snackBar(loginResponse.message!!, color)

                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                btnLogin!!.isEnabled = true
            }
        })
    }


    fun setDefaults() {
        val preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0)
        val editor = preferences.edit()
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true)
        editor.putString(Utilities.KEY_AREA, "")
        editor.putString(Utilities.KEY_PINCODE, "")
        editor.putString(Utilities.KEY_SHIPPINGCHARGES, "")
        editor.apply()
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.txtForgotPassword -> {
                val intent2 = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
                intent2.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent2)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
            R.id.btnLogin -> {

                // Method to manually check connection status
                val isConnected = app.isConnection

                if (isConnected) {
                    submitForm()
                } else {

                    val message = "Sorry! Not connected to internet"
                    val color = Color.RED

                    snackBar(message, color)
                    //showSnack(isConnected);
                }
            }
            R.id.txtSignup -> {
                val intent1 = Intent(this@LoginActivity, RegisterActivity::class.java)
                intent1.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent1)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // register connection status listener

        AppController.instance!!.setConnectivityListener(this)
    }

    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String
        val color: Int
        if (isConnected) {
            message = "Good! Connected to Internet"
            color = Color.WHITE

        } else {
            message = "Sorry! Not connected to internet"
            color = Color.RED
        }

        snackBar(message, color)


    }

    // snackBar
    private fun snackBar(message: String, color: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    // validate phone
    //    private boolean isValidPhoneNumber(String mobile) {
    //        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
    //        Matcher matcher = pattern.matcher(mobile);
    //
    //        if (mobile.isEmpty()) {
    //            tilPhone.setError("Phone no is required");
    //            requestFocus(etPhone);
    //            return false;
    //        } else if (!matcher.matches()) {
    //            tilPhone.setError("Enter a valid mobile");
    //            requestFocus(etPhone);
    //            return false;
    //        } else {
    //            tilPhone.setErrorEnabled(false);
    //        }
    //
    //        return matcher.matches();
    //    }

    // validate password
    private fun validatePassword(): Boolean {
        if (etPassword!!.text!!.toString().trim { it <= ' ' }.isEmpty()) {
            tilPassword!!.error = "Password required"
            requestFocus(etPassword!!)
            return false
        } else if (etPassword!!.length() < 6) {
            tilPassword!!.error = "Password should be atleast 6 character long"
            requestFocus(etPassword!!)
            return false
        } else {
            tilPassword!!.isErrorEnabled = false
        }

        return true
    }

    // request focus
    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    // text input layout class
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.etPhone -> isValidEmail(etEmail!!.text!!.toString().trim { it <= ' ' })
                R.id.etPassword -> validatePassword()
            }
        }
    }

    private fun isValidEmail(email: String): Boolean {
        val EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email)

        if (email.isEmpty()) {
            tilEmail!!.error = "Email is required"
            requestFocus(etEmail!!)
            return false
        } else if (!matcher.matches()) {
            tilEmail!!.error = "Enter a valid email"
            requestFocus(etEmail!!)
            return false
        } else {
            tilEmail!!.isErrorEnabled = false
        }


        return matcher.matches()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            moveTaskToBack(true)
            Process.killProcess(Process.myPid())
            System.exit(1)
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)


    }

    companion object {
        private val TAG = "LoginActivity"
        private val PREFS_LOCATION = "LOCATION_PREF"
    }
}