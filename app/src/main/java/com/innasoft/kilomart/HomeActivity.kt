package com.innasoft.kilomart

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Process
import android.provider.Settings

import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.bumptech.glide.Glide
import com.github.demono.AutoScrollViewPager
import com.google.android.gms.location.LocationRequest
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.AutoViewPager
import com.innasoft.kilomart.Adapters.HomeAdapter
import com.innasoft.kilomart.Adapters.LocationAdapter
import com.innasoft.kilomart.Adapters.PopularProductAdaptre
import com.innasoft.kilomart.Adapters.SearchAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Helper.Converter
import com.innasoft.kilomart.Model.LocationItem
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.AppVersionResponse
import com.innasoft.kilomart.Response.BannersResponse
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Response.HomeResponse
import com.innasoft.kilomart.Response.LocationsResponse
import com.innasoft.kilomart.Response.ProductResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Storage.Utilities
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

import java.util.ArrayList
import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.ABOUT_US_URL
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.PRIVACY_POLICY_URL
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.TERMS_CONDITIONS_URL
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


import com.karumi.dexter.Dexter.withActivity
import kotlinx.android.synthetic.main.content_home.*

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    ConnectivityReceiver.ConnectivityReceiverListener {
    internal var doubleBackToExitPressedOnce = false
    internal lateinit var connectivityReceiver: ConnectivityReceiver
    internal var color = Color.RED
    internal lateinit var appController: AppController



    private var pref: PrefManager? = null
    internal var snackbar: Snackbar? = null
    internal var cartindex: Int = 0
    internal var userId: String? = null
    internal var imagePic: String? = null
    internal var deviceId: String? = null
    internal var tokenValue: String? = null
    internal var loc_area: String? = null
    internal lateinit var checnkVeriosn: String
    internal lateinit var android_version: String
    internal lateinit var nav_Image: ImageView
    internal var title = ""
    internal lateinit var nav_username: TextView
    internal lateinit var nav_useremail: TextView

    //    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private val firebaseDefaultMap: HashMap<String, Any>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setLogo(R.drawable.kilomartlogo)
        toolbar.contentInsetStartWithNavigation = 0
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.title = null
        supportActionBar!!.setDisplayShowTitleEnabled(false)


        //LandScape mode
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

        appController = application as AppController
        pref = PrefManager(applicationContext)

        // Method to manually check connection status
        val isConnected = appController.isConnection
        //showSnack(isConnected);


        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        val hView = navigationView.getHeaderView(0)
        nav_username = hView.findViewById<View>(R.id.userName) as TextView
        nav_useremail = hView.findViewById<View>(R.id.userEmail) as TextView
        nav_Image = hView.findViewById<View>(R.id.imageView) as ImageView


        // search Layout
        val searchLayout = findViewById<View>(R.id.searchLayout) as TextView
        searchLayout.setOnClickListener {
            val intent = Intent(this@HomeActivity, SearchActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }

        val mLayoutManager = GridLayoutManager(applicationContext, 2)
        homeRecycler!!.layoutManager = mLayoutManager
        homeRecycler!!.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(10), true))
        homeRecycler!!.itemAnimator = DefaultItemAnimator()

        val mLayoutManager1 = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        popularRecycler!!.layoutManager = mLayoutManager1
        popularRecycler!!.itemAnimator = DefaultItemAnimator()


        if (isConnected) {

            simpleSwipeRefreshLayout!!.setColorSchemeResources(
                R.color.colorAccent,
                R.color.colorBlue,
                R.color.colorPrimary
            )
            simpleSwipeRefreshLayout!!.setOnRefreshListener {
                userData()
                cartCount()
                prepareHomeBannersData()
                prepareHomeData()
                popularData()
                locationShowFirsttime()

                simpleSwipeRefreshLayout!!.isRefreshing = false
            }

            userData()
            cartCount()
            prepareHomeBannersData()
            prepareHomeData()
            popularData()
            locationShowFirsttime()

            CheckappUpdate()


        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }


        }


    }


    // user information
    private fun userData() {
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        val username = profile["name"]
        val email = profile["email"]
        imagePic = profile["profilepic"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        nav_useremail.text = email
        nav_username.text = username

        if (imagePic != null && !imagePic!!.isEmpty() && imagePic != "null") {
            // loading album cover using Glide library
            Glide.with(applicationContext).load(imagePic).into(nav_Image)
        } else {

            nav_Image.setImageResource(R.drawable.ic_user)
        }

    }

    // cart count
    private fun cartCount() {


        appController.cartCount(userId, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindex", "" + cartindex)
        invalidateOptionsMenu()


    }

    // location Prefences
    private fun locationShowFirsttime() {
        //location prefences
        val locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0)
        val locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false)
        loc_area = locationPref.getString(Utilities.KEY_AREA, "")


        if (locationPrefBoolean) {
            showLocationsDialog()
        }
    }

    // home banners
    private fun prepareHomeBannersData() {

        simpleSwipeRefreshLayout!!.isRefreshing = true

        val call = RetrofitClient.instance!!.api.homeBanners
        call.enqueue(object : Callback<BannersResponse> {
            override fun onResponse(
                call: Call<BannersResponse>,
                response: Response<BannersResponse>
            ) {
                val bannersResponse = response.body()

                if (response.isSuccessful) {
                    simpleSwipeRefreshLayout!!.isRefreshing = false

                    if (bannersResponse!!.status.equals("10100", ignoreCase = true)) {


                        val homeBannersBeanList = response.body()!!.data

                        val mAdapter = AutoViewPager(homeBannersBeanList, this@HomeActivity)
                        viewPager!!.adapter = mAdapter
                        // optional start auto scroll
                        viewPager!!.startAutoScroll()


                    } else if (bannersResponse.status == "10200") {

                        Toast.makeText(
                            applicationContext,
                            bannersResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (bannersResponse.status == "10300") {

                        Toast.makeText(
                            applicationContext,
                            bannersResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (bannersResponse.status == "10400") {

                        Toast.makeText(
                            applicationContext,
                            bannersResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }


            }

            override fun onFailure(call: Call<BannersResponse>, t: Throwable) {
                simpleSwipeRefreshLayout!!.isRefreshing = false
                Snackbar.make(
                    homeRecycler!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()


            }
        })

    }

    // home modules
    private fun prepareHomeData() {

        simpleSwipeRefreshLayout!!.isRefreshing = true

        val call = RetrofitClient.instance!!.api.getHomePageRequest("main", "", "")
        call.enqueue(object : Callback<HomeResponse> {
            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                val addressResponse = response.body()

                if (response.isSuccessful) {
                    simpleSwipeRefreshLayout!!.isRefreshing = false

                    if (addressResponse!!.status.equals("10100", ignoreCase = true)) {

                        // modules
                        val modulesBeanList = response.body()!!.data


                        val adapter = HomeAdapter(applicationContext, modulesBeanList!!.toList())
                        homeRecycler!!.adapter = adapter


                    } else if (addressResponse.status == "10200") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (addressResponse.status == "10300") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (addressResponse.status == "10400") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }


            }

            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                simpleSwipeRefreshLayout!!.isRefreshing = false
                Snackbar.make(
                    homeRecycler!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()


            }
        })

    }


    // popular products
    private fun popularData() {

        simpleSwipeRefreshLayout!!.isRefreshing = true

        val call =
            RetrofitClient.instance!!.api.getPopularProduct("SINGLE", deviceId, "POPULARITY")
        //Call<ProductResponse> call = RetrofitClient.getInstance().getApi().getPopularProduct("SINGLE", deviceId, "dal");
        call.enqueue(object : Callback<ProductResponse> {
            override fun onResponse(
                call: Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
                val addressResponse = response.body()

                if (response.isSuccessful) {
                    simpleSwipeRefreshLayout!!.isRefreshing = false

                    if (addressResponse!!.status.equals("10100", ignoreCase = true)) {

                        // modules
                        val modulesBeanList = response.body()!!.data


                        val adapter = PopularProductAdaptre(applicationContext, modulesBeanList!!.toList())
                        popularRecycler!!.adapter = adapter


                    } else if (addressResponse.status == "10200") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (addressResponse.status == "10300") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (addressResponse.status == "10400") {

                        Toast.makeText(
                            applicationContext,
                            addressResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }

            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                simpleSwipeRefreshLayout!!.isRefreshing = false
                Snackbar.make(
                    popularRecycler!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()


            }
        })

    }

    private fun Logout() {
        val call = RetrofitClient.instance!!.api.userLogout(tokenValue, userId, deviceId)
        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                if (response.isSuccessful);
                val baseResponse = response.body()

                if (baseResponse!!.status == "10100") {

                    val preferences = applicationContext.getSharedPreferences("LOCATION_PREF", 0)
                    val editor = preferences.edit()
                    editor.apply()
                    editor.clear()

                    pref!!.clearSession()
                    //Logout
                    val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()

                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()


                } else if (baseResponse.status == "10200") {

                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (baseResponse.status == "10300") {

                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (baseResponse.status == "10400") {

                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()

            }
        })
    }

    private fun CheckappUpdate() {


        var eInfo: PackageInfo? = null
        try {
            eInfo = packageManager.getPackageInfo("com.innasoft.kilomart", 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        checnkVeriosn = eInfo!!.versionName

        pref = PrefManager(applicationContext)

        val profile = pref!!.userDetails
        tokenValue = profile["AccessToken"]


        val call = RetrofitClient.instance!!.api.CheckAppUpdate(tokenValue)
        call.enqueue(object : Callback<AppVersionResponse> {
            override fun onResponse(
                call: Call<AppVersionResponse>,
                response: Response<AppVersionResponse>
            ) {
                if (response.isSuccessful);
                val appVersionResponse = response.body()
                if (appVersionResponse!!.status == "10100") {

                    android_version = appVersionResponse.data!!.androidVersion!!

                    Log.d(TAG, "onResponse: $android_version---$checnkVeriosn")

                    if (!android_version.equals(checnkVeriosn, ignoreCase = true)) {

                        val builder = AlertDialog.Builder(this@HomeActivity)
                        builder.setTitle("Update the App")
                            .setMessage("A new version of this app is available on play store. if you update click ok")
                            .setPositiveButton(
                                "OK"
                            ) { dialog, which ->
                                val marketUri =
                                    Uri.parse("https://play.google.com/store/apps/details?id=com.innasoft.kilomart&hl=en")
                                startActivity(Intent(Intent.ACTION_VIEW, marketUri))
                            }.setNegativeButton("Later") { dialog, which ->
                                builder.setCancelable(true)
                            }.setCancelable(true).show()
                    }

                } else if (appVersionResponse.status == "10200") {
                    Toast.makeText(
                        this@HomeActivity,
                        appVersionResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (appVersionResponse.status == "10300") {
                    Toast.makeText(
                        this@HomeActivity,
                        appVersionResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }

            override fun onFailure(call: Call<AppVersionResponse>, t: Throwable) {
                Toast.makeText(this@HomeActivity, t.message, Toast.LENGTH_SHORT).show()

            }
        })

    }


    override fun onResume() {
        super.onResume()

        userData()
        cartCount()
        locationShowFirsttime()

        // receiver function
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")

        connectivityReceiver = ConnectivityReceiver()
        registerReceiver(connectivityReceiver, intentFilter)

        AppController.instance!!.setConnectivityListener(this)

    }

    override fun onRestart() {
        super.onRestart()

        userData()
        cartCount()
        locationShowFirsttime()

    }

    override fun onStart() {
        super.onStart()

        userData()
        cartCount()
        locationShowFirsttime()

    }

    override fun onStop() {
        unregisterReceiver(connectivityReceiver)
        super.onStop()
    }


    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {

        if (!isConnected) {
            Snackbar.make(
                homeRecycler!!,
                Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.Sorry_Notconnected_to_internet) + "</font>"),
                Snackbar.LENGTH_SHORT
            ).show()


        } /*else {

            Snackbar.make(homeRecycler, Html.fromHtml("<font color=\""+Color.WHITE+"\">"+ getResources().getString(R.string.goodinternet)+"</font>"),Snackbar.LENGTH_SHORT).show();

        }*/

    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds countries to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)

        val alertMenuItem = menu.findItem(R.id.action_location)
        val rootView = alertMenuItem.actionView as RelativeLayout
        val txtArea = rootView.findViewById<View>(R.id.txt_area) as TextView
        txtArea.text = loc_area

        rootView.setOnClickListener { onOptionsItemSelected(alertMenuItem) }


        val menuItem = menu.findItem(R.id.action_cart)

        menuItem.icon =
            Converter.convertLayoutToImage(
                this@HomeActivity,
                cartindex,
                R.drawable.ic_actionbar_bag
            )
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_cart) {

            val intent = Intent(applicationContext, CartActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

            return true
        } else if (id == R.id.action_location) {
            showLocationsDialog()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_account) {
            //My Account
            val intent = Intent(this@HomeActivity, MyAccountActivity::class.java)
            intent.putExtra("Checkout", false)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

        } else if (id == R.id.My_orders) {
            //My orders
            val intent = Intent(this@HomeActivity, OrderIdActvity::class.java)
            intent.putExtra("Checkout", false)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

        } else if (id == R.id.action_notification) {

            val intent = Intent(applicationContext, NotificationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

        } else if (id == R.id.nav_logout) {

            Logout()

        } else if (id == R.id.nav_wishlist) {

            //            Intent intent = new Intent(getApplicationContext(), WishListActivity.class);
            //            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            //            startActivity(intent);
            //            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_share) {

            val sAux =
                "Install Kilomart application click below link \n" + "https://play.google.com/store/apps/details?id=com.innasoft.kilomart"
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, sAux)
            sendIntent.type = "text/plain"

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)

        }   /* else if (id == R.id.nav_support) {


            Intent intent = new Intent(getApplicationContext(), SupportUsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }*/
        else if (id == R.id.nav_privacy) {

            title = "Privacy & Policy"
            val intent = Intent(applicationContext, PdfActivity::class.java)
            intent.putExtra("title", title)
            intent.putExtra("url", PRIVACY_POLICY_URL)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        } else if (id == R.id.nav_terms) {

            title = "Terms & Conditions"
            val intent = Intent(applicationContext, PdfActivity::class.java)
            intent.putExtra("title", title)
            intent.putExtra("url", TERMS_CONDITIONS_URL)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        } else if (id == R.id.nav_return_policy) {

            title = "Return Policy"
            val intent = Intent(applicationContext, PdfActivity::class.java)
            intent.putExtra("title", title)
            intent.putExtra("url", "")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        } else if (id == R.id.nav_faq) {

            title = "FAQ's"
            val intent = Intent(applicationContext, PdfActivity::class.java)
            intent.putExtra("title", title)
            intent.putExtra("url", "")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        } else if (id == R.id.nav_aboutus) {

            title = "About Us"
            val intent = Intent(applicationContext, PdfActivity::class.java)
            intent.putExtra("title", title)
            intent.putExtra("url", ABOUT_US_URL)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }


        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }


    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed()
                    moveTaskToBack(true)
                    Process.killProcess(Process.myPid())
                    System.exit(1)
                    return
                }

                this.doubleBackToExitPressedOnce = true
                Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show()

                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)


            }
        }
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    inner class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left =
                    spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right =
                    (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right =
                    spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                r.displayMetrics
            )
        )
    }


    private fun showLocationsDialog() {

        val call = RetrofitClient.instance!!.api.locations
        call.enqueue(object : Callback<LocationsResponse> {
            override fun onResponse(
                call: Call<LocationsResponse>,
                response: retrofit2.Response<LocationsResponse>
            ) {

                val addressResponse = response.body()

                if (response.isSuccessful) {


                    if (addressResponse!!.status.equals("10100", ignoreCase = true)) {


                        val locationItemsArray =
                            if (response.body() != null) response.body()!!.data else null

                        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
                        val viewGroup = findViewById<ViewGroup>(android.R.id.content)

                        //then we will inflate the custom alert dialog xml that we created
                        val dialogView = LayoutInflater.from(this@HomeActivity)
                            .inflate(R.layout.my_location_lis_dialog, viewGroup, false)

                        //Now we need an AlertDialog.Builder object
                        val builder = AlertDialog.Builder(this@HomeActivity)

                        //setting the view of the builder to our custom view that we already inflated
                        builder.setView(dialogView)

                        //finally creating the alert dialog and displaying it
                        val alertDialog = builder.create()
                        alertDialog.setCancelable(true)
                        alertDialog.show()


                        val recyclerView =
                            dialogView.findViewById<View>(R.id.locationRecycler) as RecyclerView
                        val mLayoutManager1 = LinearLayoutManager(
                            this@HomeActivity,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        recyclerView.layoutManager = mLayoutManager1
                        recyclerView.itemAnimator = DefaultItemAnimator()

                        val adapter =
                            LocationAdapter(applicationContext, locationItemsArray!!, alertDialog)
                        recyclerView.adapter = adapter
                    }
                }
            }

            override fun onFailure(call: Call<LocationsResponse>, t: Throwable) {

                Snackbar.make(
                    popularRecycler!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })

    }

    companion object {
        private val TAG = "HomeActivity"
        private val VERSION_CODE_KEY = "app_latest_version"
    }


}
