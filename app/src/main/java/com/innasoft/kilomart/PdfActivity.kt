package com.innasoft.kilomart

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar

import androidx.appcompat.app.AppCompatActivity

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_pdf.*

class PdfActivity : AppCompatActivity() {

    internal lateinit var webSettings: WebSettings
    internal var title: String? = null
    internal var url: String? = null



    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf)
        ButterKnife.bind(this)

        if (intent.extras != null) {
            title = intent.getStringExtra("title")
            url = intent.getStringExtra("url")
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = title

        if (title!!.equals("Return Policy", ignoreCase = true) ||
            title!!.equals("FAQ's", ignoreCase = true)
            || url!!.equals("", ignoreCase = true)
        ) {
            webView!!.visibility = View.GONE
            txtAlert!!.visibility = View.VISIBLE
            progress!!.visibility = View.GONE
        } else {

            // Load data into a WebView
            webSettings = webView!!.settings
            webSettings.javaScriptEnabled = true
            webView!!.webViewClient = WebViewClient()
            webView!!.loadUrl(url)
        }


    }

    inner class WebViewClient : android.webkit.WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)

            /*** Hide ProgressBar while page completely load  */
            progress!!.visibility = View.GONE

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)

    }

}

