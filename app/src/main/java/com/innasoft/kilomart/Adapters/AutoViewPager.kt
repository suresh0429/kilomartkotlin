package com.innasoft.kilomart.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.demono.adapter.InfinitePagerAdapter
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.IMAGE_BASE_URL
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.BannersResponse
import com.innasoft.kilomart.Response.HomeResponse


class AutoViewPager(
    private val data: List<BannersResponse.DataBean>?,
    private val context: Context
) : InfinitePagerAdapter() {

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun getItemView(position: Int, convertView: View?, container: ViewGroup): View {
        var convertView = convertView
        val homeBannersBean = data!![position]
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView =
                LayoutInflater.from(context).inflate(R.layout.banner_card, container, false)
        }
        // Lookup view for data population
        val tvDiscription = convertView!!.findViewById<View>(R.id.txtDescription) as TextView
        val imageView = convertView.findViewById<View>(R.id.imageView) as ImageView
        // Populate the data into the template view using the data object
        tvDiscription.text = homeBannersBean.highlighted_text
        Glide.with(context).load(IMAGE_BASE_URL + homeBannersBean.image!!).skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder)
            .into(imageView)
        // Return the completed view to render on screen
        return convertView
    }
}
