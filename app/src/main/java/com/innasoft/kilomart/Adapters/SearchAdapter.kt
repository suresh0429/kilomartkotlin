package com.innasoft.kilomart.Adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide

import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.PRODUCT_IMAGE_BASE_URL2
import com.innasoft.kilomart.ProductDetailsActivity
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.ProductResponse
import com.innasoft.kilomart.Response.SearchResponse
import com.innasoft.kilomart.Storage.Utilities

import java.text.SimpleDateFormat
import java.util.Date
import java.util.StringTokenizer

import com.innasoft.kilomart.Storage.Utilities.capitalize

class SearchAdapter(
    private val mContext: Context,
    private val homeList: List<ProductResponse.DataBean>
) : RecyclerView.Adapter<SearchAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var thumbnail: ImageView
        var txtPrice: TextView
        var txtName: TextView
        var txtDiscountPrice: TextView
        var txtDiscountTag: TextView
        var product_unit: TextView
        //  public RatingBar ratingBar;
        var tagLayout: LinearLayout
        var shadowImageView: LinearLayout
        var linearLayout: CardView

        init {

            thumbnail = view.findViewById<View>(R.id.thumbnail) as ImageView
            txtPrice = view.findViewById<View>(R.id.txtproductPrice) as TextView
            product_unit = view.findViewById<View>(R.id.product_unit) as TextView
            txtName = view.findViewById<View>(R.id.txtproductName) as TextView
            txtDiscountPrice = view.findViewById<View>(R.id.txtDiscountPrice) as TextView
            linearLayout = view.findViewById<View>(R.id.parentLayout) as CardView
            tagLayout = view.findViewById<View>(R.id.tagLayout) as LinearLayout
            shadowImageView = view.findViewById<View>(R.id.shadowImageView) as LinearLayout
            txtDiscountTag = view.findViewById<View>(R.id.txtDiscountTag) as TextView


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.products_item_card, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val home = homeList[position]

        if (home.availability!!.equals("1", ignoreCase = true)) {
            holder.shadowImageView.visibility = View.VISIBLE
        } else {
            holder.shadowImageView.visibility = View.GONE

        }

        // loading album cover using Glide library
        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2 + home.images!![0])
            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder)
            .into(holder.thumbnail)
        Log.d("ADAPTERIMASGE", "onBindViewHolder: " + PRODUCT_IMAGE_BASE_URL2 + home.images!![0])
        holder.txtName.text = capitalize(home.product_name)
        holder.txtDiscountPrice.text = "\u20B9" + Utilities.df.format(home.selling_price!!)

        holder.txtPrice.text = "\u20B9" + Utilities.df.format( home.mrp_price!!)
        holder.txtPrice.paintFlags = holder.txtPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        holder.txtPrice.setTextColor(Color.RED)

        holder.product_unit.text = "" + home.unit_value + home.unit_name

        //  holder.ratingBar.setRating(Float.parseFloat(home.getRating()));
        holder.linearLayout.setOnClickListener {
            //                    Activity activity = (Activity) mContext;


            val intent = Intent(mContext, ProductDetailsActivity::class.java)
            intent.putExtra("productId", home.id)
            intent.putExtra("productName", home.product_name)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)
            // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return homeList.size
    }


}
