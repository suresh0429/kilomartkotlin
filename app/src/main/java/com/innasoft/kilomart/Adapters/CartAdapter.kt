package com.innasoft.kilomart.Adapters

import android.content.Context
import android.graphics.Color
import android.graphics.Paint

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.innasoft.kilomart.Interface.CartProductClickListener
import com.innasoft.kilomart.Model.Product
import com.innasoft.kilomart.R

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.PRODUCT_IMAGE_BASE_URL2
import com.innasoft.kilomart.Storage.Utilities


import com.innasoft.kilomart.Storage.Utilities.capitalize
import kotlinx.android.synthetic.main.cart_product.view.*

class CartAdapter(
    private val mContext: Context,
    private val productsModelList: List<Product>,
    private val productClickListener: CartProductClickListener
) : RecyclerView.Adapter<CartAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val thumbnail1 = itemView.thumbnail1
        val product_name = itemView.product_name
        val product_Price = itemView.product_Price
        val final_product_Price = itemView.final_product_Price
        val product_quantity = itemView.product_quantity
        val product_minus = itemView.product_minus
        val product_plus = itemView.product_plus
        val btnFavourite = itemView.btnFavourite
        val linearSaveItem = itemView.linearSaveItem
        val linearremove = itemView.linearremove
        val img_remove = itemView.img_remove
        val product_unit = itemView.product_unit

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.cart_product, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val productsModel = productsModelList[position]

        // loading album cover using Glide library
        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2 + productsModel.images!!)
            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder)
            .into(holder.thumbnail1)


        holder.product_name!!.text = capitalize(productsModel.product_name)
        holder.product_quantity!!.text = "" + productsModel.purchase_quantity
        holder.product_unit!!.text = "" + productsModel.unit_value + productsModel.unit_name

        holder.product_Price!!.text =
            mContext.resources.getString(R.string.Rs) + Utilities.df.format(productsModel.mrp_price!!)
        holder.product_Price!!.paintFlags =
            holder.product_Price!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        holder.product_Price!!.setTextColor(Color.RED)

        holder.final_product_Price!!.text =
            mContext.resources.getString(R.string.Rs) + Utilities.df.format(productsModel.selling_price!!)


        holder.product_minus!!.setOnClickListener { productClickListener.onMinusClick(productsModel) }

        holder.product_plus!!.setOnClickListener { productClickListener.onPlusClick(productsModel) }

        //        holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View view) {
        //
        //                productClickListener.onWishListDialog(productsModel);
        //            }
        //        });

        holder.img_remove!!.setOnClickListener { productClickListener.onRemoveDialog(productsModel) }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return productsModelList.size
    }

}