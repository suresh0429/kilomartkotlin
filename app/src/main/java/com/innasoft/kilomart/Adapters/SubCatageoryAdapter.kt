package com.innasoft.kilomart.Adapters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide

import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.IMAGE_BASE_URL
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.HomeResponse
import com.innasoft.kilomart.ProductListActivity

import de.hdodenhof.circleimageview.CircleImageView

import com.innasoft.kilomart.Storage.Utilities.capitalize

class SubCatageoryAdapter(
    private val mContext: Context,
    private val homeList: List<HomeResponse.DataBean>,
    private val catId: String
) : RecyclerView.Adapter<SubCatageoryAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var thumbnail: CircleImageView
        var txtName: TextView
        var linearLayout: LinearLayout


        init {

            thumbnail = view.findViewById<View>(R.id.thumbnail) as CircleImageView
            txtName = view.findViewById<View>(R.id.txtCatName) as TextView
            linearLayout = view.findViewById<View>(R.id.parentLayout) as LinearLayout


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.subcat_card, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val home = homeList[position]

        // loading album cover using Glide library
        Glide.with(mContext).load(IMAGE_BASE_URL + home.image!!).skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.thumbnail)

        holder.txtName.text = capitalize(home.name)

        holder.linearLayout.setOnClickListener {
            //                    Activity activity = (Activity) mContext;
            val intent = Intent(mContext, ProductListActivity::class.java)
            intent.putExtra("catId", catId)
            intent.putExtra("title", home.name)
            intent.putExtra("subcatId", home.id)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)
            // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            /* SharedPreferences preferences1 = mContext.getSharedPreferences("SORT", 0);
                SharedPreferences.Editor editor = preferences1.edit();
                editor.putString("catId", home.getCategory_id());
                editor.putString("title", home.getTitle());
                editor.putString("subcatId", home.getId());
                editor.putString("sort", "Price High to Low");
                editor.apply();*/
        }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return homeList.size
    }


}