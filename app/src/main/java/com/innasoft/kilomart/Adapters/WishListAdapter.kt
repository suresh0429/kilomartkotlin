package com.innasoft.kilomart.Adapters

import android.app.Dialog
import android.content.Context
import android.content.Intent

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Apis.RetrofitClient


import com.innasoft.kilomart.ProductDetailsActivity
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.WishListDeleteresponse
import com.innasoft.kilomart.Response.WishlistResponse
import com.innasoft.kilomart.Storage.Utilities
import com.innasoft.kilomart.WishListActivity


import java.text.SimpleDateFormat
import java.util.Date

import retrofit2.Call
import retrofit2.Callback

import com.innasoft.kilomart.Storage.Utilities.capitalize

class WishListAdapter(
    private val mContext: Context,
    private val cartListBeanList: MutableList<WishlistResponse.WishListBean>
) : RecyclerView.Adapter<WishListAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtItemName: TextView
        var txtPrice: TextView
        var btnDecrement: TextView
        var thumbnail: ImageView
        var parentLayout: LinearLayout


        init {
            txtItemName = view.findViewById<View>(R.id.item_name) as TextView
            txtPrice = view.findViewById<View>(R.id.item_price) as TextView
            thumbnail = view.findViewById<View>(R.id.thumbnail) as ImageView
            btnDecrement = view.findViewById<View>(R.id.remove_item) as TextView
            parentLayout = view.findViewById<View>(R.id.parentLayout) as LinearLayout

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.wish_card, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val cartListBean = cartListBeanList[position]

        holder.txtItemName.text = capitalize(cartListBean.product_name)
        Log.e("PRO_NAME", "" + cartListBean.product_name!!)

        if (cartListBean.dis_price!!.equals(
                "0",
                ignoreCase = true
            ) || cartListBean.dis_price!!.equals("0.00", ignoreCase = true)
        ) {
            holder.txtPrice.text = " \u20B9" + Utilities.df.format(cartListBean.inclTax!!)
        } else {
            holder.txtPrice.text = " \u20B9" + Utilities.df.format(cartListBean.dis_price!!)
        }

        // loading album cover using Glide library
        Glide.with(mContext).load(cartListBean.image).into(holder.thumbnail)

        holder.btnDecrement.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.alert_dialog)


            // set the custom dialog components - text, image and button
            val te = dialog.findViewById<View>(R.id.txtAlert) as TextView
            te.text = "Are You Sure Want to Delete ?"

            val yes = dialog.findViewById<View>(R.id.btnYes) as TextView
            val no = dialog.findViewById<View>(R.id.btnNo) as TextView

            yes.setOnClickListener { view ->
               // val cartListBean = cartListBeanList[position]

                delete(view, cartListBean.id)
                cartListBeanList.removeAt(position)
                notifyDataSetChanged()
                Log.e("COUNTADDRESS", "" + cartListBeanList.size)

                dialog.dismiss()
            }

            no.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }

        holder.parentLayout.setOnClickListener {
            val intent = Intent(mContext, ProductDetailsActivity::class.java)
            intent.putExtra("productName", cartListBean.product_name)
            intent.putExtra("module", cartListBean.category)
            Log.e("MODULE", "" + cartListBean.category!!)


            if (cartListBean.category!!.equals("mobiles", ignoreCase = true)) {

                intent.putExtra(
                    "productId",
                    cartListBean.product_id + "&itemId=" + cartListBean.item_id
                )
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                mContext.startActivity(intent)

            } else {

                intent.putExtra("productId", cartListBean.product_id)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                mContext.startActivity(intent)
            }
        }
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun getItemCount(): Int {
        return cartListBeanList.size
    }


    private fun delete(view: View, id: String?) {


        val call = RetrofitClient.instance!!.api.deleteWishList(id)

        call.enqueue(object : Callback<WishListDeleteresponse> {
            override fun onResponse(
                call: Call<WishListDeleteresponse>,
                response: retrofit2.Response<WishListDeleteresponse>
            ) {


                val wishListDeleteresponse = response.body()

                if (response.isSuccessful) {

                    if (wishListDeleteresponse != null && wishListDeleteresponse.status!!.equals(
                            "Item Deleted Successfully From WishList",
                            ignoreCase = true
                        )
                    ) {

                        Snackbar.make(view, wishListDeleteresponse.message!!, Snackbar.LENGTH_SHORT)
                            .show()

                        val intent = Intent(mContext, WishListActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        mContext.startActivity(intent)

                    }
                }
            }

            override fun onFailure(call: Call<WishListDeleteresponse>, t: Throwable) {

            }
        })
    }


}