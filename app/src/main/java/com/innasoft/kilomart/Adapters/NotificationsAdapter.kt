package com.innasoft.kilomart.Adapters

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.IMAGE_BASE_URL
import com.innasoft.kilomart.NotificationActivity
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.NotificationReadResponse
import com.innasoft.kilomart.Response.NotificationsResponse
import com.innasoft.kilomart.Storage.PrefManager

import java.util.HashMap

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import com.innasoft.kilomart.Storage.Utilities.capitalize

class NotificationsAdapter(
    notificationActivity: NotificationActivity,
    internal var dataBeans: List<NotificationsResponse.DataBean>
) : RecyclerView.Adapter<NotificationsAdapter.Holder>() {
    internal var context: Context
    internal var read_status: String? = null
    internal var send_id: String? = null
    internal var userId: String? = null
    internal var tokenValue: String? = null
    private var pref: PrefManager? = null
    internal lateinit var alertDialog: AlertDialog

    init {
        this.context = notificationActivity
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NotificationsAdapter.Holder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_notification_list, viewGroup, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: NotificationsAdapter.Holder, i: Int) {

        holder.notify_title_name.text = capitalize(dataBeans[i].title)
        holder.notify_description.text = dataBeans[i].message
        holder.notify_dateandtime.text = dataBeans[i].createdOn

        Glide.with(context).load(IMAGE_BASE_URL + dataBeans[0].image!!)
            .error(R.drawable.default_loading).into(holder.imag_notification)

        read_status = dataBeans[i].isRead
        if (read_status == "0") {

            holder.linear_layout_row.setBackgroundColor(Color.parseColor("#d1f2e2"))
        } else {
            //nothing
        }

        pref = PrefManager(context)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        tokenValue = profile["AccessToken"]



        holder.linear_layout_row.setOnClickListener { v ->
            showCustomDialog(
                dataBeans[i].message,
                v,
                i
            )
        }

    }

    private fun showCustomDialog(message: String?, view: View, i: Int) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = view.findViewById<ViewGroup>(android.R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView =
            LayoutInflater.from(context).inflate(R.layout.my_customdialog, viewGroup, false)


        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(context)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        val txtmessage = dialogView.findViewById<View>(R.id.txtMessage) as TextView
        txtmessage.text = message


        val buttonOk = dialogView.findViewById<View>(R.id.buttonOk) as Button
        buttonOk.setOnClickListener {
            read_status = dataBeans[i].isRead

            if (read_status == "0") {

                send_id = dataBeans[i].id

                sendReadStatus(send_id)

            } else {

                alertDialog.dismiss()
            }
        }

        alertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun sendReadStatus(send_id: String?) {

        val call = RetrofitClient.instance!!.api.NotificationRead(tokenValue, userId, send_id)
        call.enqueue(object : Callback<NotificationReadResponse> {
            override fun onResponse(
                call: Call<NotificationReadResponse>,
                response: Response<NotificationReadResponse>
            ) {
                if (response.isSuccessful);
                val baseResponse = response.body()
                if (baseResponse!!.status == "10100") {

                    val intent = Intent(context, NotificationActivity::class.java)
                    context.startActivity(intent)

                } else if (baseResponse.status == "10200") {
                    Toast.makeText(context, baseResponse.message, Toast.LENGTH_SHORT).show()
                } else if (baseResponse.status == "10300") {
                    Toast.makeText(context, baseResponse.message, Toast.LENGTH_SHORT).show()
                } else if (baseResponse.status == "10400") {
                    Toast.makeText(context, baseResponse.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<NotificationReadResponse>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()

            }
        })
    }

    override fun getItemCount(): Int {
        return dataBeans.size
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imag_notification: ImageView
        var notify_title_name: TextView
        var notify_description: TextView
        var notify_dateandtime: TextView
        var linear_layout_row: LinearLayout

        init {

            notify_dateandtime = itemView.findViewById(R.id.notify_dateandtime)
            notify_description = itemView.findViewById(R.id.notify_description)
            notify_title_name = itemView.findViewById(R.id.notify_title_name)
            imag_notification = itemView.findViewById(R.id.imag_notification)
            linear_layout_row = itemView.findViewById(R.id.linear_layout_row)

        }
    }
}
