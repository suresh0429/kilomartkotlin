package com.innasoft.kilomart.Adapters

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.innasoft.kilomart.Model.SingleItemModel
import com.innasoft.kilomart.ProductDetailsActivity
import com.innasoft.kilomart.R

import java.util.ArrayList

import butterknife.BindView
import butterknife.ButterKnife
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.PRODUCT_IMAGE_BASE_URL2
import com.innasoft.kilomart.Storage.Utilities


import com.innasoft.kilomart.Storage.Utilities.capitalize
import kotlinx.android.synthetic.main.cart_product.view.*
import kotlinx.android.synthetic.main.product_card.view.*

class ProductSectionListDataAdapter(
    private val mContext: Context,
    private val itemsList: ArrayList<SingleItemModel>?
) : RecyclerView.Adapter<ProductSectionListDataAdapter.SingleItemRowHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.product_card, null)
        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        val singleItem = itemsList!![i]

        if (singleItem.availability!!.equals("1", ignoreCase = true)) {
            holder.shadowImageView!!.visibility = View.VISIBLE
        } else {
            holder.shadowImageView!!.visibility = View.GONE

        }


        holder.product_unit!!.text = "" + singleItem.unit_value + singleItem.unit_name
        holder.txtproductPrice!!.text = "\u20B9" + Utilities.df.format(singleItem.mrp_price!!)
        holder.txtproductPrice!!.paintFlags =
            holder.txtproductPrice!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        holder.txtproductPrice!!.setTextColor(Color.RED)

        holder.txtDiscountPrice!!.text = "\u20B9" + Utilities.df.format(singleItem.selling_price!!)
        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2 + singleItem.images!![0])
            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder)
            .into(holder.thumbnail!!)
        holder.txtproductName!!.text = capitalize(singleItem.product_name)

        holder.parentLayout!!.setOnClickListener {
            val intent = Intent(mContext, ProductDetailsActivity::class.java)
            intent.putExtra("productId", singleItem.id)
            intent.putExtra("productName", singleItem.product_name)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)
        }

        /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    override fun getItemCount(): Int {
        return itemsList?.size ?: 0
    }

    inner class SingleItemRowHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val thumbnail = itemView.thumbnail
        val txtproductName = itemView.txtproductName
        val product_unit = itemView.product_unit1
        val txtproductPrice = itemView.txtproductPrice
        val txtDiscountPrice = itemView.txtDiscountPrice
        val txtDiscountTag = itemView.txtDiscountTag
        val shadowImageView = itemView.shadowImageView
        val tagLayout = itemView.tagLayout
        val parentLayout = itemView.parentLayout



    }

}