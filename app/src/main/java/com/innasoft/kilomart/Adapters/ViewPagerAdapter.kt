package com.innasoft.kilomart.Adapters

import android.content.Context
import android.content.Intent

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout

import androidx.viewpager.widget.PagerAdapter

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.innasoft.kilomart.R

import java.util.ArrayList

class ViewPagerAdapter(
    private val context: Context,
    private val images: List<String>,
    private val availability: String
) : PagerAdapter() {
    private val inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val myImageLayout = inflater.inflate(R.layout.slide_card, view, false)


        val myImage = myImageLayout.findViewById<View>(R.id.thumbnail) as ImageView
        val shadowImageView = myImageLayout.findViewById<View>(R.id.shadowImageView) as LinearLayout

        if (availability.equals("1", ignoreCase = true)) {
            shadowImageView.visibility = View.VISIBLE
        } else {
            shadowImageView.visibility = View.GONE

        }

        Log.d(TAG, "instantiateItem: $availability")

        try {
            Glide.with(context).load(images[position]).skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder)
                .into(myImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /* myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductImageActivity.class);
                intent.putStringArrayListExtra("ViewPager", (ArrayList<String>) images);
                intent.putExtra("productImageName","Images");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context. startActivity(intent);
            }
        });*/


        view.addView(myImageLayout, position)
        return myImageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    companion object {
        private val TAG = "ViewPagerAdapter"
    }
}