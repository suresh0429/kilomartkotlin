package com.innasoft.kilomart.Adapters

import android.content.Context
import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.IMAGE_BASE_URL
import com.innasoft.kilomart.GroceryHomeActivity
import com.innasoft.kilomart.ProductListActivity
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.HomeResponse


class HomeAdapter(
    private val mContext: Context,
    private val homeList: List<HomeResponse.DataBean>
) : RecyclerView.Adapter<HomeAdapter.MyViewHolder>() {
    // ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var thumbNail: ImageView
        internal var txtTitle: TextView
        internal var txtPending: TextView? = null
        internal var progressBar: ProgressBar
        //  LinearLayout tagLayout;
        internal var cardView: CardView


        init {

            thumbNail = view.findViewById<View>(R.id.thumbnail) as ImageView
            txtTitle = view.findViewById<View>(R.id.txtTitle) as TextView
            progressBar = view.findViewById<View>(R.id.progressBar) as ProgressBar
            // tagLayout = (LinearLayout) view.findViewById(R.id.tagLayout);
            // txtPending = (TextView) view.findViewById(R.id.txtPending);
            cardView = view.findViewById<View>(R.id.cardView) as CardView

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.home_card, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val home = homeList[position]

        holder.txtTitle.text = home.name
        Glide.with(mContext).load(IMAGE_BASE_URL + home.image!!).skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder)
            .into(holder.thumbNail)

        holder.cardView.setOnClickListener {
            if (home.id!!.equals("5", ignoreCase = true) || home.name!!.equals(
                    "Baby care",
                    ignoreCase = true
                )
            ) {

                val intent1 = Intent(mContext, ProductListActivity::class.java)
                intent1.putExtra("catId", home.id)
                intent1.putExtra("title", home.name)
                intent1.putExtra("subcatId", "")
                intent1.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                mContext.startActivity(intent1)
            } else {
                val intent = Intent(mContext, GroceryHomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("CatId", home.id)
                intent.putExtra("Cat_name", home.name)
                mContext.startActivity(intent)
            }
        }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return homeList.size
    }


    /*
    @Override
    public int getItemViewType(int position) {
        if(position% 2 == 1) {
            return 2;
        }else{
            return 3;
        }
    }
*/
}
