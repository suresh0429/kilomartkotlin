package com.innasoft.kilomart.Adapters

import android.content.Context
import android.content.Intent

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.amulyakhare.textdrawable.util.ColorGenerator
import com.github.thunder413.datetimeutils.DateTimeStyle
import com.github.thunder413.datetimeutils.DateTimeUtils
import com.innasoft.kilomart.OrdersListActivity
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.MyOrdersResponse

import java.util.Date
import java.util.StringTokenizer

class OrderIdAdapter(
    private val mContext: Context,
    private val myOrdersListBeans: List<MyOrdersResponse.DataBean.RecordDataBean>
) : RecyclerView.Adapter<OrderIdAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtOrderId: TextView
        var txtViewItems: TextView? = null
        var txtId: TextView? = null
        var txtInvoice: TextView? = null
        var txtPaymentStatus: TextView
        var txtdd: TextView
        var txtmm: TextView
        var txtyy: TextView
        var txtIdTitle: TextView? = null
        var txtOrderTitle: TextView
        var txtpaymentTitle: TextView
        var cardView: CardView

        init {

            txtOrderId = itemView.findViewById<View>(R.id.txtOrderId) as TextView

            txtPaymentStatus = itemView.findViewById<View>(R.id.txtPaymentStatus) as TextView

            txtdd = itemView.findViewById<View>(R.id.txtdd) as TextView
            txtmm = itemView.findViewById<View>(R.id.txtmm) as TextView
            txtyy = itemView.findViewById<View>(R.id.txtyy) as TextView

            txtOrderTitle = itemView.findViewById<View>(R.id.txtOrderTitle) as TextView
            txtpaymentTitle = itemView.findViewById<View>(R.id.txtpaymentTitle) as TextView
            cardView = itemView.findViewById<View>(R.id.cardView) as CardView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.orderidlayout, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val orderListBean = myOrdersListBeans[position]

        setFadeAnimation(holder.itemView)

        val generator = ColorGenerator.MATERIAL // or use DEFAULT
        // generate random color
        val color1 = generator.randomColor

        holder.cardView.setCardBackgroundColor(color1)

        holder.txtOrderTitle.text = "ORDER ID : "
        holder.txtpaymentTitle.text = "Payment Type : "
        holder.txtOrderId.text = orderListBean.orderId
        holder.txtPaymentStatus.text = orderListBean.paymentStatus


        Log.d(
            "TIMEZONE",
            "String To Date >> " + DateTimeUtils.formatDate(orderListBean.orderDate!!)
        )

        val date = DateTimeUtils.formatDate(orderListBean.orderDate!!)
        val fullDate = DateTimeUtils.formatWithStyle(date, DateTimeStyle.MEDIUM) // June 13, 2017

        val stringTokenizer = StringTokenizer(fullDate)
        val monthDate = stringTokenizer.nextToken(",")
        val year = stringTokenizer.nextToken(",")

        val monthdateToken = StringTokenizer(monthDate)
        val mm = monthdateToken.nextToken(" ")
        val dd = monthdateToken.nextToken(" ")

        Log.e("TOKEN", "" + mm + dd)

        holder.txtdd.text = dd
        holder.txtmm.text = mm
        holder.txtyy.text = year


        holder.itemView.setOnClickListener {
            val order_id = orderListBean.id

            val intent = Intent(mContext, OrdersListActivity::class.java)
            intent.putExtra("Order_ID", order_id)
            intent.putExtra("CartStatus", false)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)
        }

        // get Invoice
        //        holder.txtInvoice.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                String url = "https://pickany24x7.com/Invoices/Invoice_" + orderListBean.getOrderId() + ".pdf";
        //
        //                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        //                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        //                mContext.startActivity(intent);
        //            }
        //        });
        //
        //        // view Items
        //        holder.txtViewItems.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                Intent intent = new Intent(mContext, OrdersListActivity.class);
        //                intent.putExtra("orderId", orderListBean.getOrderId());
        //                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        //                mContext.startActivity(intent);
        //            }
        //        });
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return myOrdersListBeans.size
    }

    fun setFadeAnimation(view: View) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 500
        view.startAnimation(anim)
    }
}
