package com.innasoft.kilomart.Adapters

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.AddressListActivity
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.CheckoutActivity

import com.innasoft.kilomart.HomeActivity
import com.innasoft.kilomart.Model.AddressModelItem
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.UpdateAddressActivity

import retrofit2.Call
import retrofit2.Callback

import com.innasoft.kilomart.Storage.Utilities.capitalize

class AddressAdapter(
    private val mContext: Context,
    private val cartListBeanList: MutableList<AddressModelItem>,
    private val tokenValue: String,
    private val userId: String,
    private val checkoutStatus: Boolean
) : BaseAdapter() {

    private var selectedIndex = -1

    override fun getCount(): Int {
        return cartListBeanList.size
    }

    override fun getItem(i: Int): Any {
        return cartListBeanList[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        val view1 = View.inflate(mContext, R.layout.address_card, null)


        val cartListBean = cartListBeanList[i]

        Log.d("ADDID", "getView: " + cartListBean.id!!)

        val txtName: TextView
        val txtAddress: TextView
        val txtMobile: TextView
        val btnEdit: Button
        val btnDelete: Button
        val radioButton: RadioButton
        val btnDelivery: Button


        txtName = view1.findViewById<View>(R.id.txtName) as TextView
        txtAddress = view1.findViewById<View>(R.id.txtAddress) as TextView
        txtMobile = view1.findViewById<View>(R.id.txtMobile) as TextView
        btnEdit = view1.findViewById<View>(R.id.txtEdit) as Button
        btnDelete = view1.findViewById<View>(R.id.btnDelete) as Button
        radioButton = view1.findViewById<View>(R.id.radioButton) as RadioButton
        btnDelivery = view1.findViewById<View>(R.id.btnDelivery) as Button


        txtName.text = capitalize(cartListBean.name)
        txtAddress.text = capitalize(
            cartListBean.address_line1 + "," + cartListBean.address_line2 + "," + cartListBean.area + "," + cartListBean.city + "," +
                    cartListBean.state + "," + cartListBean.pincode
        )

        if (cartListBean.alternate_contact_no!!.equals("", ignoreCase = true)) {
            txtMobile.text = "Mobile : " + cartListBean.contact_no!!

        } else {
            txtMobile.text =
                "Mobile : " + cartListBean.contact_no + "\n" + "Alternate Contact no : " + cartListBean.alternate_contact_no

        }


        radioButton.visibility = View.GONE


        if (selectedIndex == i) {


            radioButton.isChecked = true
            btnEdit.visibility = View.VISIBLE
            btnDelete.visibility = View.VISIBLE


            if (checkoutStatus) {
                btnDelivery.visibility = View.VISIBLE
            } else {
                btnDelivery.visibility = View.GONE
            }


        } else {
            radioButton.isChecked = false
            btnEdit.visibility = View.GONE
            btnDelete.visibility = View.GONE
            btnDelivery.visibility = View.GONE
        }


        btnDelivery.setOnClickListener {
            val activity = mContext as Activity
            val intent = Intent(mContext, CheckoutActivity::class.java)
            intent.putExtra("addressId", cartListBean.id)
            intent.putExtra("Checkout", checkoutStatus)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        btnEdit.setOnClickListener {
            val activity = mContext as Activity
            val intent = Intent(mContext, UpdateAddressActivity::class.java)
            intent.putExtra("addressid", cartListBean.id)
            intent.putExtra("city", cartListBean.city)
            intent.putExtra("username", cartListBean.name)
            intent.putExtra("state", cartListBean.state)
            intent.putExtra("mobile", cartListBean.contact_no)
            intent.putExtra("addressline1", cartListBean.address_line1)
            intent.putExtra("addressline2", cartListBean.address_line2)
            intent.putExtra("area", cartListBean.area)
            intent.putExtra("pincode", cartListBean.pincode)
            intent.putExtra("altenateno", cartListBean.alternate_contact_no)
            intent.putExtra("setdefault", cartListBean.is_default)
            intent.putExtra("Checkout", checkoutStatus)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

            // Toast.makeText(mContext,""+cartListBean.getAddressId(),Toast.LENGTH_SHORT).show();
        }

        btnDelete.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.alert_dialog)


            // set the custom dialog components - text, image and button
            val te = dialog.findViewById<View>(R.id.txtAlert) as TextView
            te.text = "Are You Sure Want to Delete ?"

            val yes = dialog.findViewById<View>(R.id.btnYes) as TextView
            val no = dialog.findViewById<View>(R.id.btnNo) as TextView

            yes.setOnClickListener { v ->
                val cartListBean1 = cartListBeanList[i]

                delete(v, tokenValue, userId, cartListBean1.id)
                cartListBeanList.removeAt(i)
                notifyDataSetChanged()
                Log.e("COUNTADDRESS", "" + cartListBeanList.size)

                /* if (cartListBeanList.size()==0){
                            Intent intent = new Intent(mContext, AddressListActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("Checkout", checkoutStatus);
                            mContext.startActivity(intent);
                        }*/

                dialog.dismiss()
            }

            no.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }



        return view1

    }

    fun setSelectedIndex(index: Int) {
        selectedIndex = index

    }


    private fun delete(view: View, tokenValue: String, userId: String, id: String?) {

        val call = RetrofitClient.instance!!.api.deleteuserAddress(tokenValue, userId, id)

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(
                call: Call<BaseResponse>,
                response: retrofit2.Response<BaseResponse>
            ) {


                val wishListDeleteresponse = response.body()

                if (response.isSuccessful) {

                    if (wishListDeleteresponse!!.status!!.equals("10100", ignoreCase = true)) {

                        Snackbar.make(view, "Address Deleted", Snackbar.LENGTH_SHORT).show()

                        val intent = Intent(mContext, AddressListActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.putExtra("Checkout", checkoutStatus)
                        mContext.startActivity(intent)


                    }
                } else {
                    Toast.makeText(
                        mContext,
                        wishListDeleteresponse!!.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

            }
        })
    }

}