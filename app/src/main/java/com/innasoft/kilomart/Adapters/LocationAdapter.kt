package com.innasoft.kilomart.Adapters

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import com.innasoft.kilomart.HomeActivity
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.LocationsResponse
import com.innasoft.kilomart.Storage.Utilities

import butterknife.BindView
import butterknife.ButterKnife

import com.innasoft.kilomart.Storage.Utilities.capitalize
import kotlinx.android.synthetic.main.cart_product.view.*
import kotlinx.android.synthetic.main.location_card.view.*

class LocationAdapter(
    private val mContext: Context,
    private val homeList: List<LocationsResponse.DataBean>,
    private val alertDialog: AlertDialog
) : RecyclerView.Adapter<LocationAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtLocation = itemView.txtLocation
        val parentLayout = itemView.parentLayout

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.location_card, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val home = homeList[position]

        holder.txtLocation!!.text = capitalize(home.area)

        holder.parentLayout!!.setOnClickListener {
            val area = home.area
            val intent = Intent(mContext, HomeActivity::class.java)
            //                intent.putExtra("loc_Area",area);
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)

            alertDialog.dismiss()

            // update location preferences
            val preferences = mContext.getSharedPreferences(Utilities.PREFS_LOCATION, 0)
            val editor = preferences.edit()
            editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false)
            editor.putString(Utilities.KEY_AREA, home.area)
            editor.putString(Utilities.KEY_PINCODE, home.pincode)
            editor.putString(Utilities.KEY_SHIPPINGCHARGES, home.shipping_charges?.toString())
            editor.apply()
        }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return homeList.size
    }


}
