package com.innasoft.kilomart.Adapters

import android.content.Context
import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.innasoft.kilomart.Model.HeaderSectionDataModel
import com.innasoft.kilomart.Model.SingleItemModel
import com.innasoft.kilomart.ProductListActivity
import com.innasoft.kilomart.R

import java.util.ArrayList

import com.innasoft.kilomart.Storage.Utilities.capitalize

class SubCatRecyclerViewDataAdapter(
    private val mContext: Context,
    private val dataList: ArrayList<HeaderSectionDataModel>?,
    private val singleItemList: ArrayList<SingleItemModel>
) : RecyclerView.Adapter<SubCatRecyclerViewDataAdapter.ItemRowHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item, null)
        return ItemRowHolder(v)
    }

    override fun onBindViewHolder(itemRowHolder: ItemRowHolder, i: Int) {

        //        final SingleItemModel singleItemModel = singleItemList.get(i);

        val sectionName = dataList!![i].headerTitle

        val singleSectionItems = dataList[i].allItemsInSection

        itemRowHolder.itemTitle.text = capitalize(sectionName)

        val itemListDataAdapter = ProductSectionListDataAdapter(mContext, singleSectionItems)

        itemRowHolder.recycler_view_list.setHasFixedSize(true)
        itemRowHolder.recycler_view_list.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        itemRowHolder.recycler_view_list.adapter = itemListDataAdapter


        itemRowHolder.recycler_view_list.isNestedScrollingEnabled = false

        itemRowHolder.btnMore.setOnClickListener {
            val intent = Intent(mContext, ProductListActivity::class.java)
            intent.putExtra("catId", dataList[i].categoryId)
            intent.putExtra("title", dataList[i].headerTitle)
            intent.putExtra("subcatId", dataList[i].subcategoryId)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)
        }


    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    inner class ItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var itemTitle: TextView

        internal var recycler_view_list: RecyclerView

        internal var btnMore: Button


        init {

            this.itemTitle = view.findViewById<View>(R.id.itemTitle) as TextView
            this.recycler_view_list =
                view.findViewById<View>(R.id.recycler_view_list) as RecyclerView
            this.btnMore = view.findViewById<View>(R.id.btnMore) as Button


        }

    }

}