package com.innasoft.kilomart.Adapters

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.PRODUCT_IMAGE_BASE_URL2
import com.innasoft.kilomart.OrdersListActivity
import com.innasoft.kilomart.Response.OrderListResponse
import com.innasoft.kilomart.R


import com.innasoft.kilomart.R.*
import com.innasoft.kilomart.Storage.Utilities
import com.innasoft.kilomart.Storage.Utilities.capitalize

class OrdersListAdapter(
    ordersListActivity: OrdersListActivity,
    internal var productsBeanList: List<OrderListResponse.DataBean.ProductsBean>
) : RecyclerView.Adapter<OrdersListAdapter.MyViewHolder>() {
    private val mContext: Context

    init {
        this.mContext = ordersListActivity
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var imgOrder: ImageView
        internal var txtOrderName: TextView
        internal var txtOrderID: TextView
        internal var txtOrderPrice: TextView
        internal var txtStatus: TextView? = null
        internal var txtReview: TextView? = null
        internal var txtQty: TextView
        internal var reviewLayout: LinearLayout? = null


        init {

            imgOrder = itemView.findViewById<View>(id.imgOrder) as ImageView
            txtOrderName = itemView.findViewById<View>(id.txtOrderName) as TextView
            txtOrderID = itemView.findViewById<View>(id.txtOrderID) as TextView
            txtOrderPrice = itemView.findViewById<View>(id.txtOrderPrice) as TextView
            txtQty = itemView.findViewById<View>(R.id.txtQty) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.myorderlistmodel1, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2 + productsBeanList[position].images!!)
            .error(R.drawable.default_loading).into(holder.imgOrder)

        holder.txtOrderName.text = capitalize(productsBeanList[position].productName)
        //        holder.txtOrderID.setText(productsBeanList.get(position).getProductId());
        holder.txtQty.text = "Product Quantity : " + productsBeanList[position].purchaseQuantity!!
        holder.txtOrderPrice.text =
            mContext.resources.getString(R.string.Rs) + " " + Utilities.df.format(productsBeanList[position].totalPrice)


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return productsBeanList.size
    }


    //    private void prepareReviewData(final View view, final String module, final String userid, final String productId, final String username, final String email, final String rating, final String message){
    //
    //
    //        Call<ReviewResponse> call = RetrofitClient.getInstance().getApi().reviewOrder( module, userid, productId, username, email, rating, message);
    //
    //        call.enqueue(new Callback<ReviewResponse>() {
    //            @Override
    //            public void onResponse(Call<ReviewResponse> call, retrofit2.Response<ReviewResponse> response) {
    //
    //                ReviewResponse reviewResponse = response.body();
    //
    //                if (response.isSuccessful()) {
    //
    //                    if ((reviewResponse != null ? reviewResponse.getStatus() : 0) ==1){
    //
    //                        Snackbar.make(view,reviewResponse.getMessage(),Snackbar.LENGTH_SHORT).show();
    //
    //                        new Handler().postDelayed(new Runnable() {
    //                            @Override
    //                            public void run() {
    //
    //                                dialog.dismiss();
    //
    //                            }
    //                        }, 2000);
    //
    //                    }
    //
    //
    //
    //
    //                }
    //
    //            }
    //
    //            @Override
    //            public void onFailure(Call<ReviewResponse> call, Throwable t) {
    //
    //
    //            }
    //        });
    //    }


}
