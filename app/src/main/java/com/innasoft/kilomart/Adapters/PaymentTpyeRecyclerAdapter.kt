package com.innasoft.kilomart.Adapters

import android.content.Context

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.IMAGE_BASE_URL
import com.innasoft.kilomart.CheckoutActivity
import com.innasoft.kilomart.Interface.PaymentTypeInterface
import com.innasoft.kilomart.R
import com.innasoft.kilomart.Response.CheckoutResponse


import com.innasoft.kilomart.Storage.Utilities.capitalize

class PaymentTpyeRecyclerAdapter(
    checkoutActivity: CheckoutActivity,
    internal var paymentGatewayBeans: List<CheckoutResponse.DataBean.PaymentGatewayBean>?,
    private val paymentTypeInterface: PaymentTypeInterface
) : RecyclerView.Adapter<PaymentTpyeRecyclerAdapter.Holder>() {
    internal var context: Context
    var lastSelectedPosition = -1

    init {
        this.context = checkoutActivity
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): PaymentTpyeRecyclerAdapter.Holder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_paymentmethods, viewGroup, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: PaymentTpyeRecyclerAdapter.Holder, i: Int) {


        holder.offer_name.text = capitalize(paymentGatewayBeans?.get(i)!!.name)
        holder.offer_select.isChecked = lastSelectedPosition == i

        Glide.with(context).load(IMAGE_BASE_URL + paymentGatewayBeans!![i].logo!!)
            .into(holder.p_image)

        Log.d("IMAGE", "onBindViewHolder: " + IMAGE_BASE_URL + paymentGatewayBeans!![i].logo)
        val clickListener = View.OnClickListener {
            lastSelectedPosition = holder.adapterPosition
            notifyDataSetChanged()

            paymentTypeInterface.onItemClick(paymentGatewayBeans, i)
            /*Log.d("POSITION", "onClick: "+paymentGatewayBeans.);*/


            // cash = paymentGatewayBeans.get(lastSelectedPosition).getId();
        }
        holder.itemView.setOnClickListener(clickListener)
        holder.offer_select.setOnClickListener(clickListener)

    }

    override fun getItemCount(): Int {
        return paymentGatewayBeans!!.size
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var offer_name: TextView
        var offer_select: RadioButton
        var p_image: ImageView

        init {

            offer_select = itemView.findViewById(R.id.offer_select)
            offer_name = itemView.findViewById(R.id.offer_name)
            p_image = itemView.findViewById(R.id.p_image)

            /* View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    paymentTypeInterface.onItemClick(itemView.getId());
                    Log.d("POSITION", "onClick: "+itemView.getId());


                   // cash = paymentGatewayBeans.get(lastSelectedPosition).getId();
                }
            };
            itemView.setOnClickListener(clickListener);
            offer_select.setOnClickListener(clickListener);*/
        }
    }

    companion object {
        var cash: String? = null
    }
}
