package com.innasoft.kilomart

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Singleton.AppController


import java.util.regex.Matcher
import java.util.regex.Pattern

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : AppCompatActivity(), View.OnClickListener,
    ConnectivityReceiver.ConnectivityReceiverListener {
    internal var OTP: String? = null
    internal var isConnected: Boolean = false



    internal lateinit var deviceID: String
    internal lateinit var app: AppController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        ButterKnife.bind(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Forgot Password")
        app = application as AppController

        deviceID = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )


        etPhone!!.addTextChangedListener(MyTextWatcher(etPhone))
        etOtp!!.addTextChangedListener(MyTextWatcher(etOtp))
        etsetPassword!!.addTextChangedListener(MyTextWatcher(etsetPassword))

        txtResendOtp.setOnClickListener(this)
        btnContinue.setOnClickListener(this)
    }



    override fun onClick(view: View) {
        when (view.id) {
            R.id.txtResendOtp -> {
                // Method to manually check connection status
                isConnected = app.isConnection

                if (isConnected) {
                    sendOtp()
                } else {

                    val message = "Sorry! Not connected to internet"
                    val color = Color.RED

                    snackBar(message, color)
                    //showSnack(isConnected);
                }
            }
            R.id.btnContinue -> {


                // Method to manually check connection status
                isConnected = app.isConnection

                if (isConnected) {

                    if (btnContinue!!.text.toString().trim { it <= ' ' }.equals(
                            "Submit",
                            ignoreCase = true
                        )
                    ) {
                        passwordChange()
                    } else {
                        sendOtp()
                    }


                } else {

                    val message = "Sorry! Not connected to internet"
                    val color = Color.RED
                    snackBar(message, color)

                }

                try {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                } catch (e: Exception) {

                }

            }
        }
    }


    // send OTP
    private fun sendOtp() {

        val mobile = etPhone!!.text!!.toString().trim { it <= ' ' }
        if (!isValidPhoneNumber(mobile)) {
            return
        }


        progressBar!!.visibility = View.VISIBLE
        btnContinue!!.isEnabled = false

        val call = RetrofitClient.instance!!.api.userForgotPassword(mobile)
        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressBar!!.visibility = View.GONE
                btnContinue!!.isEnabled = true

                val baseResponse = response.body()


                if (baseResponse!!.status.equals("10100", ignoreCase = true)) {

                    secondLayout!!.visibility = View.VISIBLE
                    // etName.setText(baseResponse.getUsername());
                    btnContinue!!.text = "Submit"

                    // OTP = baseResponse.get();

                    val color = Color.GREEN
                    snackBar(baseResponse.message!!, color)

                } else if (baseResponse.status == "10200") {
                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (baseResponse.status == "10300") {
                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (baseResponse.status == "10400") {
                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val color = Color.RED
                    snackBar(baseResponse.message!!, color)

                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()

                progressBar!!.visibility = View.GONE
                btnContinue!!.isEnabled = true
                btnContinue!!.text = "Continue"
            }
        })
    }

    // password change
    private fun passwordChange() {

        val mobile = etPhone!!.text!!.toString().trim { it <= ' ' }
        val otp = etOtp!!.text!!.toString().trim { it <= ' ' }
        val password = etsetPassword!!.text!!.toString().trim { it <= ' ' }

        if (!isValidPhoneNumber(mobile)) {
            return
        }

        /* if ((!isValidOtp(otp))) {
            return;
        }*/

        if (!isValidatePassword(password)) {
            return
        }


        progressBar!!.visibility = View.VISIBLE
        btnContinue!!.isEnabled = false

        val call = RetrofitClient.instance!!.api.userVerifyForgotPassword(
            mobile,
            otp,
            deviceID,
            password
        )

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressBar!!.visibility = View.GONE
                btnContinue!!.isEnabled = true

                val baseResponse = response.body()


                if (baseResponse!!.status.equals("10100", ignoreCase = true)) {

                    val color = Color.GREEN
                    snackBar(baseResponse.message!!, color)

                    Handler().postDelayed({
                        val intent = Intent(applicationContext, LoginActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }, 2000)


                } else if (baseResponse.status == "10200") {
                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (baseResponse.status == "10300") {
                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else if (baseResponse.status == "10400") {
                    Toast.makeText(applicationContext, baseResponse.message, Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val color = Color.RED
                    snackBar(baseResponse.message!!, color)

                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                btnContinue!!.isEnabled = true

            }
        })
    }


    override fun onResume() {
        super.onResume()

        // register connection status listener

        AppController.instance!!.setConnectivityListener(this)
    }


    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String
        val color: Int
        if (isConnected) {
            message = "Good! Connected to Internet"
            color = Color.WHITE

        } else {
            message = "Sorry! Not connected to internet"
            color = Color.RED
        }

        snackBar(message, color)


    }


    // snackBar
    private fun snackBar(message: String, color: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    // validate phone
    private fun isValidPhoneNumber(mobile: String): Boolean {
        val pattern = Pattern.compile("^[9876]\\d{9}$")
        val matcher = pattern.matcher(mobile)

        if (mobile.isEmpty()) {
            mobile_til!!.error = "Phone no is required"
            requestFocus(etPhone!!)
            return false
        } else if (!matcher.matches()) {
            mobile_til!!.error = "Enter a valid mobile"
            requestFocus(etPhone!!)
            return false
        } else {
            mobile_til!!.isErrorEnabled = false
        }

        return matcher.matches()
    }

    // valid OTP
    private fun isValidOtp(otp: String): Boolean {

        if (otp.isEmpty()) {
            txtIpOtpLayout!!.error = "OTP is required"
            requestFocus(etOtp!!)
            return false
        } else if (!otp.equals(OTP!!, ignoreCase = true) || OTP!!.length < 4) {
            txtIpOtpLayout!!.error = "Enter a valid OTP"
            requestFocus(etOtp!!)
            return false
        } else {
            txtIpOtpLayout!!.isErrorEnabled = false
        }

        return true
    }


    // validate password
    private fun isValidatePassword(password: String): Boolean {
        if (password.isEmpty()) {
            txtIpPwdLayout!!.error = "Password required"
            requestFocus(etsetPassword!!)
            return false
        } else if (password.length < 6 || password.length > 20) {
            txtIpPwdLayout!!.error = "Password Should be 6 to 20 characters"
            requestFocus(etsetPassword!!)
            return false
        } else {
            txtIpPwdLayout!!.isErrorEnabled = false
        }

        return true
    }


    // request focus
    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    // text input layout class
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {

                R.id.etPhone -> isValidPhoneNumber(etPhone!!.text!!.toString().trim { it <= ' ' })
                /* case R.id.etOtp:
                    isValidOtp(etOtp.getText().toString().trim());
                    break;*/
                R.id.etsetPassword -> isValidatePassword(etsetPassword!!.text!!.toString().trim { it <= ' ' })
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
