package com.innasoft.kilomart.Singleton

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast


import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.CartCountResponse
import com.innasoft.kilomart.Storage.PrefManager


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AppController : Application() {

    private val pref: PrefManager? = null

    // check internet connection
    val isConnection: Boolean
        get() {

            val connectivityManager =
                getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo

            return networkInfo != null && networkInfo.isConnected
        }


    override fun onCreate() {
        super.onCreate()
        instance = this
        /* HashMap<String, String> profile = pref.getUserDetails();
        String userId=profile.get("id");*/

        // appEnvironment = AppEnvironment.SANDBOX;


    }


    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }


    fun cartCount(userId: String?, deviceId: String?) {

        val call = RetrofitClient.instance!!.api.getCartCount(userId, deviceId)

        call.enqueue(object : Callback<CartCountResponse> {
            override fun onResponse(
                call: Call<CartCountResponse>,
                response: Response<CartCountResponse>
            ) {

                if (response.isSuccessful) {

                    val cartCountResponse = response.body()

                    if (cartCountResponse!!.status!!.equals("10100", ignoreCase = true)) {

                        val cartindex = cartCountResponse.data
                        Log.e("CART_INDEX", "" + cartindex)


                        val preferences = getSharedPreferences(PREFS_NAME, 0)
                        val editor = preferences.edit()
                        editor.putInt("itemCount", cartindex)
                        editor.apply()


                    } else if (cartCountResponse.status!!.equals("10200", ignoreCase = true)) {
                        Toast.makeText(
                            applicationContext,
                            cartCountResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {

                        val preferences = getSharedPreferences(PREFS_NAME, 0)
                        val editor = preferences.edit()
                        editor.putInt("itemCount", 0)
                        editor.apply()
                    }

                }


            }

            override fun onFailure(call: Call<CartCountResponse>, t: Throwable) {
                val preferences = getSharedPreferences(PREFS_NAME, 0)
                val editor = preferences.edit()
                editor.putInt("itemCount", 0)
                editor.apply()
            }
        })

    }

    companion object {

        val TAG = AppController::class.java
            .simpleName


        @get:Synchronized
        var instance: AppController? = null
            private set
        private val PREFS_NAME = "CARTCOUNT"
    }


}
