package com.innasoft.kilomart

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler

import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.widget.NestedScrollView

import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Singleton.AppController

import java.util.regex.Matcher
import java.util.regex.Pattern

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity(), View.OnClickListener,
    ConnectivityReceiver.ConnectivityReceiverListener {
    internal var isConnected: Boolean = false
    internal lateinit var agree: String
    internal var OTP: String? = null



    internal lateinit var app: AppController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        ButterKnife.bind(this)

        app = application as AppController

        val next = "Already Registered ? <font color='#f89113'>Login</font> me"
        txtSignin!!.text = Html.fromHtml(next)

        val checktext =
            "I agree to the Kilomart ? <font color='#f89113'>Terms & conditions</font> and <font color='#f89113'>Privacy Policy</font>"
        checkbox!!.text = Html.fromHtml(checktext)

        agree = "0"
        checkbox!!.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                agree = "1"
            } else {
                agree = "0"
            }
        }


        etUsername!!.addTextChangedListener(MyTextWatcher(etUsername!!))
        etPhone!!.addTextChangedListener(MyTextWatcher(etPhone!!))
        etEmail!!.addTextChangedListener(MyTextWatcher(etEmail!!))
        etPassword!!.addTextChangedListener(MyTextWatcher(etPassword!!))

        btnRegister.setOnClickListener(this)
        txtSignin.setOnClickListener(this)
    }


    // user Registration
    private fun userRegistartion() {

        val name = etUsername!!.text!!.toString().trim { it <= ' ' }
        val mobile = etPhone!!.text!!.toString().trim { it <= ' ' }
        val email = etEmail!!.text!!.toString().trim { it <= ' ' }
        val password = etPassword!!.text!!.toString().trim { it <= ' ' }

        if (!isValidName(name)) {
            return
        }
        if (!isValidPhoneNumber(mobile)) {
            return
        }
        if (!isValidEmail(email)) {
            return
        }
        if (!isValidatePassword(password)) {
            return
        }
        if (agree.equals("0", ignoreCase = true)) {
            Toast.makeText(
                applicationContext,
                "Please Accept Terms & conditions",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        progressBar!!.visibility = View.VISIBLE
        btnRegister!!.isEnabled = false

        val call =
            RetrofitClient.instance!!.api.userRegistrationRequest(name, email, mobile, password)

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressBar!!.visibility = View.GONE
                btnRegister!!.isEnabled = true

                val registerResponse = response.body()


                if (registerResponse!!.status.equals("10100", ignoreCase = true)) {

                    /* int color = Color.GREEN;
                    snackBar(registerResponse.getMessage(), color);

*/
                    Handler().postDelayed({
                        val intent = Intent(applicationContext, VerifyOtpActivity::class.java)
                        intent.putExtra("MobileNumber", mobile)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }, 2000)


                } else if (registerResponse.status == "10200") {
                    Toast.makeText(
                        this@RegisterActivity,
                        registerResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (registerResponse.status == "10300") {
                    Toast.makeText(
                        this@RegisterActivity,
                        registerResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (registerResponse.status == "10400") {
                    Toast.makeText(
                        this@RegisterActivity,
                        registerResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
                /* else{
                    int color = Color.RED;
                    snackBar(registerResponse.getMessage(), color);

                }*/


            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                btnRegister!!.isEnabled = true
            }
        })
    }



    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnRegister -> {

                // Method to manually check connection status
                val isConnected = app.isConnection
                if (isConnected) {
                    userRegistartion()
                } else {

                    val message = "Sorry! Not connected to internet"
                    val color = Color.RED
                    snackBar(message, color)
                }
            }

            R.id.txtSignin -> {
                val intent1 = Intent(this@RegisterActivity, LoginActivity::class.java)
                intent1.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent1)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }/*case R.id.txtVerify:
                // Method to manually check connection status
                isConnected = ConnectivityReceiver.isConnected();
                if (isConnected) {
                    userOtpRequest();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }

                break;*/
    }


    override fun onResume() {
        super.onResume()

        // register connection status listener

        AppController.instance!!.setConnectivityListener(this)
    }


    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String
        val color: Int
        if (isConnected) {
            message = "Good! Connected to Internet"
            color = Color.WHITE

        } else {
            message = "Sorry! Not connected to internet"
            color = Color.RED
        }

        snackBar(message, color)


    }


    // snackBar
    private fun snackBar(message: String, color: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showSnack(isConnected)
    }


    // validate name
    private fun isValidName(name: String): Boolean {
        val pattern = Pattern.compile("[a-zA-Z ]+")
        val matcher = pattern.matcher(name)

        if (name.isEmpty()) {
            user_til!!.error = "name is required"
            requestFocus(etUsername!!)
            return false
        } else if (!matcher.matches()) {
            user_til!!.error = "Enter Alphabets Only"
            requestFocus(etUsername!!)
            return false
        } else if (name.length < 5 || name.length > 20) {
            user_til!!.error = "Name Should be 5 to 20 characters"
            requestFocus(etUsername!!)
            return false
        } else {
            user_til!!.isErrorEnabled = false
        }
        return matcher.matches()
    }


    // validate phone
    private fun isValidPhoneNumber(mobile: String): Boolean {
        val pattern = Pattern.compile("^[9876]\\d{9}$")
        val matcher = pattern.matcher(mobile)

        if (mobile.isEmpty()) {
            mobile_til!!.error = "Phone no is required"
            requestFocus(etPhone!!)

            return false
        } else if (!matcher.matches()) {
            mobile_til!!.error = "Enter a valid mobile"
            requestFocus(etPhone!!)
            return false
        } else {
            mobile_til!!.isErrorEnabled = false
        }

        return matcher.matches()
    }


    // validate your email address
    private fun isValidEmail(email: String): Boolean {
        val EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email)

        if (email.isEmpty()) {
            email_til!!.error = "Email is required"
            requestFocus(etEmail!!)
            return false
        } else if (!matcher.matches()) {
            email_til!!.error = "Enter a valid email"
            requestFocus(etEmail!!)
            return false
        } else {
            email_til!!.isErrorEnabled = false
        }


        return matcher.matches()
    }


    // validate password
    private fun isValidatePassword(password: String): Boolean {
        if (password.isEmpty()) {
            pwd_til!!.error = "Password required"
            requestFocus(etPassword!!)
            return false
        } else if (password.length < 6 || password.length > 20) {
            pwd_til!!.error = "Password Should be 6 to 20 characters"
            requestFocus(etPassword!!)
            return false
        } else {
            pwd_til!!.isErrorEnabled = false
        }

        return true
    }


    // request focus
    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    // text input layout class
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.etUsername -> isValidName(etUsername!!.text!!.toString().trim { it <= ' ' })
                R.id.etPhone -> isValidPhoneNumber(etPhone!!.text!!.toString().trim { it <= ' ' })
                /* case R.id.etOtp:
                    isValidOtp(etOtp.getText().toString().trim());
                    break;*/
                R.id.etEmail -> isValidEmail(etEmail!!.text!!.toString().trim { it <= ' ' })
                R.id.etPassword -> isValidatePassword(etPassword!!.text!!.toString().trim { it <= ' ' })
            }
        }
    }
}
