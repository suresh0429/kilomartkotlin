package com.innasoft.kilomart

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Process
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import com.goodiebag.pinview.Pinview
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Response.VerifyOtpResponse
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Storage.Utilities

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_verify_otp.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyOtpActivity : Activity(), View.OnClickListener {

    internal var mno: String? = null
    internal lateinit var otp: String
    internal lateinit var deviceID: String
    internal lateinit var session: PrefManager

    internal lateinit var fcmToken: String
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_otp)
        ButterKnife.bind(this)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)


        session = PrefManager(this@VerifyOtpActivity)

        deviceID = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )

        mno = intent.getStringExtra("MobileNumber")

        // fcm Token
        FirebaseInstanceId.getInstance()
            .instanceId.addOnSuccessListener(this@VerifyOtpActivity) { instanceIdResult ->
            fcmToken = instanceIdResult.token
            Log.e("TOKEN", "" + fcmToken)
        }

        otp_txt.setOnClickListener(this)
        pinview.setOnClickListener(this)
        verify_btn.setOnClickListener(this)
        txtresend.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.verify_btn -> {

                otp = pinview!!.value.toString().trim { it <= ' ' }
                progressBar!!.visibility = View.VISIBLE
                val call = RetrofitClient.instance!!.api.userOtpRequest(mno, otp, deviceID)
                call.enqueue(object : Callback<VerifyOtpResponse> {
                    override fun onResponse(
                        call: Call<VerifyOtpResponse>,
                        response: Response<VerifyOtpResponse>
                    ) {

                        if (response.isSuccessful);
                        val verifyOtpResponse = response.body()

                        if (verifyOtpResponse!!.status == "10100") {

                            progressBar!!.visibility = View.GONE

                            session.createLogin(
                                verifyOtpResponse.data!!.user_id,
                                verifyOtpResponse.data!!.user_name,
                                verifyOtpResponse.data!!.email,
                                verifyOtpResponse.data!!.mobile,
                                null, deviceID,
                                verifyOtpResponse.data!!.jwt, verifyOtpResponse.data!!.gender
                            )

                            val intent = Intent(this@VerifyOtpActivity, HomeActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)


                            // location preferences
                            setDefaults()

                            updateFcmToken(
                                verifyOtpResponse.data!!.jwt!!,
                                verifyOtpResponse.data!!.user_id!!
                            )

                            Toast.makeText(
                                applicationContext,
                                "You have been logged in successfully.",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (verifyOtpResponse.status == "10200") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                verifyOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (verifyOtpResponse.status == "10300") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                verifyOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (verifyOtpResponse.status == "10400") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                verifyOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onFailure(call: Call<VerifyOtpResponse>, t: Throwable) {
                        progressBar!!.visibility = View.GONE
                        Toast.makeText(this@VerifyOtpActivity, t.message, Toast.LENGTH_SHORT).show()

                    }
                })
            }
            R.id.txtresend -> {
                progressBar!!.visibility = View.VISIBLE
                val call1 = RetrofitClient.instance!!.api.ResendOtpRequest(mno)
                call1.enqueue(object : Callback<BaseResponse> {
                    override fun onResponse(
                        call: Call<BaseResponse>,
                        response: Response<BaseResponse>
                    ) {
                        if (response.isSuccessful);
                        val resendOtpResponse = response.body()

                        if (resendOtpResponse!!.status == "10100") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                resendOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (resendOtpResponse.status == "10200") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                resendOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (resendOtpResponse.status == "10300") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                resendOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else if (resendOtpResponse.status == "10400") {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(
                                this@VerifyOtpActivity,
                                resendOtpResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        progressBar!!.visibility = View.GONE
                        Toast.makeText(this@VerifyOtpActivity, t.message, Toast.LENGTH_SHORT).show()


                    }
                })
            }
        }    }





    fun setDefaults() {
        val preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0)
        val editor = preferences.edit()
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true)
        editor.putString(Utilities.KEY_AREA, "")
        editor.putString(Utilities.KEY_PINCODE, "")
        editor.putString(Utilities.KEY_SHIPPINGCHARGES, "")
        editor.apply()
    }

    // update Fcm Token
    private fun updateFcmToken(jwt: String, user_id: String) {

        val call = RetrofitClient.instance!!.api.updateFcmTocken(jwt, user_id, fcmToken)
        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {


                progressBar!!.visibility = View.GONE


                val loginResponse = response.body()


                if (loginResponse!!.status == "10100") {

                    // Toast.makeText(VerifyOtpActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: " + loginResponse.message)

                } else if (loginResponse.status == "10200") {
                    Log.d(TAG, "onResponse: " + loginResponse.message)
                } else if (loginResponse.status == "10300") {
                    Log.d(TAG, "onResponse: " + loginResponse.message)
                } else if (loginResponse.status == "10400") {
                    Log.d(TAG, "onResponse: " + loginResponse.message)
                }


            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
            }
        })
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            moveTaskToBack(true)
            Process.killProcess(Process.myPid())
            System.exit(1)
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)

    }

    companion object {
        private val PREFS_LOCATION = "LOCATION_PREF"
        var TAG = "VerifyOtpActivity"
    }
}
