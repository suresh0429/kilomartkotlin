package com.innasoft.kilomart.Response

class LoginResponse {


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njk1NjEwODIsImp0aSI6InRSK1E5eGRtVGljMzd2Rlp1XC9KcmhUT0VWTUs4b2JPRTNsZW52ZlRyb0FJPSIsImlzcyI6Imh0dHA6XC9cL3d3dy5raWxvbWFydC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU2OTU2MTA4MywiZXhwIjoxNjAxMDk3MDgzLCJkYXRhIjp7InVzZXJfaWQiOiI1NSJ9fQ.mVpPpObzS-QZsaBZ4YXd7R4Dgsd6WHFMCffGxvxA90LhmytVAjLOmg3-xwemdH4q9b9tkytztLsNqrDYuQlW3g","user_id":"55","user_name":"Bhavani","email":"bhavani@innasoft.in","mobile":"8074294327","gender":"","mobile_verify_status":"0"}
     */

    var status: String? = null
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njk1NjEwODIsImp0aSI6InRSK1E5eGRtVGljMzd2Rlp1XC9KcmhUT0VWTUs4b2JPRTNsZW52ZlRyb0FJPSIsImlzcyI6Imh0dHA6XC9cL3d3dy5raWxvbWFydC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU2OTU2MTA4MywiZXhwIjoxNjAxMDk3MDgzLCJkYXRhIjp7InVzZXJfaWQiOiI1NSJ9fQ.mVpPpObzS-QZsaBZ4YXd7R4Dgsd6WHFMCffGxvxA90LhmytVAjLOmg3-xwemdH4q9b9tkytztLsNqrDYuQlW3g
         * user_id : 55
         * user_name : Bhavani
         * email : bhavani@innasoft.in
         * mobile : 8074294327
         * gender :
         * mobile_verify_status : 0
         */

        var jwt: String? = null
        var user_id: String? = null
        var user_name: String? = null
        var email: String? = null
        var mobile: String? = null
        var gender: String? = null
        var mobile_verify_status: String? = null
    }
}
