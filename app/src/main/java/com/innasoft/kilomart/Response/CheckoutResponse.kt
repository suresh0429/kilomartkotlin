package com.innasoft.kilomart.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckoutResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : {"mobile_verify_status":"1","mobile":"8074294327","address":[{"id":"64","name":"Bhavani Rao","address_line1":"one","address_line2":"","area":"Chandanagar","city":"Hyderabad","state":"Telangana","pincode":"500050","country":"India","contact_no":"8074294327","alternate_contact_no":"","latitude":"0","longitude":"0","is_default":"Yes","delivery_status":true,"delivery_charges":50}],"products":[{"cart_id":"425","product_id":"193","c1_url":"staples","c2_url":"oils-and-ghee","c3_url":null,"product_name":"Fortune Kachi Ghani Mustard Oil, 500 ml Bottle","selling_price":"70.00","mrp_price":"74.00","purchase_quantity":"1","unit_id":"9","unit_name":"ml","unit_value":"500","brand_id":"1","brand_name":"KILOMART","images":"d7613-18.png","p_url":"staples/oils-and-ghee/193","net_amount":70,"gross_amount":74},{"cart_id":"424","product_id":"315","c1_url":"personalneeds","c2_url":"soaps-and-body-wash","c3_url":null,"product_name":"Santoor Sandal & Turmeric Soap","selling_price":"27.50","mrp_price":"28.00","purchase_quantity":"1","unit_id":"3","unit_name":"Piece","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"671c8-3.png","p_url":"personalneeds/soaps-and-body-wash/315","net_amount":27.5,"gross_amount":28},{"cart_id":"423","product_id":"9","c1_url":"staples","c2_url":"rice-and-riceproducts","c3_url":null,"product_name":"KILOMART idly-rice","selling_price":"91.00","mrp_price":"92.00","purchase_quantity":"9","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"43bbb-1.png","p_url":"staples/rice-and-riceproducts/9","net_amount":819,"gross_amount":828}],"final_gross_amount":"930.00","final_net_amount":"916.50","cart_discount":"0.00","coupon_discount":"0.00","voucher_discount":"0.00","final_discount":"13.50","payable_amount":"916.50","payment_gateway":[{"id":"1","name":"Pay on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]}
     */

    var status: String? = ""
    var message: String? = ""
    var data: DataBean? = DataBean()

    class DataBean {
        /**
         * mobile_verify_status : 1
         * mobile : 8074294327
         * address : [{"id":"64","name":"Bhavani Rao","address_line1":"one","address_line2":"","area":"Chandanagar","city":"Hyderabad","state":"Telangana","pincode":"500050","country":"India","contact_no":"8074294327","alternate_contact_no":"","latitude":"0","longitude":"0","is_default":"Yes","delivery_status":true,"delivery_charges":50}]
         * products : [{"cart_id":"425","product_id":"193","c1_url":"staples","c2_url":"oils-and-ghee","c3_url":null,"product_name":"Fortune Kachi Ghani Mustard Oil, 500 ml Bottle","selling_price":"70.00","mrp_price":"74.00","purchase_quantity":"1","unit_id":"9","unit_name":"ml","unit_value":"500","brand_id":"1","brand_name":"KILOMART","images":"d7613-18.png","p_url":"staples/oils-and-ghee/193","net_amount":70,"gross_amount":74},{"cart_id":"424","product_id":"315","c1_url":"personalneeds","c2_url":"soaps-and-body-wash","c3_url":null,"product_name":"Santoor Sandal & Turmeric Soap","selling_price":"27.50","mrp_price":"28.00","purchase_quantity":"1","unit_id":"3","unit_name":"Piece","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"671c8-3.png","p_url":"personalneeds/soaps-and-body-wash/315","net_amount":27.5,"gross_amount":28},{"cart_id":"423","product_id":"9","c1_url":"staples","c2_url":"rice-and-riceproducts","c3_url":null,"product_name":"KILOMART idly-rice","selling_price":"91.00","mrp_price":"92.00","purchase_quantity":"9","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"43bbb-1.png","p_url":"staples/rice-and-riceproducts/9","net_amount":819,"gross_amount":828}]
         * final_gross_amount : 930.00
         * final_net_amount : 916.50
         * cart_discount : 0.00
         * coupon_discount : 0.00
         * voucher_discount : 0.00
         * final_discount : 13.50
         * payable_amount : 916.50
         * payment_gateway : [{"id":"1","name":"Pay on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]
         */

        var mobile_verify_status: String? = ""
        var mobile: String? = ""


        var final_gross_amount: String? = ""

        var final_net_amount: String = ""
        var cart_discount: String = ""
        var coupon_discount: String = ""
        var voucher_discount: String = ""
        var final_discount: String = ""
        var payable_amount: String = ""
        var address: List<AddressBean>? = null
        var products: List<ProductsBean>? = null
        var payment_gateway: List<PaymentGatewayBean>? = null

        class AddressBean {
            /**
             * id : 64
             * name : Bhavani Rao
             * address_line1 : one
             * address_line2 :
             * area : Chandanagar
             * city : Hyderabad
             * state : Telangana
             * pincode : 500050
             * country : India
             * contact_no : 8074294327
             * alternate_contact_no :
             * latitude : 0
             * longitude : 0
             * is_default : Yes
             * delivery_status : true
             * delivery_charges : 50
             */

            var id: String? = ""
            var name: String? = ""
            var address_line1: String? = ""
            var address_line2: String? = ""
            var area: String? = ""
            var city: String? = ""
            var state: String? = ""
            var pincode: String? = ""
            var country: String? = ""
            var contact_no: String? = ""
            var alternate_contact_no: String? = ""
            var latitude: String? = ""
            var longitude: String? = ""
            var is_default: String? = ""
            var isDelivery_status: Boolean = false
            var delivery_charges: Int = 0
        }

        class ProductsBean {
            /**
             * cart_id : 425
             * product_id : 193
             * c1_url : staples
             * c2_url : oils-and-ghee
             * c3_url : null
             * product_name : Fortune Kachi Ghani Mustard Oil, 500 ml Bottle
             * selling_price : 70.00
             * mrp_price : 74.00
             * purchase_quantity : 1
             * unit_id : 9
             * unit_name : ml
             * unit_value : 500
             * brand_id : 1
             * brand_name : KILOMART
             * images : d7613-18.png
             * p_url : staples/oils-and-ghee/193
             * net_amount : 70
             * gross_amount : 74
             */

            var cart_id: String? = ""
            var product_id: String? = ""
            var c1_url: String? = ""
            var c2_url: String? = ""
            var c3_url: Any? = ""
            var product_name: String? = ""
            var selling_price: String? = ""
            var mrp_price: String? = ""
            var purchase_quantity: String? = ""
            var unit_id: String? = ""
            var unit_name: String? = ""
            var unit_value: String? = ""
            var brand_id: String? = ""
            var brand_name: String? = ""
            var images: String? = ""
            var p_url: String? = ""
            var net_amount: String? = ""
            var gross_amount: String? = ""
        }

        class PaymentGatewayBean {
            /**
             * id : 1
             * name : Pay on Delivery
             * logo : images/gateway/Cash-on-Delivery.jpg
             */

            var id: String? = ""
            var name: String? = ""
            var logo: String? = ""
        }
    }
}
