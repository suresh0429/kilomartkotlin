package com.innasoft.kilomart.Response

class DocsResponse {


    /**
     * status : 1
     * docs : [{"id":"1","category_name":"rentals","file_path":"https://pickany24x7.com/image/terms_and_onditions/961509251_TERMS+AND+CONDITIONS+OF+CAR+RENTALS.pdf","file_type":"terms_and_onditions","status":"1"},{"id":"2","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/271257675_Final+Return+Policy-converted.pdf","file_type":"return_policies","status":"1"},{"id":"3","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/414115476_Privacy-%26-Policy-converted.pdf","file_type":"privacy_policies","status":"1"},{"id":"4","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/398144600_Terms+%26+Conditions-converted.pdf","file_type":"terms_and_onditions","status":"1"},{"id":"5","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/693942752_FAQS.pdf","file_type":"faqs","status":"1"},{"id":"6","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/502322564_ABOUTUS.pdf","file_type":"aboutus","status":"1"}]
     * message : Get Documents
     */

    var status: Int = 0
    var message: String? = null
    var docs: List<DocsBean>? = null

    class DocsBean {
        /**
         * id : 1
         * category_name : rentals
         * file_path : https://pickany24x7.com/image/terms_and_onditions/961509251_TERMS+AND+CONDITIONS+OF+CAR+RENTALS.pdf
         * file_type : terms_and_onditions
         * status : 1
         */

        var id: String? = null
        var category_name: String? = null
        var file_path: String? = null
        var file_type: String? = null
        var status: String? = null
    }
}
