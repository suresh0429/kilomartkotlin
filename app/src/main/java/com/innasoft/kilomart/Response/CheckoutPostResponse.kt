package com.innasoft.kilomart.Response

class CheckoutPostResponse {


    /**
     * status : 10100
     * message : Thank you! Your order has been placed successfully. Reference ID: KM2019091601002185.
     * order_id : 3
     */

    var status: String? = null
    var message: String? = null
    var order_id: Int = 0
}
