package com.innasoft.kilomart.Response

class PayumoneyResponse {


    /**
     * status : 0
     * message : payment status for :331893
     * result : {"postBackParamId":207443,"mihpayid":"162580","paymentId":331893,"mode":"CC","status":"success","unmappedstatus":"captured","key":"d7XqbGKV","txnid":"1538647035037","amount":"17000.00","additionalCharges":"","addedon":"2018-10-04 15:30:52","createdOn":1538647258000,"productinfo":"product_info","firstname":"suresh","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"ksuresh.unique@gmail.com","phone":"8985018103","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"a6a0ed67f4d80b6098181a236a16ffd9fbf01e14ea5ad697420be255ba83c302dd761e8eedb86538f29fc5ce5216638b1223f942430768bb9e91b2423b1be5fe","field1":"862951","field2":"696416","field3":"202804","field4":"MC","field5":"101118670374","field6":"00","field7":"0","field8":"3DS","field9":" Verification of Secure Hash Failed: E700 -- Approved -- Transaction Successful -- Unable to be determined--E000","bank_ref_num":"862951","bankcode":"MAST","error":"E000","error_Message":"No Error","cardToken":"","offer_key":"","offer_type":"","offer_availed":"","pg_ref_no":"","offer_failure_reason":"","name_on_card":"payu","cardnum":"512345XXXXXX2346","cardhash":"This field is no longer supported in postback params.","card_type":"","card_merchant_param":null,"version":"","postUrl":"https://www.payumoney.com/mobileapp/payumoney/success.php","calledStatus":false,"additional_param":"","amount_split":"{\"PAYU\":\"17000.0\"}","discount":"0.00","net_amount_debit":"17000","fetchAPI":null,"paisa_mecode":"","meCode":"{\"vpc_Merchant\":\"TESTIBIBOWEB\"}","payuMoneyId":"331893","encryptedPaymentId":null,"id":null,"surl":null,"furl":null,"baseUrl":null,"retryCount":0,"merchantid":null,"payment_source":null,"pg_TYPE":"AXISPG"}
     * errorCode : null
     * responseCode : null
     */

    var status: Int = 0
    var message: String? = null
    var result: ResultBean? = null
    var errorCode: Any? = null
    var responseCode: Any? = null

    class ResultBean {
        /**
         * postBackParamId : 207443
         * mihpayid : 162580
         * paymentId : 331893
         * mode : CC
         * status : success
         * unmappedstatus : captured
         * key : d7XqbGKV
         * txnid : 1538647035037
         * amount : 17000.00
         * additionalCharges :
         * addedon : 2018-10-04 15:30:52
         * createdOn : 1538647258000
         * productinfo : product_info
         * firstname : suresh
         * lastname :
         * address1 :
         * address2 :
         * city :
         * state :
         * country :
         * zipcode :
         * email : ksuresh.unique@gmail.com
         * phone : 8985018103
         * udf1 :
         * udf2 :
         * udf3 :
         * udf4 :
         * udf5 :
         * udf6 :
         * udf7 :
         * udf8 :
         * udf9 :
         * udf10 :
         * hash : a6a0ed67f4d80b6098181a236a16ffd9fbf01e14ea5ad697420be255ba83c302dd761e8eedb86538f29fc5ce5216638b1223f942430768bb9e91b2423b1be5fe
         * field1 : 862951
         * field2 : 696416
         * field3 : 202804
         * field4 : MC
         * field5 : 101118670374
         * field6 : 00
         * field7 : 0
         * field8 : 3DS
         * field9 :  Verification of Secure Hash Failed: E700 -- Approved -- Transaction Successful -- Unable to be determined--E000
         * bank_ref_num : 862951
         * bankcode : MAST
         * error : E000
         * error_Message : No Error
         * cardToken :
         * offer_key :
         * offer_type :
         * offer_availed :
         * pg_ref_no :
         * offer_failure_reason :
         * name_on_card : payu
         * cardnum : 512345XXXXXX2346
         * cardhash : This field is no longer supported in postback params.
         * card_type :
         * card_merchant_param : null
         * version :
         * postUrl : https://www.payumoney.com/mobileapp/payumoney/success.php
         * calledStatus : false
         * additional_param :
         * amount_split : {"PAYU":"17000.0"}
         * discount : 0.00
         * net_amount_debit : 17000
         * fetchAPI : null
         * paisa_mecode :
         * meCode : {"vpc_Merchant":"TESTIBIBOWEB"}
         * payuMoneyId : 331893
         * encryptedPaymentId : null
         * id : null
         * surl : null
         * furl : null
         * baseUrl : null
         * retryCount : 0
         * merchantid : null
         * payment_source : null
         * pg_TYPE : AXISPG
         */

        var postBackParamId: Int = 0
        var mihpayid: String? = null
        var paymentId: Int = 0
        var mode: String? = null
        var status: String? = null
        var unmappedstatus: String? = null
        var key: String? = null
        var txnid: String? = null
        var amount: String? = null
        var additionalCharges: String? = null
        var addedon: String? = null
        var createdOn: Long = 0
        var productinfo: String? = null
        var firstname: String? = null
        var lastname: String? = null
        var address1: String? = null
        var address2: String? = null
        var city: String? = null
        var state: String? = null
        var country: String? = null
        var zipcode: String? = null
        var email: String? = null
        var phone: String? = null
        var udf1: String? = null
        var udf2: String? = null
        var udf3: String? = null
        var udf4: String? = null
        var udf5: String? = null
        var udf6: String? = null
        var udf7: String? = null
        var udf8: String? = null
        var udf9: String? = null
        var udf10: String? = null
        var hash: String? = null
        var field1: String? = null
        var field2: String? = null
        var field3: String? = null
        var field4: String? = null
        var field5: String? = null
        var field6: String? = null
        var field7: String? = null
        var field8: String? = null
        var field9: String? = null
        var bank_ref_num: String? = null
        var bankcode: String? = null
        var error: String? = null
        var error_Message: String? = null
        var cardToken: String? = null
        var offer_key: String? = null
        var offer_type: String? = null
        var offer_availed: String? = null
        var pg_ref_no: String? = null
        var offer_failure_reason: String? = null
        var name_on_card: String? = null
        var cardnum: String? = null
        var cardhash: String? = null
        var card_type: String? = null
        var card_merchant_param: Any? = null
        var version: String? = null
        var postUrl: String? = null
        var isCalledStatus: Boolean = false
        var additional_param: String? = null
        var amount_split: String? = null
        var discount: String? = null
        var net_amount_debit: String? = null
        var fetchAPI: Any? = null
        var paisa_mecode: String? = null
        var meCode: String? = null
        var payuMoneyId: String? = null
        var encryptedPaymentId: Any? = null
        var id: Any? = null
        var surl: Any? = null
        var furl: Any? = null
        var baseUrl: Any? = null
        var retryCount: Int = 0
        var merchantid: Any? = null
        var payment_source: Any? = null
        var pg_TYPE: String? = null
    }
}
