package com.innasoft.kilomart.Response

class PayuMoneyPutResponse {


    /**
     * status : 0
     * user_id :
     * order_id :
     * payment_id :
     * message : Online Transaction Failed
     */

    var status: Int = 0
    var user_id: String? = null
    var order_id: String? = null
    var payment_id: String? = null
    var message: String? = null
}
