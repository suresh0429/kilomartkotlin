package com.innasoft.kilomart.Response

class CartResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"records":[{"cart_id":"444","product_id":"196","c1_url":"staples","c2_url":"ravaflours ","c3_url":null,"product_name":"KILOMART premium-rice-flour-500gm","selling_price":"22.00","mrp_price":"24.00","purchase_quantity":"3","unit_id":"2","unit_name":"gms","unit_value":"500","brand_id":"1","brand_name":"KILOMART","images":"bf375-ricefllor.png","p_url":"staples/ravaflours /196","net_amount":66,"gross_amount":72},{"cart_id":"426","product_id":"36","c1_url":"beverages","c2_url":"healthdrinksmix","c3_url":null,"product_name":"Glucon-D Glucose Based Beverage Mix - Nimbu Pani Flavoured, 200 g Carton","selling_price":"57.00","mrp_price":"63.00","purchase_quantity":"17","unit_id":"2","unit_name":"gms","unit_value":"250","brand_id":"1","brand_name":"KILOMART","images":"6460c-7.png","p_url":"beverages/healthdrinksmix/36","net_amount":969,"gross_amount":1071},{"cart_id":"425","product_id":"193","c1_url":"staples","c2_url":"oils-and-ghee","c3_url":null,"product_name":"Fortune Kachi Ghani Mustard Oil, 500 ml Bottle","selling_price":"70.00","mrp_price":"74.00","purchase_quantity":"1","unit_id":"9","unit_name":"ml","unit_value":"500","brand_id":"1","brand_name":"KILOMART","images":"d7613-18.png","p_url":"staples/oils-and-ghee/193","net_amount":70,"gross_amount":74},{"cart_id":"424","product_id":"315","c1_url":"personalneeds","c2_url":"soaps-and-body-wash","c3_url":null,"product_name":"Santoor Sandal & Turmeric Soap","selling_price":"27.50","mrp_price":"28.00","purchase_quantity":"1","unit_id":"3","unit_name":"Piece","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"671c8-3.png","p_url":"personalneeds/soaps-and-body-wash/315","net_amount":27.5,"gross_amount":28},{"cart_id":"423","product_id":"9","c1_url":"staples","c2_url":"rice-and-riceproducts","c3_url":null,"product_name":"KILOMART idly-rice","selling_price":"91.00","mrp_price":"92.00","purchase_quantity":"2","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"43bbb-1.png","p_url":"staples/rice-and-riceproducts/9","net_amount":182,"gross_amount":184},{"cart_id":"418","product_id":"2","c1_url":"staples","c2_url":"dalsandpulses","c3_url":null,"product_name":"KILOMART urad-dal","selling_price":"99.00","mrp_price":"100.00","purchase_quantity":"5","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"995f9-urad-dal.png","p_url":"staples/dalsandpulses/2","net_amount":495,"gross_amount":500}],"final_gross_amount":"1,929.00","final_net_amount":"1,809.50","cart_discount":"0.00","coupon_discount":"0.00","voucher_discount":"0.00","final_discount":"119.50","payable_amount":"1,809.50"}
     */

    var status: String? = null
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * records : [{"cart_id":"444","product_id":"196","c1_url":"staples","c2_url":"ravaflours ","c3_url":null,"product_name":"KILOMART premium-rice-flour-500gm","selling_price":"22.00","mrp_price":"24.00","purchase_quantity":"3","unit_id":"2","unit_name":"gms","unit_value":"500","brand_id":"1","brand_name":"KILOMART","images":"bf375-ricefllor.png","p_url":"staples/ravaflours /196","net_amount":66,"gross_amount":72},{"cart_id":"426","product_id":"36","c1_url":"beverages","c2_url":"healthdrinksmix","c3_url":null,"product_name":"Glucon-D Glucose Based Beverage Mix - Nimbu Pani Flavoured, 200 g Carton","selling_price":"57.00","mrp_price":"63.00","purchase_quantity":"17","unit_id":"2","unit_name":"gms","unit_value":"250","brand_id":"1","brand_name":"KILOMART","images":"6460c-7.png","p_url":"beverages/healthdrinksmix/36","net_amount":969,"gross_amount":1071},{"cart_id":"425","product_id":"193","c1_url":"staples","c2_url":"oils-and-ghee","c3_url":null,"product_name":"Fortune Kachi Ghani Mustard Oil, 500 ml Bottle","selling_price":"70.00","mrp_price":"74.00","purchase_quantity":"1","unit_id":"9","unit_name":"ml","unit_value":"500","brand_id":"1","brand_name":"KILOMART","images":"d7613-18.png","p_url":"staples/oils-and-ghee/193","net_amount":70,"gross_amount":74},{"cart_id":"424","product_id":"315","c1_url":"personalneeds","c2_url":"soaps-and-body-wash","c3_url":null,"product_name":"Santoor Sandal & Turmeric Soap","selling_price":"27.50","mrp_price":"28.00","purchase_quantity":"1","unit_id":"3","unit_name":"Piece","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"671c8-3.png","p_url":"personalneeds/soaps-and-body-wash/315","net_amount":27.5,"gross_amount":28},{"cart_id":"423","product_id":"9","c1_url":"staples","c2_url":"rice-and-riceproducts","c3_url":null,"product_name":"KILOMART idly-rice","selling_price":"91.00","mrp_price":"92.00","purchase_quantity":"2","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"43bbb-1.png","p_url":"staples/rice-and-riceproducts/9","net_amount":182,"gross_amount":184},{"cart_id":"418","product_id":"2","c1_url":"staples","c2_url":"dalsandpulses","c3_url":null,"product_name":"KILOMART urad-dal","selling_price":"99.00","mrp_price":"100.00","purchase_quantity":"5","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"995f9-urad-dal.png","p_url":"staples/dalsandpulses/2","net_amount":495,"gross_amount":500}]
         * final_gross_amount : 1,929.00
         * final_net_amount : 1,809.50
         * cart_discount : 0.00
         * coupon_discount : 0.00
         * voucher_discount : 0.00
         * final_discount : 119.50
         * payable_amount : 1,809.50
         */

        var final_gross_amount: String? = null
        var final_net_amount: String? = null
        var cart_discount: String? = null
        var coupon_discount: String? = null
        var voucher_discount: String? = null
        var final_discount: String? = null
        var payable_amount: String? = null
        var records: List<RecordsBean>? = null

        class RecordsBean {
            /**
             * cart_id : 444
             * product_id : 196
             * c1_url : staples
             * c2_url : ravaflours
             * c3_url : null
             * product_name : KILOMART premium-rice-flour-500gm
             * selling_price : 22.00
             * mrp_price : 24.00
             * purchase_quantity : 3
             * unit_id : 2
             * unit_name : gms
             * unit_value : 500
             * brand_id : 1
             * brand_name : KILOMART
             * images : bf375-ricefllor.png
             * p_url : staples/ravaflours /196
             * net_amount : 66
             * gross_amount : 72
             */

            var cart_id: String? = null
            var product_id: String? = null
            var c1_url: String? = null
            var c2_url: String? = null
            var c3_url: Any? = null
            var product_name: String? = null
            var selling_price: Double? = 0.00
            var mrp_price: Double? = 0.00
            var purchase_quantity: String? = null
            var unit_id: String? = null
            var unit_name: String? = null
            var unit_value: String? = null
            var brand_id: String? = null
            var brand_name: String? = null
            var images: String? = null
            var p_url: String? = null
            var net_amount: Double = 0.00
            var gross_amount: Double = 0.00
        }
    }
}
