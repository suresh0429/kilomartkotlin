package com.innasoft.kilomart.Response

class WishListPostResponse {


    /**
     * status : 1
     * message : WishList Items has been Inserted
     */

    var status: Int = 0
    var message: String? = null
}
