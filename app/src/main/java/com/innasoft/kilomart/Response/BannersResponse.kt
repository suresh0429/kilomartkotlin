package com.innasoft.kilomart.Response

class BannersResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"0","highlighted_text":"Promo banner","image":"images/banners/app/c0b57-5357a-4.png"}]
     */

    var status: String? = null
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * id : 0
         * highlighted_text : Promo banner
         * image : images/banners/app/c0b57-5357a-4.png
         */

        var id: String? = null
        var highlighted_text: String? = null
        var image: String? = null
    }
}
