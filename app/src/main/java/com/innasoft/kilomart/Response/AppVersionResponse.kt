package com.innasoft.kilomart.Response

import com.google.gson.annotations.SerializedName

class AppVersionResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : {"android_version":"1.0","ios_version":"1.0"}
     */

    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("data")
    var data: DataBean? = null

    class DataBean {
        /**
         * android_version : 1.0
         * ios_version : 1.0
         */

        @SerializedName("android_version")
        var androidVersion: String? = null
        @SerializedName("ios_version")
        var iosVersion: String? = null
    }
}
