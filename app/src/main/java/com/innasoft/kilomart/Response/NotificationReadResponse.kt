package com.innasoft.kilomart.Response

import com.google.gson.annotations.SerializedName

class NotificationReadResponse {


    /**
     * status : 10100
     * message : Data updated successfully.
     */

    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
}
