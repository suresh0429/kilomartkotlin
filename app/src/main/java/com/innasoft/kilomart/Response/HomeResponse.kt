package com.innasoft.kilomart.Response

class HomeResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"1","name":"Staples","image":"images/category/main/d82ee-staples1.png"},{"id":"2","name":"Beverages","image":"images/category/main/a8bb1-beverages1.png"},{"id":"3","name":"Household Care","image":"images/category/main/2704f-householdcare1.png"},{"id":"4","name":"Personal Needs","image":"images/category/main/4b222-personlaneed1.png"},{"id":"5","name":"Baby care","image":"images/category/main/caa83-babycare1.png"},{"id":"6","name":"Packed Food","image":"images/category/main/2cfd7-packedfood1.png"}]
     */

    var status: String? = null
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * id : 1
         * name : Staples
         * image : images/category/main/d82ee-staples1.png
         */

        var id: String? = null
        var name: String? = null
        var image: String? = null
    }
}
