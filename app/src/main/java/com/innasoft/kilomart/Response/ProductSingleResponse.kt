package com.innasoft.kilomart.Response

class ProductSingleResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"id":"1","url_name":"toor-dal","product_name":"Toor Dal","type":"SINGLE","main_category_id":"1","main_category_name":"Staples","sub_category_id":"1","sub_category_name":"Dals And Pulses","child_category_id":"0","child_category_name":null,"unit_id":"1","unit_name":"Kg","unit_value":"2","brand_id":"1","brand_name":"KILOMART","qty":"10","mrp_price":"150.00","offer_price":"0","selling_price":"100.00","about":"kjbkj\n","moreinfo":"lknlk","availability":"0","user_rating":"0","features":"0","position":"1","seo_title":"toor-dal","seo_description":"Toor Dal","seo_keywords":"Toor Dal","images":["3bd75-toor-dal.jpeg"]}
     */

    var status: String? = null
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * id : 1
         * url_name : toor-dal
         * product_name : Toor Dal
         * type : SINGLE
         * main_category_id : 1
         * main_category_name : Staples
         * sub_category_id : 1
         * sub_category_name : Dals And Pulses
         * child_category_id : 0
         * child_category_name : null
         * unit_id : 1
         * unit_name : Kg
         * unit_value : 2
         * brand_id : 1
         * brand_name : KILOMART
         * qty : 10
         * mrp_price : 150.00
         * offer_price : 0
         * selling_price : 100.00
         * about : kjbkj
         * moreinfo : lknlk
         * availability : 0
         * user_rating : 0
         * features : 0
         * position : 1
         * seo_title : toor-dal
         * seo_description : Toor Dal
         * seo_keywords : Toor Dal
         * images : ["3bd75-toor-dal.jpeg"]
         */

        var id: String? = null
        var url_name: String? = null
        var product_name: String? = null
        var type: String? = null
        var main_category_id: String? = null
        var main_category_name: String? = null
        var sub_category_id: String? = null
        var sub_category_name: String? = null
        var child_category_id: String? = null
        var child_category_name: Any? = null
        var unit_id: String? = null
        var unit_name: String? = null
        var unit_value: String? = null
        var brand_id: String? = null
        var brand_name: String? = null
        var qty: String? = null
        var mrp_price: Double? = null
        var offer_price: Double? = null
        var selling_price: Double? = null
        var about: String? = null
        var moreinfo: String? = null
        var availability: String? = null
        var user_rating: String? = null
        var features: String? = null
        var position: String? = null
        var seo_title: String? = null
        var seo_description: String? = null
        var seo_keywords: String? = null
        var images: List<String>? = null
    }
}
