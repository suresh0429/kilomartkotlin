package com.innasoft.kilomart.Response

class LocationsResponse {


    /**
     * status : 10100
     * message : Data Fetch successfully.
     * data : [{"pincode":"500033","area":"Jublee Hills","shipping_charges":"20"},{"pincode":"500007","area":"Habsiguda","shipping_charges":"50"}]
     */

    var status: String? = null
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * pincode : 500033
         * area : Jublee Hills
         * shipping_charges : 20
         */

        var pincode: String? = null
        var area: String? = null
        var shipping_charges: Double? = null
    }
}
