package com.innasoft.kilomart.Response

import com.google.gson.annotations.SerializedName

class OrderListResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"total_products":2,"order_details":{"reference_id":"KM2019091604072927","order_confirmation_reference_id":"KM2019091604072998","total_mrp_price":"450","discount":"0","shipping_charges":"0","final_price":"450","cancel_charges":"0","refund_price":"0","pincode":"500081","medium":"ANDROID","order_status":"Pending","status":"0","getaway_name":"CcAvenue","ip_address":"157.48.97.171","expected_delivery":"N/A","expected_delivery_slot":"N/A","order_date":"16-09-2019 04:07 pm"},"status":[],"products":[{"id":"5","product_id":"5","product_name":"Peanuts","images":"3542a-peanuts.png","product_mrp_price":"60","purchase_quantity":"5","total_price":"300","status":"0"},{"id":"16","product_id":"4","product_name":"Chana Dal","images":"64121-chana-dal.png","product_mrp_price":"100","purchase_quantity":"5","total_price":"450","status":"0"}]}
     */

    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("data")
    var data: DataBean? = null

    class DataBean {
        /**
         * total_products : 2
         * order_details : {"reference_id":"KM2019091604072927","order_confirmation_reference_id":"KM2019091604072998","total_mrp_price":"450","discount":"0","shipping_charges":"0","final_price":"450","cancel_charges":"0","refund_price":"0","pincode":"500081","medium":"ANDROID","order_status":"Pending","status":"0","getaway_name":"CcAvenue","ip_address":"157.48.97.171","expected_delivery":"N/A","expected_delivery_slot":"N/A","order_date":"16-09-2019 04:07 pm"}
         * status : []
         * products : [{"id":"5","product_id":"5","product_name":"Peanuts","images":"3542a-peanuts.png","product_mrp_price":"60","purchase_quantity":"5","total_price":"300","status":"0"},{"id":"16","product_id":"4","product_name":"Chana Dal","images":"64121-chana-dal.png","product_mrp_price":"100","purchase_quantity":"5","total_price":"450","status":"0"}]
         */

        @SerializedName("total_products")
        var totalProducts: Int = 0
        @SerializedName("order_details")
        var orderDetails: OrderDetailsBean? = null
        @SerializedName("status")
        var status: List<*>? = null
        @SerializedName("products")
        var products: List<ProductsBean>? = null

        class OrderDetailsBean {
            /**
             * reference_id : KM2019091604072927
             * order_confirmation_reference_id : KM2019091604072998
             * total_mrp_price : 450
             * discount : 0
             * shipping_charges : 0
             * final_price : 450
             * cancel_charges : 0
             * refund_price : 0
             * pincode : 500081
             * medium : ANDROID
             * order_status : Pending
             * status : 0
             * getaway_name : CcAvenue
             * ip_address : 157.48.97.171
             * expected_delivery : N/A
             * expected_delivery_slot : N/A
             * order_date : 16-09-2019 04:07 pm
             */

            @SerializedName("reference_id")
            var referenceId: String? = null
            @SerializedName("order_confirmation_reference_id")
            var orderConfirmationReferenceId: String? = null
            @SerializedName("total_mrp_price")
            var totalMrpPrice: String? = null
            @SerializedName("discount")
            var discount: String? = null
            @SerializedName("shipping_charges")
            var shippingCharges: String? = null
            @SerializedName("final_price")
            var finalPrice: String? = null
            @SerializedName("cancel_charges")
            var cancelCharges: String? = null
            @SerializedName("refund_price")
            var refundPrice: String? = null
            @SerializedName("pincode")
            var pincode: String? = null
            @SerializedName("medium")
            var medium: String? = null
            @SerializedName("order_status")
            var orderStatus: String? = null
            @SerializedName("status")
            var status: String? = null
            @SerializedName("getaway_name")
            var getawayName: String? = null
            @SerializedName("ip_address")
            var ipAddress: String? = null
            @SerializedName("expected_delivery")
            var expectedDelivery: String? = null
            @SerializedName("expected_delivery_slot")
            var expectedDeliverySlot: String? = null
            @SerializedName("order_date")
            var orderDate: String? = null
        }

        class ProductsBean {
            /**
             * id : 5
             * product_id : 5
             * product_name : Peanuts
             * images : 3542a-peanuts.png
             * product_mrp_price : 60
             * purchase_quantity : 5
             * total_price : 300
             * status : 0
             */

            @SerializedName("id")
            var id: String? = null
            @SerializedName("product_id")
            var productId: String? = null
            @SerializedName("product_name")
            var productName: String? = null
            @SerializedName("images")
            var images: String? = null
            @SerializedName("product_mrp_price")
            var productMrpPrice: String? = null
            @SerializedName("purchase_quantity")
            var purchaseQuantity: String? = null
            @SerializedName("total_price")
            var totalPrice: String? = null
            @SerializedName("status")
            var status: String? = null
        }
    }
}
