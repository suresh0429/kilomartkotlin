package com.innasoft.kilomart.Response

class AddressResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"2","name":"suresh","address_line1":"near ","address_line2":"VV Nagar","area":"Parvat nagar","city":"Hyderabad","state":"Telangana","pincode":"500081","country":"India","contact_no":"8985018103","alternate_contact_no":"8919480920","latitude":"17.487","longitude":"72.7638","is_default":"Yes","delivery_status":true,"delivery_charges":"0"}]
     */

    var status: String? = null
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * id : 2
         * name : suresh
         * address_line1 : near
         * address_line2 : VV Nagar
         * area : Parvat nagar
         * city : Hyderabad
         * state : Telangana
         * pincode : 500081
         * country : India
         * contact_no : 8985018103
         * alternate_contact_no : 8919480920
         * latitude : 17.487
         * longitude : 72.7638
         * is_default : Yes
         * delivery_status : true
         * delivery_charges : 0
         */

        var id: String? = null
        var name: String? = null
        var address_line1: String? = null
        var address_line2: String? = null
        var area: String? = null
        var city: String? = null
        var state: String? = null
        var pincode: String? = null
        var country: String? = null
        var contact_no: String? = null
        var alternate_contact_no: String? = null
        var latitude: String? = null
        var longitude: String? = null
        var is_default: String? = null
        var isDelivery_status: Boolean = false
        var delivery_charges: String? = null
    }
}
