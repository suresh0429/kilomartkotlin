package com.innasoft.kilomart.Response

class WishlistResponse {


    /**
     * status : 1
     * wishList : [{"id":"2019","product_id":"PIK727DH6","item_id":"354","image":"https://pickany24x7.com/stationery/stationery/../images/product_images/PIK727DH6/1.jpg","product_name":"Mathematical Instrument Box","inclTax":"150.00","dis_price":"135","dis_percent":"10","category":"stationery","status":"0"}]
     * message : Wish List Items
     */

    var status: Int = 0
    var message: String? = null
    var wishList: List<WishListBean>? = null

    class WishListBean {
        /**
         * id : 2019
         * product_id : PIK727DH6
         * item_id : 354
         * image : https://pickany24x7.com/stationery/stationery/../images/product_images/PIK727DH6/1.jpg
         * product_name : Mathematical Instrument Box
         * inclTax : 150.00
         * dis_price : 135
         * dis_percent : 10
         * category : stationery
         * status : 0
         */

        var id: String? = null
        var product_id: String? = null
        var item_id: String? = null
        var image: String? = null
        var product_name: String? = null
        var inclTax: String? = null
        var dis_price: String? = null
        var dis_percent: String? = null
        var category: String? = null
        var status: String? = null
    }
}
