package com.innasoft.kilomart.Response

class WishListDeleteresponse {


    /**
     * status : Item Deleted Successfully From WishList
     * message : Item Deleted Successfully from WishList
     */

    var status: String? = null
    var message: String? = null
}
