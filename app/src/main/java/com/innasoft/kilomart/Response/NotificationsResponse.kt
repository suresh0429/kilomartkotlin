package com.innasoft.kilomart.Response

import com.google.gson.annotations.SerializedName

class NotificationsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"1","notification_id":"1","title":"cxvxcv","message":"cvzxcv","created_on":"0000-00-00 00:00:00","is_read":"122","image":""}]
     */

    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("data")
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * id : 1
         * notification_id : 1
         * title : cxvxcv
         * message : cvzxcv
         * created_on : 0000-00-00 00:00:00
         * is_read : 122
         * image :
         */

        @SerializedName("id")
        var id: String? = null
        @SerializedName("notification_id")
        var notificationId: String? = null
        @SerializedName("title")
        var title: String? = null
        @SerializedName("message")
        var message: String? = null
        @SerializedName("created_on")
        var createdOn: String? = null
        @SerializedName("is_read")
        var isRead: String? = null
        @SerializedName("image")
        var image: String? = null
    }
}
