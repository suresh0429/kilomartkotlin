package com.innasoft.kilomart.Response

class BaseResponse {


    /**
     * status : 10100
     * message : OTP has been send to your mobile.
     */

    var status: String? = null
    var message: String? = null
}
