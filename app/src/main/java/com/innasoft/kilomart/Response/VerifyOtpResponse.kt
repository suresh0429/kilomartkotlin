package com.innasoft.kilomart.Response

class VerifyOtpResponse {


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njg3MDE5MDcsImp0aSI6Ik8rXC9XQzIySFBXdWRVTHJSOE80YURocER5Sm9jeGhMYWo5eHl5bG0yK1pRPSIsImlzcyI6Imh0dHA6XC9cL2lubmFzb2Z0LmluXC9waHAtanNvblwvIiwibmJmIjoxNTY4NzAxOTA4LCJleHAiOjE2MDAyMzc5MDgsImRhdGEiOnsidXNlcl9pZCI6IjEzIn19.7-e7WMrFMVxIKbWmLE2tL56iGwYQeGbw-dbWYfwdYZEaCf6fY4zlZvWUgD4mY2K9FUsvMeSXrFELpSfMWJRe-w","user_id":"13","user_name":"Bhavani","email":"bhavani@innasoft.in","mobile":"9542410774","gender":""}
     */

    var status: String? = null
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Njg3MDE5MDcsImp0aSI6Ik8rXC9XQzIySFBXdWRVTHJSOE80YURocER5Sm9jeGhMYWo5eHl5bG0yK1pRPSIsImlzcyI6Imh0dHA6XC9cL2lubmFzb2Z0LmluXC9waHAtanNvblwvIiwibmJmIjoxNTY4NzAxOTA4LCJleHAiOjE2MDAyMzc5MDgsImRhdGEiOnsidXNlcl9pZCI6IjEzIn19.7-e7WMrFMVxIKbWmLE2tL56iGwYQeGbw-dbWYfwdYZEaCf6fY4zlZvWUgD4mY2K9FUsvMeSXrFELpSfMWJRe-w
         * user_id : 13
         * user_name : Bhavani
         * email : bhavani@innasoft.in
         * mobile : 9542410774
         * gender :
         */

        var jwt: String? = null
        var user_id: String? = null
        var user_name: String? = null
        var email: String? = null
        var mobile: String? = null
        var gender: String? = null
    }
}
