package com.innasoft.kilomart.Response

class CartCountResponse {


    /**
     * status : 10100
     * message : Data save successfully
     * data : 3
     */

    var status: String? = null
    var message: String? = null
    var data: Int = 0
}
