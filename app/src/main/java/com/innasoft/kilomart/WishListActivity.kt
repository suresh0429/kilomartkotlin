package com.innasoft.kilomart

import android.content.Intent
import android.graphics.Color
import android.os.Bundle

import android.text.Html
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView


import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.innasoft.kilomart.Adapters.WishListAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Storage.PrefManager
import com.innasoft.kilomart.Response.WishlistResponse
import com.innasoft.kilomart.Singleton.AppController

import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_wish_list.*
import retrofit2.Call
import retrofit2.Callback

class WishListActivity : AppCompatActivity() {
    internal lateinit var appController: AppController


    private var wishListAdapter: WishListAdapter? = null
    private var pref: PrefManager? = null
    internal var wishlistResponse = WishlistResponse()
    internal var gson: Gson? = null
    internal var userId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wish_list)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "My Wishlist"

        appController = application as AppController
        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]


        if (appController.isConnection) {

            wishListData()


        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }


        }


    }

    private fun wishListData() {

        progressBar!!.visibility = View.VISIBLE

        val call = RetrofitClient.instance!!.api.getWhishlisList(userId)

        call.enqueue(object : Callback<WishlistResponse> {
            override fun onResponse(
                call: Call<WishlistResponse>,
                response: retrofit2.Response<WishlistResponse>
            ) {
                progressBar!!.visibility = View.GONE
                val wishlistResponse = response.body()

                if (response.isSuccessful) {

                    progressBar!!.visibility = View.GONE

                    if (wishlistResponse?.status ?: 0 == 1) {


                        // wishlist Adapter
                        val mLayoutManager = LinearLayoutManager(applicationContext)
                        recyclerWishlist!!.layoutManager = mLayoutManager
                        recyclerWishlist!!.itemAnimator = DefaultItemAnimator()

                        wishListAdapter =
                            WishListAdapter(this@WishListActivity,
                                wishlistResponse!!.wishList as MutableList<WishlistResponse.WishListBean>
                            )
                        recyclerWishlist!!.adapter = wishListAdapter
                        wishListAdapter!!.notifyDataSetChanged()


                    } else {
                        txtError!!.visibility = View.VISIBLE
                        txtError!!.text = wishlistResponse!!.message
                    }


                } else {
                    // error case
                    when (response.code()) {
                        404 -> Snackbar.make(
                            recyclerWishlist!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.not_found) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        500 -> Snackbar.make(
                            recyclerWishlist!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.server_broken) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        else -> Snackbar.make(
                            recyclerWishlist!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.unknown_error) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }

            }

            override fun onFailure(call: Call<WishlistResponse>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Snackbar.make(
                    recyclerWishlist!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}