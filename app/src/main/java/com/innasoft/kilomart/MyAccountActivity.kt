package com.innasoft.kilomart

import android.content.Intent
import android.os.Bundle

import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.widget.NestedScrollView

import com.innasoft.kilomart.Reciever.ConnectivityReceiver
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_my_account.*

class MyAccountActivity : AppCompatActivity(), View.OnClickListener {


    private var pref: PrefManager? = null
    internal var userId: String? = null
    internal var checkoutStatus: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        ButterKnife.bind(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "My Account"



        if (intent.extras != null) {
            checkoutStatus = intent.getBooleanExtra("Checkout", false)

        }


        val app = application as AppController
        pref = PrefManager(applicationContext)

        // Method to manually check connection status

        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        val username = profile["name"]
        val mobile = profile["mobile"]
        val email = profile["email"]
        val imagePic = profile["profilepic"]


        txtAccName!!.text = username
        txtAccMobile!!.text = mobile
        txtMail!!.text = email

        ChangePassword.setOnClickListener(this)
        myAddressCard.setOnClickListener(this)
        myOrdersCard.setOnClickListener(this)
        myWhishListCard.setOnClickListener(this)
        myNotificationsCard.setOnClickListener(this)
        editProfile.setOnClickListener(this)



    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ChangePassword -> {
                val editMyAcc = Intent(this@MyAccountActivity, ChangePasswordActivity::class.java)
                editMyAcc.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(editMyAcc)
            }
            R.id.myAddressCard -> {
                val address = Intent(this@MyAccountActivity, AddressListActivity::class.java)
                address.putExtra("Checkout", false)
                address.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(address)
            }
            R.id.myOrdersCard -> {
                val orders = Intent(this@MyAccountActivity, OrderIdActvity::class.java)
                orders.putExtra("Checkout", false)
                orders.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(orders)
            }
            R.id.myWhishListCard -> {
                val wishlist = Intent(this@MyAccountActivity, WishListActivity::class.java)
                wishlist.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(wishlist)
            }
            R.id.editProfile -> {
                val vechils = Intent(this@MyAccountActivity, UpdateProfileActivity::class.java)
                vechils.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(vechils)
            }
            R.id.myNotificationsCard -> {
                val service = Intent(this@MyAccountActivity, NotificationActivity::class.java)
                service.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(service)
            }
        }    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }



}
