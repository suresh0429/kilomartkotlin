package com.innasoft.kilomart

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.CartAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Interface.CartProductClickListener
import com.innasoft.kilomart.Model.Product
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Response.CartResponse
import com.innasoft.kilomart.Response.WishListPostResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.ArrayList

import butterknife.ButterKnife
import com.innasoft.kilomart.Storage.Utilities
import kotlinx.android.synthetic.main.activity_cart.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log

class CartActivity : AppCompatActivity(), CartProductClickListener {

    internal lateinit var productArrayList: ArrayList<Product>
    internal lateinit var cartProductClickListener: CartProductClickListener
    internal lateinit var cartAdapter: CartAdapter
    internal lateinit var appController: AppController
    private var pref: PrefManager? = null

    internal var userId: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    //int totalQty = 0;
    internal var totalQuantity = 0
    internal var minimumOrder = 500

    lateinit var length: String
    var module: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Cart"

        cartProductClickListener = this

        appController = application as AppController

        pref = PrefManager(applicationContext)
        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userId = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]


        if (appController.isConnection) {

            // Showing progress dialog before making http request

            progressBar!!.visibility = View.VISIBLE

            // cart count Update
            appController.cartCount(userId, deviceId)

            cartData()


        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                invalidateOptionsMenu()
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }


    override fun onMinusClick(cartItemsBean: Product) {
        val i = productArrayList.indexOf(cartItemsBean)

        if (cartItemsBean.purchase_quantity > 1) {


            val updatedProduct = Product(
                cartItemsBean.id,
                cartItemsBean.product_id,
                cartItemsBean.product_name,
                cartItemsBean.selling_price,
                cartItemsBean.mrp_price,
                cartItemsBean.purchase_quantity - 1,
                cartItemsBean.unit_id,
                cartItemsBean.unit_name,
                cartItemsBean.unit_value,
                cartItemsBean.brand_id,
                cartItemsBean.brand_name,
                cartItemsBean.net_amount,
                cartItemsBean.gross_amount,
                cartItemsBean.images
            )

            productArrayList.remove(cartItemsBean)
            productArrayList.add(i, updatedProduct)

            updateQuantity(updatedProduct)
            cartAdapter.notifyDataSetChanged()


            calculateCartTotal()

        }
    }

    override fun onPlusClick(cartItemsBean: Product) {
        val i = productArrayList.indexOf(cartItemsBean)

        val updatedProduct = Product(
            cartItemsBean.id,
            cartItemsBean.product_id,
            cartItemsBean.product_name,
            cartItemsBean.selling_price,
            cartItemsBean.mrp_price,
            cartItemsBean.purchase_quantity + 1,
            cartItemsBean.unit_id,
            cartItemsBean.unit_name,
            cartItemsBean.unit_value,
            cartItemsBean.brand_id,
            cartItemsBean.brand_name,
            cartItemsBean.net_amount,
            cartItemsBean.gross_amount,
            cartItemsBean.images
        )

        productArrayList.remove(cartItemsBean)
        productArrayList.add(i, updatedProduct)

        Log.e("QUNATITY", "" + updatedProduct.purchase_quantity)
        updateQuantity(updatedProduct)

        cartAdapter.notifyDataSetChanged()


        calculateCartTotal()
    }

    override fun onSaveClick(product: Product) {

    }

    override fun onRemoveDialog(product: Product) {
        val dialog = Dialog(this@CartActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog)


        // set the custom dialog components - text, image and button
        val te = dialog.findViewById<View>(R.id.txtAlert) as TextView
        te.text = "Are you want to Delete?"

        val yes = dialog.findViewById<View>(R.id.btnYes) as TextView
        val no = dialog.findViewById<View>(R.id.btnNo) as TextView

        yes.setOnClickListener {
            delete(product)
            cartAdapter.notifyDataSetChanged()
            dialog.dismiss()
            Snackbar.make(parentLayout!!, "Deleted Successfully", Snackbar.LENGTH_SHORT).show()
        }
        no.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    override fun onWishListDialog(product: Product) {

        val dialog = Dialog(this@CartActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog)


        // set the custom dialog components - text, image and button
        val te = dialog.findViewById<View>(R.id.txtAlert) as TextView
        te.text = "Add to WishList?"

        val yes = dialog.findViewById<View>(R.id.btnYes) as TextView
        val no = dialog.findViewById<View>(R.id.btnNo) as TextView

        yes.setOnClickListener {
            saveWishList(product)
            cartAdapter.notifyDataSetChanged()
            dialog.dismiss()
        }
        no.setOnClickListener { dialog.dismiss() }

        dialog.show()


    }

    // Remove from Cart
    fun delete(product: Product) {
        // Showing progress dialog before making http request
        progressBar!!.visibility = View.VISIBLE

        val call = RetrofitClient.instance!!.api.deleteCart(userId, product.id, deviceId)
        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                progressBar!!.visibility = View.GONE
                val wishListPostResponse = response.body()

                if (response.isSuccessful) {

                    if (wishListPostResponse!!.status.equals("10100", ignoreCase = true)) {

                        val i = productArrayList.indexOf(product)
                        if (i == -1) {
                            throw IndexOutOfBoundsException()
                        }
                        productArrayList.remove(productArrayList[i])

                        // cart count Update
                        appController.cartCount(userId, deviceId)
                        invalidateOptionsMenu()

                        calculateCartTotal()
                        cartAdapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                progressBar!!.visibility = View.GONE
            }
        })
    }

    // Save Wishlist
    fun saveWishList(product: Product) {

        progressBar!!.visibility = View.VISIBLE

        val call = RetrofitClient.instance!!.api.addWishList(
            product.brand_id,
            userId,
            product.brand_id,
            "move"
        )
        call.enqueue(object : Callback<WishListPostResponse> {
            override fun onResponse(
                call: Call<WishListPostResponse>,
                response: Response<WishListPostResponse>
            ) {

                progressBar!!.visibility = View.GONE

                val wishListPostResponse = response.body()

                if (response.isSuccessful) {

                    if (wishListPostResponse?.status ?: 0 == 1) {

                        val i = productArrayList.indexOf(product)
                        if (i == -1) {
                            throw IndexOutOfBoundsException()
                        }
                        productArrayList.remove(productArrayList[i])

                        Snackbar.make(
                            parentLayout!!,
                            "Item Added to Wishlist",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    } else {
                        Snackbar.make(
                            parentLayout!!,
                            "Item already Added to Wishlist",
                            Snackbar.LENGTH_SHORT
                        ).show()


                    }

                    // cart count Update
                    appController.cartCount(userId, deviceId)
                    invalidateOptionsMenu()

                    calculateCartTotal()
                    cartAdapter.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<WishListPostResponse>, t: Throwable) {

                progressBar!!.visibility = View.GONE
            }
        })
    }


    private fun cartData() {

        productArrayList = ArrayList()

        val call = RetrofitClient.instance!!.api.cartItemsList(userId, deviceId)
        call.enqueue(object : Callback<CartResponse> {
            override fun onResponse(call: Call<CartResponse>, response: Response<CartResponse>) {

                progressBar!!.visibility = View.GONE

                val cartResponse = response.body()

                if (response.isSuccessful) {


                    assert(cartResponse != null)
                    if (cartResponse!!.status.equals("10100", ignoreCase = true)) {

                        for (cartItemsBean in cartResponse.data!!.records!!) {


                            productArrayList.add(
                                Product(
                                    cartItemsBean.cart_id,
                                    cartItemsBean.product_id,
                                    cartItemsBean.product_name,
                                    cartItemsBean.selling_price!!.toDouble(),
                                    cartItemsBean.mrp_price!!.toDouble(),
                                    Integer.parseInt(cartItemsBean.purchase_quantity!!),
                                    cartItemsBean.unit_id,
                                    cartItemsBean.unit_name,
                                    cartItemsBean.unit_value,
                                    cartItemsBean.brand_id,
                                    cartItemsBean.brand_name,
                                    cartItemsBean.net_amount!!.toDouble(),
                                    cartItemsBean.gross_amount!!.toDouble(),
                                    cartItemsBean.images
                                )
                            )


                            Log.e("LENGTH", "" + productArrayList.size)

                            length = productArrayList.size.toString()


                        }

                        val mLayoutManager1 = LinearLayoutManager(
                            applicationContext,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        productsRcycler!!.layoutManager = mLayoutManager1
                        productsRcycler!!.itemAnimator = DefaultItemAnimator()
                        productsRcycler!!.setHasFixedSize(true)

                        cartAdapter = CartAdapter(
                            this@CartActivity,
                            productArrayList,
                            cartProductClickListener
                        )
                        productsRcycler!!.adapter = cartAdapter
                        cartAdapter.notifyDataSetChanged()
                        // calculate total amount
                        calculateCartTotal()


                    } else if (cartResponse.status == "10200") {

                        Toast.makeText(applicationContext, cartResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    } else if (cartResponse.status == "10300") {

                        setContentView(R.layout.emptycart)
                        val btnGotohome = findViewById<View>(R.id.btnGotohome) as Button
                        btnGotohome.setOnClickListener {
                            val intent = Intent(this@CartActivity, HomeActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }

                    } else if (cartResponse.status == "10400") {

                        Toast.makeText(applicationContext, cartResponse.message, Toast.LENGTH_SHORT)
                            .show()
                    }

                }
            }

            override fun onFailure(call: Call<CartResponse>, t: Throwable) {

                progressBar!!.visibility = View.GONE
                Snackbar.make(
                    parentLayout!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + t.message + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()



            }
        })


    }

    // update quantity
    fun updateQuantity(product: Product) {

        val call = RetrofitClient.instance!!.api.updateQuantity(
            product.id,
            product.purchase_quantity.toString()
        )
        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {

                if (response.isSuccessful) {

                    val baseResponse = response.body()

                    if (baseResponse!!.status.equals("10100", ignoreCase = true)) {

                        //Toast.makeText(getApplicationContext(),baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.status.equals("10200", ignoreCase = true)) {
                        // Toast.makeText(getApplicationContext(),baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }


            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

            }
        })
    }


    // total Amount
    fun calculateCartTotal() {

        var grandTotal = 0.00

        for (order in productArrayList) {

            grandTotal += order.selling_price!! * order.purchase_quantity

        }

        Log.e("TOTAL", "" + productArrayList.size + "Items | " + "  DISCOUNT : " + grandTotal)

        txtSubTotal!!.text = productArrayList.size.toString() + " Items | Rs " + Utilities.df.format(grandTotal)

        if (grandTotal >= minimumOrder) {

            btnCheckOut!!.visibility = View.VISIBLE
            txtAlert!!.visibility = View.GONE
        } else {

            btnCheckOut!!.visibility = View.INVISIBLE
            txtAlert!!.visibility = View.VISIBLE
            txtAlert!!.text = "Minimum Order Must Be Greater Than Rs.$minimumOrder*"

        }

        btnCheckOut!!.setOnClickListener {
            val intent = Intent(this@CartActivity, CheckoutActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("Checkout", true)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }


        if (productArrayList.size == 0) {

            setContentView(R.layout.emptycart)
            val btnGotohome = findViewById<View>(R.id.btnGotohome) as Button
            btnGotohome.setOnClickListener {
                val intent = Intent(this@CartActivity, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }

    }


    private fun ParseDouble(strNumber: String?): Double {
        return if (strNumber != null && strNumber.length > 0) {
            try {
                java.lang.Double.parseDouble(strNumber)
            } catch (e: Exception) {
                -1.0   // or some value to mark this field is wrong. or make a function validates field first ...
            }

        } else
            0.0
    }


    override fun onRestart() {
        super.onRestart()

        // cart count Update
        appController.cartCount(userId, deviceId)

        cartData()
    }

    override fun onStart() {
        super.onStart()

        // cart count Update
        appController.cartCount(userId, deviceId)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> {
                invalidateOptionsMenu()
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {

        super.onBackPressed()
    }


}
