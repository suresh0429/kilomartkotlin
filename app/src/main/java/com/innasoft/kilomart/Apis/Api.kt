package com.innasoft.kilomart.Apis


import com.innasoft.kilomart.Response.*
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {


    @get:GET("banners")
    val homeBanners: Call<BannersResponse>


    @get:GET("locations")
    val locations: Call<LocationsResponse>

    @FormUrlEncoded
    @POST("users/verify-login")
    fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("browser_id") brower_id: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("users/registration")
    fun userRegistrationRequest(
        @Field("name") name: String, @Field("email") email: String,
        @Field("mobile") mobile: String, @Field("conf_pwd") conf_pwd: String
    ): Call<BaseResponse>


    @FormUrlEncoded
    @POST("users/verify-otp")
    fun userOtpRequest(
        @Field("mobile") mobile: String?,
        @Field("otp") otp: String?,
        @Field("browser_id") browser_id: String?
    ): Call<VerifyOtpResponse>

    @FormUrlEncoded
    @POST("users/resend-otp")
    fun ResendOtpRequest(@Field("mobile") mobile: String?): Call<BaseResponse>


    @FormUrlEncoded
    @POST("users/forgot-password")
    fun userForgotPassword(@Field("mobile") mobile: String): Call<BaseResponse>


    @FormUrlEncoded
    @POST("users/verify-otp-forgot-password")
    fun userVerifyForgotPassword(
        @Field("mobile") mobile: String,
        @Field("otp") otp: String,
        @Field("browser_id") browser_id: String,
        @Field("new_pwd") new_pwd: String
    ): Call<BaseResponse>


    @FormUrlEncoded
    @POST("user/{id}/logout")
    fun userLogout(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") id: String?, @Field(
            "browser_id"
        ) browser_id: String?
    ): Call<BaseResponse>

    @FormUrlEncoded
    @POST("user/{id}/change-password")
    fun userChangePassword(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") id: String?, @Field(
            "old_pwd"
        ) old_pwd: String, @Field("new_pwd") new_pwd: String
    ): Call<BaseResponse>

    @FormUrlEncoded
    @POST("user/{id}/profile")
    fun updateProfile(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") id: String?,
        @Field("name") name: String?, @Field("email") email: String?,
        @Field("mobile") mobile: String, @Field("gender") gender: String
    ): Call<BaseResponse>


    @GET("categories")
    fun getHomePageRequest(
        @Query("type") type: String, @Query("main_category") main_category: String?, @Query(
            "sub_category"
        ) sub_category: String
    ): Call<HomeResponse>


    @GET("products")
    fun getPopularProduct(
        @Query("type") type: String,
        @Query("browser_id") browser_id: String?,
        @Query("sort_by") sort_by: String
        //@Query("search") String search
    ): Call<ProductResponse>


    @GET("products")
    fun getProductSearch(
        @Query("type") type: String,
        /*@Query("user_id") String user_id,*/
        @Query("browser_id") browser_id: String?,
        @Query("search") search: String

    ): Call<ProductResponse>


    @GET("products")
    fun getProductList(
        @Query("main_category") main_category: String?,
        @Query("sub_category") sub_category: String?,
        @Query("child_category") child_category: String,
        @Query("type") type: String,
        @Query("page") page: String,
        @Query("user_id") user_id: String?,
        @Query("browser_id") browser_id: String?,
        @Query("search") search: String,
        @Query("brand_id") brand_id: String,
        @Query("sort_by") sort_by: String
    ): Call<ProductResponse>


    @GET("product/{id}")
    fun getProductdetails(@Path("id") productId: String?, @Query("browser_id") browser_id: String?): Call<ProductSingleResponse>

    @FormUrlEncoded
    @POST("cart")
    fun addtoCart(
        @Field("product_id") product_id: String, @Field("browser_id") browser_id: String?,
        @Field("user_id") user_id: String?, @Field("purchase_quantity") purchase_quantity: String
    ): Call<BaseResponse>


    @GET("cart")
    fun cartItemsList(@Query("user_id") userId: String?, @Query("browser_id") browser_id: String?): Call<CartResponse>


    @FormUrlEncoded
    @POST("cart/quantity")
    fun updateQuantity(
        @Field("cart_id") cart_id: String?,
        @Field("purchase_quantity") purchase_quantity: String?
    ): Call<BaseResponse>

    @FormUrlEncoded
    @POST("cart/delete")
    fun deleteCart(@Field("user_id") user_id: String?, @Field("cart_id") cart_id: String?, @Field("browser_id") browser_id: String?): Call<BaseResponse>


    @GET("cart/count")
    fun getCartCount(@Query("user_id") userId: String?, @Query("browser_id") browser_id: String?): Call<CartCountResponse>


    @GET("user/{id}/addresses")
    fun getAddress(@Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?): Call<AddressResponse>


    @FormUrlEncoded
    @POST("user/{id}/address")
    fun userAddress(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?,
        @Field("name") name: String, @Field("address_line1") address_line1: String,
        @Field("address_line2") address_line2: String, @Field("area") area: String,
        @Field("city") city: String, @Field("state") state: String,
        @Field("pincode") pincode: String, @Field("contact_no") contact_no: String,
        @Field("alternate_contact_no") alternate_contact_no: String, @Field("latitude") latitude: String,
        @Field("longitude") longitude: String, @Field("is_default") is_default: String
    ): Call<BaseResponse>


    @FormUrlEncoded
    @POST("user/{id}/address/{id1}")
    fun updateuserAddress(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?, @Path("id1") addressid: String?,
        @Field("name") name: String, @Field("address_line1") address_line1: String,
        @Field("address_line2") address_line2: String, @Field("area") area: String,
        @Field("city") city: String, @Field("state") state: String,
        @Field("pincode") pincode: String, @Field("contact_no") contact_no: String,
        @Field("alternate_contact_no") alternate_contact_no: String, @Field("latitude") latitude: String,
        @Field("longitude") longitude: String, @Field("is_default") is_default: String
    ): Call<BaseResponse>


    @DELETE("user/{id}/address/{id1}")
    fun deleteuserAddress(
        @Header("Authorization-Basic") tokenValue: String, @Path("id") userId: String, @Path(
            "id1"
        ) addressid: String?
    ): Call<BaseResponse>


    @GET("checkout")
    fun checkOutStatus(
        @Header("Authorization-Basic") tokenValue: String?, @Query("user_id") user_id: String?, @Query(
            "address_id"
        ) address_id: String?
    ): Call<CheckoutResponse>


    @FormUrlEncoded
    @POST("checkout")
    fun checkoutPost(
        @Header("Authorization-Basic") tokenValue: String?, @Field("user_id") user_id: String?, @Field(
            "address_id"
        ) address_id: String, @Field("code") code: String,
        @Field("payment_gateway_id") payment_gateway_id: String, @Field("platform") platform: String
    ): Call<CheckoutPostResponse>


    @GET("user/{id}/orders")
    fun MyOrders(@Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?): Call<MyOrdersResponse>

    @GET("user/{id}/orders/{id1}")
    fun getOrderList(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?, @Path(
            "id1"
        ) orderId: String?
    ): Call<OrderListResponse>

    @GET("user/{id}/notification")
    fun Notifications(@Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?): Call<NotificationsResponse>

    @PUT("user/{id}/notification/{id1}/read")
    fun NotificationRead(
        @Header("Authorization-Basic") tokenValue: String?, @Path("id") userId: String?, @Path(
            "id1"
        ) notificationId: String?
    ): Call<NotificationReadResponse>


    @FormUrlEncoded
    @PUT("fcm-token")
    fun updateFcmTocken(
        @Header("Authorization-Basic") tokenValue: String, @Field("user_id") user_id: String, @Field(
            "fcm_token"
        ) fcm_token: String
    ): Call<BaseResponse>


    @GET("app/version")
    fun CheckAppUpdate(@Header("Authorization-Basic") tokenValue: String?): Call<AppVersionResponse>


    @GET("wishList")
    fun getWhishlisList(@Query("user_id") userId: String?): Call<WishlistResponse>


    @DELETE("wishList_items/{id}")
    fun deleteWishList(@Path("id") id: String?): Call<WishListDeleteresponse>


    @FormUrlEncoded
    @POST("wishList")
    fun addWishList(
        @Field("module") module: String?, @Field("user_id") userId: String?,
        @Field("item_id") item_id: String?, @Field("add_type") add_type: String?
    ): Call<WishListPostResponse>

}
