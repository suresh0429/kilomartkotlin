package com.innasoft.kilomart.Apis

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient private constructor() {
    private val retrofit: Retrofit

    val api: Api
        get() = retrofit.create(Api::class.java)

    init {

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Accept", "Application/JSON")
                    .addHeader(
                        "X-Api-Key",
                        "003026bbc133714df1834b8638bb496e-8f4b3d9a-e931-478d-a994-28a725159ab9"
                    )
                    .addHeader("X-Platform", "ANDROID")
                    .build()
                chain.proceed(request)
            }.build()

        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    companion object {

        /*
        public static final String BASE_URL = "http://kilomarttesting.learningslot.in/api/";
    */


        val BASE_URL = "http://www.kilomart.in/api/v1.0/"

        // images url
        val IMAGE_BASE_URL = "http://www.kilomart.in/"
        val PRODUCT_IMAGE_BASE_URL = IMAGE_BASE_URL + "images/products/70x70/"
        val PRODUCT_IMAGE_BASE_URL1 = IMAGE_BASE_URL + "images/products/150x200/"
        val PRODUCT_IMAGE_BASE_URL2 = IMAGE_BASE_URL + "images/products/400x400/"

        // web links url
        val ABOUT_US_URL = BASE_URL + "urls/about-us"
        val PRIVACY_POLICY_URL = BASE_URL + "urls/privacy-policy"
        val TERMS_CONDITIONS_URL = BASE_URL + "urls/terms-conditions"


        private var mInstance: RetrofitClient? = null

        val instance: RetrofitClient?
            @Synchronized get() {
                if (mInstance == null) {
                    mInstance = RetrofitClient()
                }
                return mInstance
            }
    }
}
