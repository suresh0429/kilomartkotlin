package com.innasoft.kilomart

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle

import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.ViewPager

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.ViewPagerAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Helper.Converter
import com.innasoft.kilomart.Response.BaseResponse
import com.innasoft.kilomart.Response.ProductSingleResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager
import com.rd.PageIndicatorView

import java.util.ArrayList
import java.util.HashMap

import butterknife.BindView
import butterknife.ButterKnife
import com.innasoft.kilomart.Apis.RetrofitClient.Companion.PRODUCT_IMAGE_BASE_URL2
import com.innasoft.kilomart.Storage.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import com.innasoft.kilomart.Storage.Utilities.capitalize
import kotlinx.android.synthetic.main.activity_product_details.*

class ProductDetailsActivity : AppCompatActivity() {



    internal lateinit var appController: AppController
    internal var viewPagerItemslist: MutableList<String> = ArrayList()


    private var pref: PrefManager? = null
    internal var userid: String? = null
    internal var cartindex: Int = 0
    internal var count = 1

    internal var id: String? = null
    internal var title: String? = null
    internal var deviceId: String? = null
    internal var tokenValue: String? = null
    internal var productIdsSet: String? = null
    internal var cartTypeSet: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        loadOncreateData()


    }

    private fun loadOncreateData() {
        if (intent.extras != null) {
            id = intent.getStringExtra("productId")
            Log.e("PRODUCTID", "" + id!!)
            title = intent.getStringExtra("productName")
        }
        supportActionBar!!.title = capitalize(title)
        appController = applicationContext as AppController

        pref = PrefManager(applicationContext)

        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userid = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        if (appController.isConnection) {

            progressBarMain!!.visibility = View.VISIBLE

            prepareProductDetailsData(id, deviceId)

            // cart count
            appController.cartCount(userid, deviceId)
            val preferences = getSharedPreferences("CARTCOUNT", 0)
            cartindex = preferences.getInt("itemCount", 0)
            productIdsSet = preferences.getString("productIds", null)
            cartTypeSet = preferences.getString("cartType", null)


            Log.e("productIdsSet", "$productIdsSet------$cartTypeSet")


            invalidateOptionsMenu()


        } else {

            setContentView(R.layout.internet)

            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }


    private fun prepareProductDetailsData(id: String?, deviceId: String?) {

        progressBarMain!!.visibility = View.VISIBLE
        val call = RetrofitClient.instance!!.api.getProductdetails(id, deviceId)

        call.enqueue(object : Callback<ProductSingleResponse> {
            override fun onResponse(
                call: Call<ProductSingleResponse>,
                response: Response<ProductSingleResponse>
            ) {


                val productResponse = response.body()

                if (response.isSuccessful) {
                    progressBarMain!!.visibility = View.GONE

                    if (productResponse!!.status.equals("10100", ignoreCase = true)) {

                        val product = response.body()!!.data


                        txtProductName!!.text = capitalize(product!!.product_name)

                        txtPrice!!.text = resources.getString(R.string.Rs) + Utilities.df.format(product.selling_price)
                        txtWeight!!.text = product.unit_value + product.unit_name

                        txtDisPrice!!.text = resources.getString(R.string.Rs) + Utilities.df.format(product.mrp_price)
                        txtDisPrice!!.paintFlags =
                            txtDisPrice!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        txtDisPrice!!.setTextColor(Color.RED)
                        txtDescription!!.text = product.seo_description

                        // check avalibulity
                        if (product.availability.equals("1", ignoreCase = true)) {
                            btnAddtocart!!.isClickable = false
                            btnAddtocart!!.isEnabled = false
                            btnAddtocart!!.setBackgroundColor(Color.parseColor("#C0C0C0"))
                        }


                        // slider Images
                        val namesList = product.images

                        Log.e("ALLIMAGESLIDES", "" + namesList!!.size)


                        viewPagerItemslist.clear()
                        for (name in namesList!!) {

                            println(name)

                            Log.e("IMAGESLIDES", "" + name)

                            viewPagerItemslist.add(PRODUCT_IMAGE_BASE_URL2 + name)
                        }

                        val pageIndicatorView =
                            findViewById<PageIndicatorView>(R.id.pageIndicatorView)
                        val adapter = ViewPagerAdapter(
                            applicationContext,
                            viewPagerItemslist,
                            product.availability.toString()
                        )
                        val viewPager = findViewById<View>(R.id.viewPager) as ViewPager
                        viewPager.adapter = adapter
                        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                            override fun onPageScrolled(
                                position: Int,
                                positionOffset: Float,
                                positionOffsetPixels: Int
                            ) {

                            }

                            override fun onPageSelected(position: Int) {
                                pageIndicatorView.selection = position
                            }

                            override fun onPageScrollStateChanged(state: Int) {

                            }
                        })


                        // quantity decrease
                        product_minus!!.setOnClickListener {
                            if (count > 1) {
                                count--
                                product_quantity!!.text = "" + count
                            }
                        }

                        // quantity increase
                        product_plus!!.setOnClickListener {
                            count++
                            product_quantity!!.text = "" + count
                        }


                        btnAddtocart!!.setOnClickListener {
                            addtocartData(
                                product.id!!,
                                product_quantity!!.text.toString()
                            )
                        }

                        btnGotoCart!!.setOnClickListener {
                            val intent = Intent(applicationContext, CartActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }


                    } else if (productResponse.status == "10200") {

                        Toast.makeText(
                            applicationContext,
                            productResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (productResponse.status == "10300") {

                        Toast.makeText(
                            applicationContext,
                            productResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (productResponse.status == "10400") {

                        Toast.makeText(
                            applicationContext,
                            productResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                }
            }

            override fun onFailure(call: Call<ProductSingleResponse>, t: Throwable) {
                progressBarMain!!.visibility = View.GONE

            }
        })


    }


    // submit details
    private fun addtocartData(productid: String, quantity: String) {

        progressBarMain!!.visibility = View.VISIBLE

        val call = RetrofitClient.instance!!.api.addtoCart(productid, deviceId, userid, quantity)

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                progressBarMain!!.visibility = View.GONE

                val cartPostResponse = response.body()

                if (response.isSuccessful) {

                    if (cartPostResponse!!.status.equals("10100", ignoreCase = true)) {

                        // Toast.makeText(ProductDetailsActivity.this, cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.WHITE + "\">" + cartPostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        // cart count updation
                        cartindex = cartindex + 1
                        Log.e("CARTPLUSCOUNT", "" + cartindex)
                        val preferences = getSharedPreferences("CARTCOUNT", 0)
                        val editor = preferences.edit()
                        editor.putInt("itemCount", cartindex)
                        editor.apply()


                        // cart count Update
                        appController.cartCount(userid, deviceId)
                        invalidateOptionsMenu()

                    } else if (cartPostResponse.status == "10200") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + cartPostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        //  Toast.makeText(getApplicationContext(), cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (cartPostResponse.status == "10300") {
                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + cartPostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()

                        //  Toast.makeText(getApplicationContext(), cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (cartPostResponse.status == "10400") {

                        Snackbar.make(
                            parentLayout!!,
                            Html.fromHtml("<font color=\"" + Color.RED + "\">" + cartPostResponse.message + "</font>"),
                            Snackbar.LENGTH_SHORT
                        ).show()


                        // Toast.makeText(getApplicationContext(), cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                progressBarMain!!.visibility = View.GONE
            }
        })
    }


    override fun onRestart() {
        super.onRestart()

        loadOncreateData()
        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindexonstart", "" + cartindex)
        invalidateOptionsMenu()
    }

    override fun onStart() {
        super.onStart()

        //  loadOncreateData();
        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindexonstart", "" + cartindex)
        invalidateOptionsMenu()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.actionbar_menu, menu)
        val menuItem = menu.findItem(R.id.action_cart)
        menuItem.icon = Converter.convertLayoutToImage(
            this@ProductDetailsActivity,
            cartindex,
            R.drawable.ic_actionbar_bag
        )
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()

            R.id.action_cart -> {
                val intent = Intent(applicationContext, CartActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            R.id.action_search -> {
                val intent1 = Intent(this@ProductDetailsActivity, SearchActivity::class.java)
                intent1.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent1)
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
