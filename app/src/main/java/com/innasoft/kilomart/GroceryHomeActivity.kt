package com.innasoft.kilomart

import android.content.Intent
import android.graphics.Color
import android.os.Bundle

import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager

import com.google.android.material.snackbar.Snackbar
import com.innasoft.kilomart.Adapters.SubCatRecyclerViewDataAdapter
import com.innasoft.kilomart.Adapters.SubCatageoryAdapter
import com.innasoft.kilomart.Apis.RetrofitClient
import com.innasoft.kilomart.Helper.Converter
import com.innasoft.kilomart.Model.HeaderSectionDataModel
import com.innasoft.kilomart.Model.SingleItemModel
import com.innasoft.kilomart.Response.HomeResponse
import com.innasoft.kilomart.Response.ProductResponse
import com.innasoft.kilomart.Singleton.AppController
import com.innasoft.kilomart.Storage.PrefManager

import java.util.ArrayList

import butterknife.ButterKnife
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import com.innasoft.kilomart.Storage.Utilities.capitalize
import kotlinx.android.synthetic.main.activity_grocery_home.*

class GroceryHomeActivity : AppCompatActivity() {
    internal lateinit var appController: AppController


    internal lateinit var allSampleData: ArrayList<HeaderSectionDataModel>


    private var adapter: SubCatageoryAdapter? = null
    private var subCatRecyclerViewDataAdapter: SubCatRecyclerViewDataAdapter? = null

    private var pref: PrefManager? = null
    internal var userid: String? = null
    internal var tokenValue: String? = null
    internal var deviceId: String? = null
    internal var cartindex: Int = 0
    internal var catId: String? = null
    internal var cat_name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grocery_home)
        ButterKnife.bind(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        appController = application as AppController

        pref = PrefManager(applicationContext)

        if (intent != null) {
            catId = intent.getStringExtra("CatId")
            cat_name = intent.getStringExtra("Cat_name")

        }

        supportActionBar!!.title = capitalize(cat_name)

        allSampleData = ArrayList()


        // Displaying user information from shared preferences
        val profile = pref!!.userDetails
        userid = profile["id"]
        tokenValue = profile["AccessToken"]
        deviceId = profile["deviceId"]

        simpleSwipeRefreshLayout!!.setColorSchemeResources(
            R.color.colorAccent,
            R.color.colorBlue,
            R.color.colorPrimary
        )
        appController = applicationContext as AppController
        if (appController.isConnection) {

            prepareSubCatData()
            // cart count
            appController.cartCount(userid, deviceId)
            val preferences = getSharedPreferences("CARTCOUNT", 0)
            cartindex = preferences.getInt("itemCount", 0)
            Log.e("cartindex", "" + cartindex)
            invalidateOptionsMenu()


            simpleSwipeRefreshLayout!!.setOnRefreshListener {
                prepareSubCatData()

                simpleSwipeRefreshLayout!!.isRefreshing = false
            }

        } else {

            setContentView(R.layout.internet)
            val tryButton = findViewById<View>(R.id.btnTryagain) as Button
            tryButton.setOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }


        }


    }

    private fun prepareSubCatData() {

        simpleSwipeRefreshLayout!!.isRefreshing = true
        val call = RetrofitClient.instance!!.api.getHomePageRequest("sub", catId, "")
        call.enqueue(object : Callback<HomeResponse> {
            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                simpleSwipeRefreshLayout!!.isRefreshing = false

                val subCatResponse = response.body()

                if (response.isSuccessful) {

                    if (subCatResponse!!.status.equals("10100", ignoreCase = true)) {

                        if (subCatResponse.message.equals("No records found", ignoreCase = true)) {

                            Toast.makeText(
                                applicationContext,
                                subCatResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            // subCat Recycler
                            val mLayoutManager = LinearLayoutManager(
                                applicationContext,
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )
                            catRecycler!!.layoutManager = mLayoutManager
                            catRecycler!!.itemAnimator = DefaultItemAnimator()
                            catRecycler!!.setHasFixedSize(true)
                            catRecycler!!.isNestedScrollingEnabled = false

                            // subcat Adapter
                            adapter =
                                SubCatageoryAdapter(applicationContext, subCatResponse.data!!.toList(), catId.toString())
                            catRecycler!!.adapter = adapter
                            adapter!!.notifyDataSetChanged()


                            allSampleData.clear()
                            for (subCategoryBean in subCatResponse.data!!) {

                                productsData(subCategoryBean.name!!, subCategoryBean.id!!, catId)

                            }
                        }


                    }


                } else {
                    // error case
                    when (response.code()) {
                        404 -> Snackbar.make(
                            simpleSwipeRefreshLayout!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.not_found) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        500 -> Snackbar.make(
                            simpleSwipeRefreshLayout!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.server_broken) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        else -> Snackbar.make(
                            simpleSwipeRefreshLayout!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.unknown_error) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }


            }

            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                simpleSwipeRefreshLayout!!.isRefreshing = false
                Snackbar.make(
                    simpleSwipeRefreshLayout!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })


    }

    private fun productsData(title: String, subCategory_Id: String, category_id: String?) {

        val dm = HeaderSectionDataModel()
        dm.headerTitle = title
        dm.categoryId = category_id
        dm.subcategoryId = subCategory_Id

        val singleItem = ArrayList<SingleItemModel>()

        simpleSwipeRefreshLayout!!.isRefreshing = true
        val call = RetrofitClient.instance!!.api.getProductList(
            category_id,
            subCategory_Id,
            "0",
            "SINGLE",
            "1",
            userid,
            deviceId,
            "",
            "",
            "LOW_PRICE"
        )
        call.enqueue(object : Callback<ProductResponse> {
            override fun onResponse(
                call: Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
                simpleSwipeRefreshLayout!!.isRefreshing = false

                val productResponse1 = response.body()


                if (response.isSuccessful) {

                    if (productResponse1!!.status.equals("10100", ignoreCase = true)) {


                        for (productResponse in productResponse1.data!!) {

                            singleItem.add(
                                SingleItemModel(
                                    productResponse.id,
                                    productResponse.url_name,
                                    productResponse.product_name,
                                    productResponse.type,
                                    productResponse.main_category_id,
                                    productResponse.main_category_name,
                                    productResponse.sub_category_id,
                                    productResponse.sub_category_name,
                                    productResponse.child_category_id,
                                    productResponse.child_category_name,
                                    productResponse.unit_id,
                                    productResponse.unit_name,
                                    productResponse.unit_value,
                                    productResponse.brand_id,
                                    productResponse.brand_name,
                                    productResponse.qty,
                                    productResponse.mrp_price,
                                    productResponse.offer_price,
                                    productResponse.selling_price,
                                    productResponse.about,
                                    productResponse.moreinfo,
                                    productResponse.availability,
                                    productResponse.user_rating,
                                    productResponse.features,
                                    productResponse.position,
                                    productResponse.seo_title,
                                    productResponse.seo_description,
                                    productResponse.seo_keywords,
                                    productResponse.images
                                )
                            )
                        }

                        dm.allItemsInSection = singleItem
                        allSampleData.add(dm)

                        // product Recycler
                        val mLayoutManager1 = LinearLayoutManager(
                            applicationContext,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        my_recycler_view!!.layoutManager = mLayoutManager1
                        my_recycler_view!!.itemAnimator = DefaultItemAnimator()
                        my_recycler_view!!.setHasFixedSize(true)
                        my_recycler_view!!.isNestedScrollingEnabled = false

                        // product Adapter
                        subCatRecyclerViewDataAdapter = SubCatRecyclerViewDataAdapter(
                            applicationContext,
                            allSampleData,
                            singleItem
                        )
                        my_recycler_view!!.adapter = subCatRecyclerViewDataAdapter
                        subCatRecyclerViewDataAdapter!!.notifyDataSetChanged()


                    }


                } else {
                    // error case
                    when (response.code()) {
                        404 -> Snackbar.make(
                            simpleSwipeRefreshLayout!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.not_found) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        500 -> Snackbar.make(
                            simpleSwipeRefreshLayout!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.server_broken) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                        else -> Snackbar.make(
                            simpleSwipeRefreshLayout!!,
                            Html.fromHtml(
                                "<font color=\"" + Color.RED + "\">" + resources.getString(R.string.unknown_error) + "</font>"
                            ),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }


            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                simpleSwipeRefreshLayout!!.isRefreshing = false
                Snackbar.make(
                    simpleSwipeRefreshLayout!!,
                    Html.fromHtml("<font color=\"" + Color.RED + "\">" + resources.getString(R.string.slowInternetconnection) + "</font>"),
                    Snackbar.LENGTH_SHORT
                ).show()

            }
        })


    }


    override fun onRestart() {

        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindexonstart", "" + cartindex)
        invalidateOptionsMenu()
        super.onRestart()
    }

    override fun onStart() {
        appController.cartCount(userid, deviceId)
        val preferences = getSharedPreferences("CARTCOUNT", 0)
        cartindex = preferences.getInt("itemCount", 0)
        Log.e("cartindexonstart", "" + cartindex)
        invalidateOptionsMenu()
        super.onStart()
    }

    ///
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.actionbar_menu, menu)
        val menuItem = menu.findItem(R.id.action_cart)
        menuItem.icon = Converter.convertLayoutToImage(
            this@GroceryHomeActivity,
            cartindex,
            R.drawable.ic_actionbar_bag
        )
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            android.R.id.home -> onBackPressed()

            R.id.action_cart -> {
                val intent = Intent(applicationContext, CartActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            R.id.action_search -> {
                val intent1 = Intent(this@GroceryHomeActivity, SearchActivity::class.java)
                intent1.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent1)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        var MODULE = "grocery"
    }


}
