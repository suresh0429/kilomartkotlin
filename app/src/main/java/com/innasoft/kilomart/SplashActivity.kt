package com.innasoft.kilomart

import android.content.Intent
import android.os.Handler
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            // shared animation between two activites
            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
            startActivity(intent)
        }, SPLASH_TIME_OUT.toLong())

    }

    override fun onBackPressed() {
        //super.onBackPressed();
        moveTaskToBack(true)
        android.os.Process.killProcess(android.os.Process.myPid())
        System.exit(1)
    }

    companion object {
        private val SPLASH_TIME_OUT = 3000
    }
}
